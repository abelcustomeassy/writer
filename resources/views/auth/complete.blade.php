@extends('layouts.page')

@section('title','Registration Complete')

@section('page')
    <section  class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1"
                     data-aos="fade-up">
                    @include('pages.partials.offers')
                    <p class="mt-5">Join <strong class="text-success text-monospace">1000+</strong> writers making
                        money from home</p>
                </div>
                <div class="col-lg-6 flex-lg-column order-1 order-lg-2" data-aos="fade-up">
                    <div class="bg-white ml-md-auto mr-md-auto p-5 rounded shadow w-85">
                        <div class="text-center py-5">
                            <p class="text-success">
                                <span><i class="bx bx-check-circle bx-lg"></i></span>
                            </p>
                            <h2 class="mt-3"> Congratulations!</h2>
                            <p>Your account has been created successfully</p>
                            <p>
                                To complete the registration process, <strong>Check your mail</strong> for our
                                confirmation link to complete registration.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
