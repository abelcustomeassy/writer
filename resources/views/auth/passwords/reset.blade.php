@extends('layouts.page')

@section('title', 'Reset Password')

@section('page')
    <section  class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1"
                     data-aos="fade-up">
                    @include('pages.partials.offers')
                    <a href="{{ route('register') }}" class="download-btn">
                        <i class="bx bx-chevron-right-circle"></i>Become a freelancer, <span class="light-text">It's free</span>
                    </a>
                    <p class="mt-5">Join <strong class="text-success text-monospace">1000+</strong> writers making
                        money from home</p>
                </div>
                <div class="col-lg-6 flex-lg-column order-1 order-lg-2" data-aos="fade-up">
                    <div class="bg-white ml-md-auto mr-md-auto p-5 rounded shadow w-85">
                        <div class="text-center h5 py-3">RESET PASSWORD</div>
                        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                            @csrf()
                            <input type="hidden" name="token" value="{{ $token }}">
                            <input type="hidden" class="form-control" name="email" value="{{ $email }}">
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="control-label">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-circled">
                                    <i class="bx bx-save"></i> Reset Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
