@extends('layouts.page')

@section('title', 'Password Reset')

@section('page')
    <section  class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1"
                     data-aos="fade-up">
                    @include('pages.partials.offers')
                    <a href="{{ route('register') }}" class="download-btn">
                        <i class="bx bx-chevron-right-circle"></i>Become a Writer, <span
                                class="light-text">It's free</span>
                    </a>
                    <p class="mt-5">Join <strong class="text-success text-monospace">1000+</strong> writers making
                        money from home</p>
                </div>
                <div class="col-lg-6 flex-lg-column order-1 order-lg-2" data-aos="fade-up">
                    <div class="bg-white ml-md-auto mr-md-auto p-5 rounded shadow w-85">
                        <div class="text-center h5 py-3">PASSWORD RESET</div>
                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            @csrf()
                            <div class="form-group">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                       value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                 <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-circled">
                                    <i class="bx bxs-envelope"></i> Send RESET Link
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
