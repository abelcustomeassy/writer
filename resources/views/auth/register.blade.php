@extends('layouts.page')

@section('title', 'Join Us')

@if(\Illuminate\Support\Facades\App::environment('production'))
    @push('scripts')
    <script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITE_KEY') }}"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('{{ env('RECAPTCHA_SITE_KEY') }}', {action: 'contact'}).then(function (token) {
                $("#captcha").val(token);
            });
        });
    </script>
    @endpush
@endif

@section('page')
    <section  class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1"
                     data-aos="fade-up">
                    @include('pages.partials.offers')
                    <p class="mt-5">Join <strong class="text-success text-monospace">1000+</strong> writers making
                        money from home</p>
                </div>
                <div class="col-lg-6 flex-lg-column order-1 order-lg-2" data-aos="fade-up">
                    @if($configuration->writer_registration_enabled)
                        <div class="bg-white ml-md-auto mr-md-auto p-5 rounded shadow w-85">
                            <div class="text-center h5">REGISTER</div>
                            <form id="register" class="form-horizontal" method="POST" action="{{ route('register') }}">
                                @csrf()
                                <input type="hidden" name="timezone" class="timezone">
                                <input type="hidden" name="captcha" id="captcha">
                                <div class="form-group">
                                    <label for="name" class="control-label">Name</label>
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') }}" autofocus>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                               <strong>{{ $errors->first('name') }}</strong>
                            </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email"
                                           value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password" class="control-label">Password</label>
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                 <strong>{{ $errors->first('password') }}</strong>
                            </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation" class="control-label">Confirm Password</label>
                                    <input id="password_confirmation" type="password" class="form-control"
                                           name="password_confirmation">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-circled">
                                        <i class="bx bx-user-plus"></i> Register
                                    </button>
                                </div>
                            </form>
                            <p>Already have an account?
                                <a href="{{ route('login') }}">
                                    Sign In
                                </a>
                            </p>
                        </div>
                    @else
                        <div class="bg-white ml-md-auto mr-md-auto p-5 rounded shadow w-85">
                            <div class="text-center py-5">
                                <p class="text-warning">
                                    <span><i class="bx bxs-error bx-lg"></i></span>
                                </p>
                                <p>
                                    Thank you for your interest in joining our elite team of writers. However,
                                    registration is
                                    currently closed, please try again later.
                                </p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
