@extends('layouts.page')

@section('title','Login')

@section('page')
    <section  class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1"
                     data-aos="fade-up">
                    <div>
                        @include('pages.partials.offers')
                       <a class="btn btn-warning btn-circled ml-4" href="{{ route('register') }}">
                    <i class="bx bx-chevron-right-circle"></i> Register Now
                </a>
                        <p class="mt-5">Join <strong class="text-success text-monospace">1000+</strong> writers making
                            money from home</p>
                    </div>
                </div>
                <div class="col-lg-6 flex-lg-column order-1 order-lg-2" data-aos="fade-up">
                    <div class="bg-white ml-md-auto mr-md-auto p-5 rounded shadow w-85">
                        <div class="text-center h5 py-3">LOG IN</div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf()
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <input id="email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                       value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <input id="password" type="password"
                                       class="form-control{{ $errors->has('password') ? 'is-invalid' : '' }}"
                                       name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"
                                           id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">Remember Me</label>
                                </div>
                            </div>
                            <div class="row mt-4 align-items-center">
                                <div class="col">
                                    <button type="submit" class="btn btn-primary btn-circled">
                                        <i class="bx bx-log-in-circle"></i> Log In
                                    </button>
                                </div>
                                <div class="col text-right">
                                    <a href="{{ route('password.request') }}">
                                        Forgot password?
                                    </a>
                                </div>
                            </div>
                        </form>
                        <div class="py-4">
                            <p>Forgot your password?
                                <a href="{{ route('password.request') }}">
                                    Reset
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
