<section id="expectations" class="details">
    <div class="container">
        <div class="row content">
            <div class="col-md-4 order-1 order-md-2" data-aos="fade-left">
                <div class="mt-md-5">
                    <img src="{{ asset('images/details-4.png') }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-md-8 pt-5 order-2 order-md-1" data-aos="fade-up">
                <h3>What we expect from our writers</h3>
                <p class="font-italic">
                    Academic giants expects its writers to meet these duties:
                </p>
               
                <ul>
                    <li><i class="icofont icofont-check"></i>  Adhere to correct paragraph, sentence and paper structure
                    </li>
                    <li><i class="icofont icofont-check"></i>    Meet customer’s instructions, paper requirements, citation rules, and formatting guidelines
                    </li>
                    <li><i class="icofont icofont-check"></i>  Illustrate coherence of ideas and logical train of thought within the papers
                    </li>
                </ul>
                <p>It is important to consider the importance of maintaining timeliness and providing plagiarism-free, high-quality papers as the writer’s work directly affects the company’s reputation. </p>
            </div>
        </div>
    </div>
</section>