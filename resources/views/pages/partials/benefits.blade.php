<section id="benefits" class="details">
    <div class="container">
        <div class="row content">
            <div class="col-md-4" data-aos="fade-right">
                <div class="mt-md-4">
                    <img src="{{ asset('images/details-3.png') }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-md-8 pt-5" data-aos="fade-up">
                <h3>Who we are </h3>
                <p>Cupiditate placeat cupiditate placeat est ipsam culpa. Delectus quia minima quod. Sunt saepe
                    odit aut quia voluptatem hic voluptas dolor doloremque.</p>
                
                <p>
                    Qui consequatur temporibus. Enim et corporis sit sunt harum praesentium suscipit ut
                    voluptatem. Et nihil magni debitis consequatur est.
                </p>
                <p>
                    Suscipit enim et. Ut optio esse quidem quam reiciendis esse odit excepturi. Vel dolores
                    rerum soluta explicabo vel fugiat eum non.
                </p>
            </div>
        </div>
    </div>
</section>