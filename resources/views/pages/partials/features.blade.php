<section id="features" class="features">
    <div class="container">
        <div class="section-title">
            <h2>Work Features</h2>
            <p>Our organization’s long-term strategy involves providing the best quality standard in the writing world in terms of providing high-quality custom writing services. The company’s goal is to:</p>
        </div>

        <div class="row no-gutters">

            <div class="col-xl-7 d-flex align-items-stretch order-2 order-lg-1">
                <div class="content d-flex flex-column justify-content-center">
                    <div class="row">
                        <div class="col-md-6 icon-box" data-aos="fade-up">
                            <i class="bx bx-receipt"></i>
                            
                            <p> Offer non-plagiarized papers that are authentic in nature and demonstrate innovativeness</p>
                        </div>
                        <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                            <i class="bx bx-cube-alt"></i>
                            <p>   Instilling a professional identity in our writers, allowing them to improve their work</p>
                        </div>
                        <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                            <i class="bx bx-time"></i>
                            <p>  Providing timely assistance to our clients</p>
                        </div>
                        <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                            <i class="bx bx-image"></i>
                            
                            <p>   Maintaining a positive image </p>
                        </div>
                         <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                            <i class="bx bx-images"></i>
                            <p>   Cultivate mutual respect and collaboration among writers and clients</p>
                        </div>
                        <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                            <i class="bx bx-user"></i>
                            
                            <p>  Improve writers’ proficiency for personal development </p>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="image col-xl-5 d-flex align-items-stretch justify-content-center order-1 order-lg-2"
                 data-aos="fade-left" data-aos-delay="100">
                <img src="{{ asset('images/features.png') }}" class="img-fluid" alt="">
            </div>
            
        </div>
        <div class="row"> 
        <div class="col-md-6">
                <p>Our writers have a large number of disciplines to choose from depending on their preference and area of expertise. From law to medicine, writers can choose from a range of topics that range from research papers, articles or even personal papers such as those used for college admission. Academic giants deals with essays, research papers, dissertations, and creative writing papers. </p>
            </div>

             <div class="col-md-6">
                <p>New writers are advised to access the websites’ library guides and work on short papers on different topics. They can then work on longer, more demanding assignments that are both demanding and pay better. It is important to discern your strengths and weaknesses to tailor them to your writing style in fulfilment of the customers’ requirements.  </p>
            </div>

             <div class="col-md-6">
                <p class="font-weight-bold">Types of assignments to expect:<br></p>
<i class="icofont icofont-check"></i>   Annotated bibliography<br>
<i class="icofont icofont-check"></i>   Reviews (Book/movie)<br>
<i class="icofont icofont-check"></i>   Case Study<br>
<i class="icofont icofont-check"></i>   Creative Writing<br>
<i class="icofont icofont-check"></i>   Essay<br>
<i class="icofont icofont-check"></i>   Presentation, inclusive of speech notes<br>
<i class="icofont icofont-check"></i>   Research Paper<br>
<i class="icofont icofont-check"></i>   Full dissertation or Individual Chapters

            </div>
             <div class="col-md-6">
                <p>Every paper has various requirements, starting with deadlines, where they range from between a few hours to several days. New writers are advised to start working on long-deadline papers, with time, they may start working on orders with tight deadlines. There are various orders to choose from a list of available papers, most of these papers follow either APA or MLA citation styles. It is crucial for writers to determine the guidelines for each paper and use the most updated form of the citation styles for each paper, adhering to every aspect of the paper. However, fret not, the company provides guides for every formatting style, allowing them to work with relative ease on each order.  </p>
            </div>
    </div>
</section>