<div class="py-5 bg-lighter">
    <div class="container">
        <div class="text-center">
            <p>
                Register now to start earning
                <a class="btn btn-warning btn-circled ml-4" href="{{ route('register') }}">
                    <i class="bx bx-chevron-right-circle"></i> Get Started
                </a>
            </p>
        </div>
    </div>
</div>