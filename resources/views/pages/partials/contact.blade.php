@if(\Illuminate\Support\Facades\App::environment('production'))
    @push('scripts')
    <script src="https://www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_SITE_KEY') }}"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('{{ env('RECAPTCHA_SITE_KEY') }}', {action: 'contact'}).then(function (token) {
                $("#captcha").val(token);
            });
        });
    </script>
    @endpush
@endif
<section id="contact" class="contact">
    <div class="container">
        <div class="section-title">
            <h2>Contact</h2>
            <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit
                sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias
                ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="info" data-aos="fade-up" data-aos-delay="200">
                    <i class="bx bx-envelope"></i>
                    <h4>Email Us</h4>
                    <p>contact@academicgiants.com</p>
                </div>
                <div class="info mt-3" data-aos="fade-up" data-aos-delay="300">
                    <i class="bx bx-chat"></i>
                    <h4>Chat</h4>
                    <p>We are available on chat 24/7</p>
                </div>
            </div>
            <div class="col-lg-6">
                <form action="{{ route('contacts.store') }}" method="post" role="form"
                      class="php-email-form validate-form"
                      data-aos="fade-up">
                    @csrf()
                    <input type="hidden" name="captcha" id="captcha">
                    <div class="form-group">
                        <input placeholder="Your Name" type="text" name="name"
                               class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               data-rule-minlength="3"
                               data-rule-required="true"/>
                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                               <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input placeholder="Your Email" type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               data-rule-maxlength="255"
                               data-rule-required="true" data-rule-email="true"/>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                               <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input placeholder="Subject" type="text"
                               class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject"
                               data-rule-required="true"
                               data-rule-minlength="5"
                               data-rule-maxlength="255"/>
                        @if ($errors->has('subject'))
                            <span class="invalid-feedback">
                               <strong>{{ $errors->first('subject') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                                <textarea placeholder="Message"
                                          class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}"
                                          name="message" rows="5"
                                          data-rule-required="true"
                                          data-rule-minlength="5"
                                          data-rule-maxlength="600"></textarea>
                        @if ($errors->has('message'))
                            <span class="invalid-feedback">
                               <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('captcha'))
                            <span class="invalid-feedback">
                               <strong>{{ $errors->first('captcha') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="text-center">
                        <button type="submit">
                            <i class="bx bx-envelope"></i>
                            Send Message
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>