<h1>What we offer</h1>
<ul class="offers mb-5">
    <li><i class="bx bx-timer text-primary"></i>Manage your time and workload</li>
    <li><i class="bx bx-support text-success"></i>24/7 available support</li>
    <li><i class="bx bxs-coin-stack text-default"></i>Timely payments</li>
    <li><i class="bx bx-badge-check text-info"></i>Registration is free</li>
    <li><i class="bx bx-current-location text-danger"></i>Work from anywhere</li>
</ul>