<section id="about" class="bg-lighter details">
    <div class="container">
        <div class="row content">
            <div class="col-md-4" data-aos="fade-right">
                   <img src="{{ asset('images/details-1.svg') }}" class="img-fluid mt-md-7" alt="About All Term Paper">
            </div>
            <div class="col-md-8 pt-4" data-aos="fade-up">
                <h3>The benefits you enjoy working as a freelance writer:</h3>
                <p >
                   Freelance writing provides individuals with several benefits that people with other jobs do not enjoy. 
                </p>
                 <ul>
                    <li><i class="icofont icofont-check"></i> Time independence. This is arguably one of the best features of online writing and the freelance form of life</li>
                    <li><i class="icofont icofont-check"></i> Location independence. You can work from any location, affording you the opportunity to operate from any place, worldwide
                    </li>
                    <li><i class="icofont icofont-check"></i> Getting away from the commute. Freelance writers also benefit from flexible operations, where you will only need an internet connection and a smart device to work. You do not need to move anymore, say goodbye to commuting. 
                    </li>
                    <li><i class="icofont icofont-check"></i>  Limitless income. Theoretically, freelance writers have an endless supply of income. The more you work, the more you earn. Furthermore, you do not have restrictions about how much you can do, provided that you deliver quality work, you can keep working until you feel tired.
                    </li>
                    <li><i class="icofont icofont-check"></i>  Interesting work. Freelance writers are continually exposed to different types of papers across varying fields of study. You will always have access to new information, improving your writing abilities and knowledge.  
                    </li>
                </ul>
            
            </div>
        </div>
    </div>
</section>