@extends('layouts.page')

@section('title', 'Terms and Conditions')

@section('page')
<section class=" py-5 bg-primary " style="margin-top: 70px">
	<div class="container ">
		<div class="row">
			<div class="col-md-6">
				<h1 class="text-white">Contacts</h1>
			</div>
			<div class="col-md-6" >
			<a class="btn btn-warning btn-circled ml-4" href="{{ route('register') }}" style="float: right;">
                    <i class="bx bx-chevron-right-circle"></i> Register Now
                </a>
			</div>				
		</div>
	</div>
</section>
 @include('pages.partials.contact')
@endsection