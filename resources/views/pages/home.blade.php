@extends('layouts.page')

@section('title', 'Hire freelancer writers')

@section('hero')
    <section class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1"
                     data-aos="fade-up">
                    <div class="mt-lg-5">
                        <h1>REGISTER NOW TO START EARNING!</h1>
                        <h2>Join 1000+ writers making money from home</h2>
                     
                        <a class="btn btn-warning btn-circled ml-4" href="{{ route('register') }}">
                    <i class="bx bx-chevron-right-circle"></i> Register Now
                </a>
                    </div>
                </div>
                <div class="col-lg-6 flex-lg-column order-1 order-lg-2 hero-img"
                     data-aos="fade-up">
                    <img src="{{ asset('/images/details-4.png') }}" class="img-fluid mb-lg-4"
                         alt="Freelance Writing Jobs">
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page')
    @include('pages.partials.about')
    @include('pages.partials.features')

    <div class="py-5 bg-lighter">
    <div class="container">
        <div class="text-center">
            <p>
              You are an applicant and have questions? We will help you!
                <a class="btn btn-warning btn-circled ml-4" href="{{ route('register') }}">
                    Talk To Us
                </a>
            </p>
        </div>
    </div>
</div>

    @include('pages.partials.expectations')
      <div class="py-5 bg-lighter">
    <div class="container">
        <div class="text-center">
            <p>
             Do you want to see the full terms and conditions? 
                <a class="btn btn-warning btn-circled ml-4" href="{{ route('register') }}">
                 T&C
                </a>
            </p>
        </div>
    </div>
</div>
   
  
@endsection
