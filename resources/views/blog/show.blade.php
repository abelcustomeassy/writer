@extends('layouts.page')

@section('title',$post->title)

@section('content')
    <div class="container">
        <div class="blog-content">
            <h2>{{ $post->title }}</h2>
            <div class="content">
                {!! $post->content !!}
            </div>
            <hr>
            <div id="disqus_thread"></div>
        </div>
    </div>
@stop

@section('script')
    <script type="javascript">
        var disqus_config = function () {
            this.page.url = "{{ url('blog',$post->slug) }}";
            this.page.identifier = Number('{{ $post->id }}');
        };
        (function () {
            var d = document, s = d.createElement('script');
            s.src = '//{{ config('system.discuss_name') }}.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
@append
