@extends('layouts.page')

@section('title','Blog')

@section('page')
<section class=" py-5 bg-primary " style="margin-top: 70px">
    <div class="container ">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-white">Blogs</h1>
            </div>
            <div class="col-md-6" >
            <a class="btn btn-warning btn-circled ml-4" href="{{ route('register') }}" style="float: right;">
                    <i class="bx bx-chevron-right-circle"></i> Register Now
                </a>
            </div>              
        </div>
    </div>
</section>
    <section class="section" id="blog">
        <div class="container">
            <div class="row justify-content-center pt-5">
                <div class="col-md-8 col-lg-6 text-center">
                    <h2 class="section-title">
                        Blog
                    </h2>
                    <p>
                        Get the latest tips and trends in the world of writing
                    </p>
                </div>
            </div>
            <div class="py-3 border-top mt-3">
                @if($posts->count())
                    <form method="post" action="{{ route('posts.search') }}" class="">
                        {!! csrf_field() !!}
                        <div class="form-row">
                            <label for="search" class="control-label text-right mt-3 h5 col-sm-8">Search:</label>
                            <div class="col-sm-4">
                                <input type="text" id="search" name="item" class="form-control">
                            </div>
                        </div>
                    </form>
                @endif
            </div>
            <div class="row justify-content-center">
                @forelse($posts as $post)
                    <div class="col-lg-4 col-md-6">
                        <div class="blog-box">
                            <div class="blog-img-box">
                                <img src="{{ asset($post->getFirstMediaUrl('featured')) }}" alt=""
                                     class="img-fluid blog-img">
                            </div>
                            <div class="single-blog">
                                <div class="blog-content">
                                    <h6>{{ $post->created_at->format('d F Y') }}</h6>
                                    <a href="#">
                                        <h3 class="card-title">
                                            {{ Illuminate\Support\Str::limit($post->title,50,'...') }}
                                        </h3>
                                    </a>
                                    <p>{!! Illuminate\Support\Str::limit(strip_tags($post->content),'200','...') !!}</p>
                                    <a href="{{ url('blog',$post->slug)}}" class="read-more">Read Article</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="alert alert-info">
                        <i class="bx bx-info-circle"></i> We have not posted any content yet please check back later.
                    </div>
                @endforelse
                {{ $posts->render() }}
            </div>
        </div>
    </section>
@endsection