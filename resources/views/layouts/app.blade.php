@extends('layouts.master')

@section('body-class')
    class="g-sidenav-show g-sidenav-pinned"
@endsection

@section('content')
    <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
        <div class="scrollbar-inner">
            <div class="sidenav-header align-items-center">
                <a class="navbar-brand text-center" href="{{ route('dashboard') }}">
                    <img src="{{ asset('images/logo.png') }}" class="navbar-brand-img">
                </a>
            </div>
            <div class="navbar-inner">
                <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                    @yield('sidebar')
                </div>
            </div>
        </div>
    </nav>
    <div class="main-content" id="panel">
        @include('app.nav.top')
        <div class="container-fluid mt--6">
            @yield('app')
        </div>
    </div>
@endsection