<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 footer-links" data-aos="fade-up" data-aos-delay="100">
                    <h4>Account</h4>
                    <ul>
                       
                        <li>
                            <i class="bx bx-chevron-right"></i> <a href="{{ url('terms-and-conditions') }}">Terms of  service</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i> <a href="{{ url('privacy-policy') }}">Privacy policy</a>
                        </li>
                        <li>
                            <i class="bx bx-chevron-right"></i> <a href="{{ route('register') }}">Create Account</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-4 footer-links" data-aos="fade-up" data-aos-delay="200">
                    <h4>Our Services</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ url('hiring') }}">Hiring</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ url('Contact') }}">Contacts</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ url('faq') }}">Faq</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('posts.index') }}">Blog</a></li>
                    </ul>
                </div>

                <div class="col-sm-4 footer-links" data-aos="fade-up" data-aos-delay="300">
                    <h4>Our Social Networks</h4>
                    <p>You can interact with the company via Facebook, Twitter and Instagram handles.</p>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container py-4">
        <div class="text-center">
            &copy; Copyright {{ date('Y') }}. All Rights Reserved
        </div>
    </div>
</footer>
<a href="#" class="back-to-top"><i class="icofont icofont-simple-up"></i></a>