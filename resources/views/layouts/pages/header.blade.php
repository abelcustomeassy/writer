<header id="header" class="fixed-top">
    <div class="container d-flex">
        <div class="logo mr-auto">
            <a href="{{ route('home') }}">
                <img src="{{ asset('images/logo.png') }}" alt="All Termpaper Logo" class="img-fluid">
            </a>
        </div>
        <nav class="nav-menu d-none d-lg-block">
            <ul>
               
                <li><a href="{{ url('hiring') }}">Hiring</a></li>
                <li><a href="{{ route('posts.index') }}">Blog</a></li>
                <li><a href="{{ url('faq') }}">FAQ</a></li>
                <li><a href="{{ url('contact') }}">Contact Us</a></li>
                <li class="get-started">
                    <a href="{{ route('login') }}">
                        <i class="bx bx-user mt-1"></i>
                        Log In
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</header>