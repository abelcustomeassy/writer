@extends('layouts.master')

@section('body-class')
    class="page"
@endsection

@section('content')
    @include('layouts.pages.header')
    @yield('hero')
    <main id="main">
        @yield('page')
    </main>
    @include('layouts.pages.footer')
@endsection