<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')

    <title>{{ config('app.name') }} - @yield('title')</title>

    <link href="{{ asset('css/vendor.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.bundle.css') }}" rel="stylesheet">
</head>
<body @yield('body-class')>
<div id="app">
    @yield('content')
</div>
<script src="{{ asset('js/vendor.bundle.js') }}"></script>
<script src="{{ asset('js/scripts.bundle.js') }}"></script>
@include('sweetalert::alert')
@stack('scripts')
</body>
</html>