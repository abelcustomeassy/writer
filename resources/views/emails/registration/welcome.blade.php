@component('mail::message')
# Dear {{ $user->name }},

Congratulations, your email address is now confirmed.

As part of our standard fraud-prevention practice, we require you to confirm your identity. We will need a photo of
your national ID or passport and will also need proof of an education certificate and the skills you are proficient in.

It is imperative that the pictures be clear, and the particular on the document visible and legible. If we suspect any
trace of photo editing software, your application will be discontinued.

You will also be required to pass a language test and submit a sample essay from instructions given.

We value your confidentiality, and we keep all personal information secure, according tour Privacy Policy.

@component('mail::button', ['url' => route('dashboard')])
CONTINUE APPLICATION
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
