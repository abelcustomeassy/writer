@component('mail::message')
# Dear {{ $user->name }},

Thank you for taking the time to submit your application, and importantly, your interest in our company. Please verify
your email address for your submission to be considered.

@component('mail::button', ['url' => route('confirm',['code' => $user->validation_code])])
CONFIRM ACCOUNT
@endcomponent

If you did not create this account, you can ignore this email.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
