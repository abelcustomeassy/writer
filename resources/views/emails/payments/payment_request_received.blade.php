@component('mail::message')
# Dear {{ $withdrawal->user->name }},

Your payment request of __{{ currency() }}{{ $withdrawal->amount }}__ has been received.The money will be sent to the
following account within the payment period.

@component('mail::panel')
@foreach($withdrawal->payment_method_details as $key => $detail)
__{{ ucwords(str_replace('_',' ',$key)) }}:__ {{ $detail }}

@endforeach
@endcomponent

In case there is an issue with the request or change of payment method, please contact the payment department ASAP.

Thank you for the corporation.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
