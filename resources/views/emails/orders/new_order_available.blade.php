@component('mail::message')
# Dear {{ $writer->name }},

A new order __#{{ $order->id }}__ has been placed.

Please have a look at it and see whether you can handle it.

@component('mail::button', ['url' => $url])
View order
@endcomponent

Kind Regards,<br>
{{ config('app.name') }}
@endcomponent
