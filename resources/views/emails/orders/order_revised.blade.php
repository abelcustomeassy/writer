@component('mail::message')
# Dear {{ $order->writer->name }},

You have a revision request for the order __#{{ $order->id }}__. Please confirm receipt and affirm that you will
complete the amendments within the allocated time. Before submission, please ensure that you have adhered to all the
comments accompanying the revision request to avoid a subsequent revision.

@if($order->status == \App\Common\OrderStatus::PENDING_CLIENT_REVIEW_CONFIRMATION)
If the revision instructions contain additional information not featured in the initial instructions, please notify
the support department immediately. Please be specific in your message, including everything that was changed.
If your argument is justified, we will contact the client and ask them to pay more, consequently increasing your bid.
@endif

@component('mail::button', ['url' => $url])
View order
@endcomponent

Kind Regards,<br>
{{ config('app.name') }}
@endcomponent
