@component('mail::message')
# Dear {{ $order->writer->name }},

Order __#{{ $order->id }}__ has been assigned to you. Please review the instructions carefully and
confirm the order to let us know that you working on it.

@component('mail::button', ['url' => $url])
View order
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
