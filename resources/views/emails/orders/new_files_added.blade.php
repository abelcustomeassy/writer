@component('mail::message')
# Dear {{ $order->writer->name }},

The client has added new files for order __#{{ $order->id }}__.

Note that if the instructions have changed from the initial, it is your duty to inform us as soon as possible.

@component('mail::button', ['url' => $url])
    View order
@endcomponent

Kind Regards,<br>
{{ config('app.name') }}
@endcomponent
