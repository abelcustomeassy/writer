@component('mail::message')
# Dear {{ $user->name }},

You have been reassigned from the order __#{{ $orderId }}__. Please check your financial overview to see if any changes
have been applied.

Thank you for your cooperation.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
