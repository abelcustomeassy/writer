@component('mail::message')
# Dear {{ $order->writer->name }},

You have taken order __#{{ $order->id }}__ .

Please start working on it and upload on time.

@component('mail::button', ['url' => $url])
View order
@endcomponent

Kind Regards,<br>
{{ config('app.name') }}
@endcomponent
