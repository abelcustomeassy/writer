@component('mail::message')
# Dear {{ $order->writer->name }},

The order __#{{ $order->id }}__ has been assigned to you. Please acknowledge that you are working on it ASAP.

Should you have any reservations regarding the assignment, please contact the support department before
confirming/rejecting the project.

@component('mail::button', ['url' => $url])
View order
@endcomponent

Kind Regards,<br>
{{ config('app.name') }}
@endcomponent
