@component('mail::message')
# Dear {{ $application->user->name }},

Congratulations! We are happy to welcome you to our team. As part of {{ config('app.name') }},
you will be part of a global workforce that specializes in custom essay writing, editing and business writing.

As a freelance writer, you will enjoy freedom to choose from a broad spectrum of topics and lengths, at
your preferred time. However, we would like to inform you that we value and reward serious candidates.

Above all, please ensure that you maintain the highest standards of professionalism when interacting with anyone
through our platform.

Please visit your dashboard to see the available orders.

@component('mail::button', ['url' => route('dashboard')])
Browse Orders
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
