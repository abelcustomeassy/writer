@component('mail::message')
# Dear {{ $application->user->name }},

We appreciate your interest in our company, and the time you have invested in your application. However, we have
several criteria obligatory for an applicant to meet, to be approved as a freelance writer in our company.
Unfortunately, you did not meet one or more of the requirements, and we cannot accept you as one of our writers.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
