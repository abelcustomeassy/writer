@component('mail::message')
# Dear {{ $user->name }}

Thank you for submitting your application for evaluation. Our editors are already working on it to establish that you
meet the necessary requirements to work in our company. The review process may take up to seven working days,
after which we will get back to you via email with a decision.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
