@component('mail::message')
# Dear {{ $user->name }}

Congratulations! Your account level has been upgraded to __{{ $user->writerProfile->writerLevel->name }}__. Our Quality Assurance
department has discovered a considerable improvement in your writing skills.

Thank you for continuing to produce quality content.

@component('mail::button', ['url' => route('profile')])
VIEW PROFILE
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
