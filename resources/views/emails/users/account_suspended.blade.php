@component('mail::message')
# Dear {{ $user->name }},

Your account has been suspended.

After careful consideration, we have found you in violation of our term of service. Your account is under review
and may be reinstated after three months.

Please contact customer support for any concerns.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
