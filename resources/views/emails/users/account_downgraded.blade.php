@component('mail::message')
# Dear {{ $user->name }}

Your account level has been lowered to __{{ $user->writerProfile->writerLevel->name }}__. Our Quality Assurance department has discovered
a significant flop in your writing skills.

Please make some adjustments to improve your quality to avoid suspension/deactivation.

@component('mail::button', ['url' => route('profile')])
VIEW PROFILE
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
