@component('mail::message')
# Dear {{ $user->name }},

Your account’s suspension status has been lifted. Kindly consider the probation period as a warning, and strive to
adhere to our terms of service.

You can now log in to your account, and see the available orders.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
