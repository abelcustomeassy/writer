@component('mail::message')
# Dear {{ $user->name }},

Your account has been deactivated.

After careful consideration, we have found you in violation of our terms of service.

Kindly note that this verdict is final, and nothing can be done to salvage the account.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
