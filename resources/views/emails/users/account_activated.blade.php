@component('mail::message')
# Dear {{ $user->name }},

After much consideration from your appeal, your account is now activated. Please strive to adhere to our terms of service.

You can now log in to your account, and see the available orders.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
