@component('mail::message')
# Hello {{ $message->recipient->name }},

You have a new message from {{ $message->sender->userType->name == 'Employee' ?
$message->sender->employeeProfile->department->name:$message->sender->userType->name  }}.

@component('mail::button', ['url' => route('messages.index')])
View Messages
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
