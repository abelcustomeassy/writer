@component('mail::message')
# Hello {{ $paymentAccount->user->name }}

A new payment account for __{{  $paymentAccount->paymentMethod->name }}__ has been created. Please click on the verify
account button to enable it as one of your payment methods.

@component('mail::button', ['url' => route('wallet.verify-payment-account', ['code' => $paymentAccount->verification_code ])])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
