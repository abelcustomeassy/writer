@component('mail::message')
# Dear {{ $order->writer->name }},

You have exceeded the time allocated to the order __#{{ $order->id }}__. We have a stringent policy on deadlines, and
we have no option but to impose a fine for the inconvenience.

Please check your financial overview to see the changes that have been applied.

@component('mail::button', ['url' => route('wallet.index')])
VIEW FINANCES
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
