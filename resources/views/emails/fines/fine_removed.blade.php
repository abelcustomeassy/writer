@component('mail::message')
# Dear {{ $writer->name }},

After careful consideration, we have decided to lift the fine that was imposed for the order __#{{ $orderId }}__.
Please check the financial overview to see the applied changes.

Thank you for your cooperation.

@component('mail::button', ['url' => route('wallet.index')])
VIEW FINANCES
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
