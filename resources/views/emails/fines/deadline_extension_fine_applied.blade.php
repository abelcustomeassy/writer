@component('mail::message')
# Dear {{ $order->writer->name }},

You requested a deadline extension on the order __#{{ $order->id }}__. We do not condone this practice, as it brews mix up and
contention. We had no option, but to impose a fine.

Please check your financial overview to see the applied changes, and be more careful next time.

@component('mail::button', ['url' => route('wallet.index')])
VIEW FINANCES
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
