@component('mail::message')
# Dear {{ $order->writer->name }},

We discovered a very high similarity index in the order __#{{ $order->id }}__. Therefore, a fine has been
imposed for being in breach of our plagiarism-free policy. Please check your financial overview to see if any
changes have been applied.

@component('mail::button', ['url' => route('wallet.index')])
VIEW FINANCES
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
