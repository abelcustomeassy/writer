@component('mail::message')
# Dear {{ $order->writer->name }},

You have failed to respond to our messages severally. Please be warned that communication is an integral part of our
agreement. We have no option but to impose a fine.

Please check your financial overview to see the applied changes.

@component('mail::button', ['url' => route('wallet.index')])
VIEW FINANCES
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
