@component('mail::message')
# Dear {{ $order->writer->name }},

You have been reassigned from the order __#{{ $order->id }}__. Due to the inconvenience caused, we have been forced to impose a fine.
Please check your financial overview to see the applied changes.

Please ensure that you can handle a paper comfortably before placing your bid next time.

@component('mail::button', ['url' => route('wallet.index')])
VIEW FINANCES
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
