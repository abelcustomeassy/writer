<section class="section">
    <div class="error-card">
        <div class="error-title-block">
            <h1 class="error-title">403</h1>
            <h2 class="error-sub-title">
                Permission Denied.
            </h2>
        </div>
        <div class="error-container">
            <p>Why not try refreshing your page? or you can contact support</p>
            <a class="btn btn-primary" href="{{ route('dashboard') }}">
                <i class="bx bx-angle-left"></i>
                Back to Dashboard
            </a>
        </div>
    </div>
</section>