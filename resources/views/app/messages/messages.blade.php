<div id="messages">
    @forelse($messages as $message)
        <div class="message {{ $message->isUnread($user)?" unread":"" }}">
            <div class="row">
                <div class="col-sm-10">
                    <a class="message-header" href="javascript:" data-parent="#messages"
                       data-toggle="collapse" data-target="#message{{ $message->id }}">
                         <span class="sender">
                            @if($message->sender_id == $user->id)
                                 @if($message->recipient->userType->name == "Employee")
                                     {{ "Me -> ".$message->recipient->employeeProfile->department->name }}
                                 @else
                                     {{ "Me -> ".$message->recipient->userType->name }}
                                 @endif
                             @elseif($message->recipient_id == $user->id)
                                 @if($message->sender->userType->name == "Employee")
                                     {{ $message->sender->employeeProfile->department->name." -> Me" }}
                                 @else
                                     {{ $message->sender->UserType->name." -> Me" }}
                                 @endif
                             @endif
                         </span>
                        <span class="subject">
                            @if($message->order_id)
                                <strong>Order #{{  $message->order_id }}</strong>
                            @endif
                            {!! strip_tags($message->subject,'<strong>') !!}
                         </span>
                        <span class="date">
                            @if($message->flag == $messageFlag::REPLY)
                                <i class="bx bx-reply"></i>
                            @endif
                            @if($message->flag == $messageFlag::BLOCKED)
                                <span class="badge badge-danger"><i class="bx bx-hand-stop-o"></i> Blocked</span>
                                <br>
                            @endif
                            {{ $message->formattedDateTime('created_at') }}
                         </span>
                    </a>
                </div>
                <div class="col-sm-2">
                    @if($message->order_id)
                        <span class="order-id">
                            <a href="{{  route('orders.show',['id' => $message->order_id]) }}">#{{ $message->order_id }}</a>
                        </span>
                    @endif
                </div>
            </div>
            <div id="message{{ $message->id }}" data-message-id="{{ $message->id }}"
                 class="collapse message-body">
                {!!  nl2br(strip_tags($message->content,'<strong><p>')) !!}
                @if($message->flag == $messageFlag::BLOCKED)
                    <div>
                        <small class="text text-danger">
                            <em>This message is against our <a href="{{ url('terms-and-conditions') }}">
                                    Terms & Conditions.</a></em>
                        </small>
                    </div>
                @endif
                @if($message->hasMedia('messages'))
                    <div class="well well-sm margin-top-15">
                        <h6 class="text-muted margin-0"><i class="bx bx-paperclip"></i> Attachments</h6>
                        <ul>
                            @foreach($message->getMedia('messages') as $media)
                                <li>
                                    <a href="{{ route('messages.download',['mediaItem'=> $media->id]) }}">
                                        {{ $media->file_name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                @endif
                @if($message->sender_id != $user->id)
                    <div class="message-actions text-center">
                        <button class="btn btn-primary btn-sm btn-round reply"
                                data-message-id="{{ $message->id }}"
                                data-toggle="modal"
                                data-target="#reply_message">
                            <i class="bx bx-reply font-size-13"></i> Reply
                        </button>
                    </div>
                @endif
            </div>
        </div>
    @empty
        <div class="alert alert-info">
            <div class="alert-icon">
                <i class="bx bx-info-circle"></i>
            </div>
            <span class="alert-text">You don't have any messages.</span>
        </div>
    @endforelse
</div>