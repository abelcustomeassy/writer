@extends('layouts.app')

@section('title','Messages')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
        <div class="card">
            <div class="card-header">
                <div class="text-right">
                    <a href="#" class="btn btn-primary mb-3" data-toggle="modal" data-target="#new_message">
                        <i class="bx bx-edit"></i> New Message
                    </a>
                </div>
            </div>
            <div class="card-body">
                @include('app.messages.messages')
            </div>
        {{ $messages->links() }}
        </div>
    @include('app.messages.new_message')
    @include('app.messages.reply_message')
@endsection