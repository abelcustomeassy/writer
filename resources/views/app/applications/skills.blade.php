@extends('layouts.app')

@section('title','Skills')

@section('sidebar')
    @include('app.nav.application',['completed' => 'profile,education', 'active' => 'skills'])
@endsection

@section('app')
    <form id="skills" method="post" action="{{ route('application-skills') }}">
        @csrf()
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="past_writer" class="control-label">
                                Have you ever worked as on online writer?
                            </label>
                            <select name="past_writer" id="past_writer" class="form-control">
                                <option value="">Select Option</option>
                                <option value="1" {{ old('past_writer') == '1'? 'selected':null }}>Yes
                                </option>
                                <option value="0" {{ old('past_writer') == '0'? 'selected':null }}>No
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="col-lg-6 control-label">
                                Citation styles you are proficient in?
                            </label>
                            <div class="col-lg-6">
                                @foreach($citations as $citation)
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" id="citation{{ $citation->id }}" name="citations[]"
                                               class="custom-control-input"
                                               value="{{ $citation->id }}">
                                        <label for="citation{{ $citation->id }}" class="custom-control-label">
                                            {{ $citation->name }}
                                        </label>
                                    </div>
                                @endforeach
                                <div id="form_citation_error"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <h4>Disciplines</h4>
                    <div id="form_discipline_error"></div>
                    <div class="choices pl-3">
                        <div class="row">
                            @foreach($disciplines as $discipline)
                                <div class="custom-control custom-checkbox col-sm-4">
                                    <input type="checkbox" name="disciplines[]" id="disc{{ $discipline->id }}"
                                           class="custom-control-input"
                                           value="{{ $discipline->id }}">
                                    <label for="disc{{ $discipline->id }}" class="custom-control-label" style="font-size: 11px">
                                        {!! $discipline->name  !!}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-round">
                    <i class="bx bx-save"></i> Save
                </button>
            </div>
        </div>
    </form>
@endsection