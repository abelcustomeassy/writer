@extends('layouts.app')

@section('title','Application Status')

@section('sidebar')
    @include('app.nav.application',['completed' => 'profile,education,skills,language,essay,review', 'active' => 'review'])
@endsection

@section('app')
    @if($application->status == $status::DECLINED)
        <div class="row">
            <div class="col-md-5 offset-md-3">
                <div class="card text-center">
                    <div class="card-body">
                        <div class="card-title">
                            Application Status
                        </div>
                        <div class="status-content">
                            <i class="bx bx-error text-danger bx-lg"></i>
                            <p class="margin-bottom-0 margin-top-15">Sorry, your application was declined.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(($application->status == $status::PENDING || $application->status == $status::PENDING_APPROVAL)
           && $application->step == $step::COMPLETE)
        <div class="row">
            <div class="col-md-5 offset-md-3">
                <div class="card text-center">
                    <div class="card-body">
                        <div class="card-title">
                            Application Status
                        </div>
                        <div class="status-content">
                            <i class="bx bx-check-circle text-success bx-lg"></i>
                            <p>Congratulations, your application is currently
                                being reviewed.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($application->status == $status::APPROVED )
        <div class="row">
            <div class="col-md-5 offset-md-3">
                <div class="card text-center">
                    <div class="card-body">
                        <div class="card-title">
                            Congratulations!!
                        </div>
                        <div class="status-content">
                            <i class="bx bx-check-circle bx-lg text-success"></i>
                            <p class="margin-bottom-0 margin-top-15">Your application was successful.</p>
                            <p>Dive right in and start bidding for orders.</p>
                            <a href="{{  route('dashboard') }}" class="btn btn-primary">
                                <i class="bx bx-home"></i> Dashboard
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection