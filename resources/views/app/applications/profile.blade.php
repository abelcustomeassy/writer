@extends('layouts.app')

@section('title','Profile')

@section('sidebar')
    @include('app.nav.application',['completed' => '', 'active' => 'profile'])
@endsection

@section('app')
    <form id="profile" method="post" action="{{ route('application-profile') }}" enctype="multipart/form-data">
        @csrf()
        <div class="card">
            <div class="card-body">

            
                        
                
                       <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="writer_type">Upload Your Profile Picture</label><br>
                             <img class="img-circle img-fluid" style="height:150px; width: 150px; position: relative;" 
                              src="{{ asset('images/default-avatar.png') }}" id="profileDisplay" placeholder="click to upload">
                               <input type="file" name="avatar" id="avatar" onChange="displayImage(this)" style="display: none;">
                               <button type="button" onClick="triggerClick()" class="btn btn-just-icon btn-primary btn-file">
                                <i class="material-icons"> Attach </i>  </button>


                        </div>
                        
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="writer_type">Writer Type</label>
                            <select name="writer_type" id="writer_type" data-rule-required="true"
                                    class="form-control">
                                <option value="">Select Option</option>
                                <option value="GENERAL">General</option>
                                <option value="TECHNICAL">Technical</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="english">English</label>
                            <select name="english" id="english" data-rule-required="true" class="form-control">
                                <option value="">Select Option</option>
                                <option value="ESL">ESL</option>
                                <option value="ENL">ENL</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="phone">Phone Number</label>
                            <input type="text" name="phone" id="phone" class="form-control">
                            <input type="hidden" name="phone_number" id="phone_number">
                        </div>
                        <div class="col-md-6">
                            <label for="country_code">Country</label>
                            <select name="country_code" id="countryCode" data-rule-required="true"
                                    class="form-control">
                                <option value="">--Select Country--</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                   <div class="row">
                       <div class="col-md-6">
                           <div class="custom-file">
                               <input type="file" class="custom-file-input" id="document" name="document">
                               <label class="custom-file-label" for="document">Your ID Document</label>
                           </div>
                           @if ($errors->has('document'))
                               <span class="text-danger">
                           <strong>{{ $errors->first('document') }}</strong>
                        </span>
                           @endif
                       </div>
                   </div>
                </div>
                <div class="form-group">
                        <textarea name="about" class="form-control" data-rule-required="true"
                                  data-rule-minlength="250"
                                  data-rule-maxlength="1000"
                                  placeholder="About You" rows="4" id="about">
                            </textarea>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-round">
                    <i class="bx bx-save"></i> Save
                </button>
            </div>
        </div>
    </form>
    <script type="text/javascript">
      function triggerClick(e)
{
document.querySelector('#avatar').click();
}
function displayImage(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#profileDisplay').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}

    </script>
@endsection