@extends('layouts.app')

@section('title','Essay Test')

@section('sidebar')
    @include('app.nav.application',['completed' => 'profile,education,skills,language', 'active' => 'essay'])
@endsection

@section('app')
    @if($data['type'] == "evaluation")
        @include('app.applications.evaluations.essay.start')
    @else
        @include('app.applications.evaluations.essay.question')
    @endif
@endsection