<form id="essay-submit" action="{{ route('submit-essay-question',['id' =>  $data['question']->id ]) }}"
      enctype="multipart/form-data" method="post">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <essay :question="{{ $data['question'] }}"></essay>
                    <hr>
                    {!! nl2br($data['question']->description) !!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        Upload Essay
                    </div>
                    @csrf()
                    <div class="form-group">
                        <div class="custom-file{{ $errors->has('essay') ? " is-invalid":"" }}">
                            <input type="file" class="custom-file-input" id="essay" name="essay">
                            <label class="custom-file-label" for="essay">Essay</label>
                        </div>
                        @if ($errors->has('essay'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('essay') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-round">
                            <i class="bx bx-save"></i>
                            Submit Essay
                        </button>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
