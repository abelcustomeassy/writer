<div class="card">
    <div class="card-body">
        <div class="margin-top-15">
            <p>
                {{ $data['userEvaluation']->evaluation->instructions }}
            </p>
            <buttton class="btn btn-primary btn-round"
                     onclick="document.getElementById('start-essay').submit();">
                <i class="bx bx-time"></i>
                Start Test
            </buttton>
            <form id="start-essay" action="{{ route('start-essay-evaluation') }}" method="POST"
                  style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>

    </div>
</div>