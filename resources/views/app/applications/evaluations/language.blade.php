@extends('layouts.app')

@section('title','Language Test')

@section('sidebar')
    @include('app.nav.application',['completed' => 'profile,education,skills', 'active' => 'language'])
@endsection

@section('app')
    <language :evaluation-data="{{ $data }}" start-url="{{ route('start-language-evaluation') }}"
              question-url="{{ route('submit-language-question') }}">
    </language>
@endsection