@extends('layouts.app')

@section('title','Education')

@section('sidebar')
    @include('app.nav.application',['completed' => 'profile', 'active' => 'education'])
@endsection

@section('app')
    <form id="education" method="post" action="{{ route('application-education') }}" enctype="multipart/form-data">
        @csrf()
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="academic_level_id">Academic Level</label>
                            <select name="academic_level_id" id="academic_level_id" class="form-control">
                                <option value="">Select Option</option>
                                @foreach($academic_levels as $level)
                                    <option value="{{ $level->id }}" {{ old('academic_level') == $level->id ? 'selected':null }}>
                                        {{ $level->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="course">Course</label>
                            <input type="text" name="course" id="course" placeholder="Major/Course"
                                   class="form-control"
                                   value="{{ old('course') }}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="school">School</label>
                            <input type="text" name="school" id="school" class="form-control" placeholder="School"
                                   value="{{ old('school') }}"/>
                        </div>
                        <div class="col-md-6">
                            <label for="date_completed">Date Completed</label>
                            <div class="input-group date">
                                <input type="text" name="date_completed" id="date_completed"
                                       class="form-control"
                                       readonly
                                       placeholder="Date Completed" value="{{ old('date_completed') }}"/>
                                <div class="input-group-addon"><i class="bx bx-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="certificate" class="control-label">Upload Certificate</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="certificate" name="certificate">
                                <label class="custom-file-label" for="certificate">Certificate</label>
                            </div>
                            @if ($errors->has('certificate'))
                                <span class="text-danger">
                                                <strong>{{ $errors->first('certificate') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary btn-round">
                    <i class="bx bx-save"></i> Save
                </button>
            </div>
        </div>
    </form>
@endsection