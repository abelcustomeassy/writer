@extends('layouts.app')

@section('title','Dashboard')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('stats')
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <a class="widget-link" href="{{ route('orders.available') }}">
                <div class="card card-stats">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Available Orders</h5>
                                <span class="h2 font-weight-bold mb-0">{{ number_format($availableCount) }}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                    <i class="bx bx-time"></i>
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-nowrap text-darker">Total available orders</span>
                        </p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-md-6">
            <a class="widget-link" href="{{ route('orders.assigned') }}">
                <div class="card card-stats">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Assigned Orders</h5>
                                <span class="h2 font-weight-bold mb-0">{{ $assignedCount }}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-purple text-white rounded-circle shadow">
                                    <i class="bx bxs-hand-right"></i>
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-nowrap text-darker">Orders in progress</span>
                        </p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-md-6">
            <a class="widget-link" href="{{ route('orders.submitted') }}">
                <div class="card card-stats">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Submited Orders</h5>
                                <span class="h2 font-weight-bold mb-0">{{ number_format($submittedCount) }}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                    <i class="bx bx-check-circle"></i>
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-nowrap text-darker">Completed orders</span>
                        </p>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xl-3 col-md-6">
            <a class="widget-link" href="{{ route('wallet.index') }}">
                <div class="card card-stats">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Wallet Balance</h5>
                                <span class="h2 font-weight-bold mb-0">{{ currency() }}{{ number_format($walletBalance,2) }}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                    <i class="bx bx-coin-stack"></i>
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-sm">
                            <span class="text-nowrap text-darker">Pending Payment</span>
                        </p>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection

@section('app')
    <div class="dashboard">
        <div class="row">
            <div class="col-sm-7">
                <div class="card">
                    <div class="card-header">
                        <h4 class="m-0">
                            <i class="bx bx-shopping-bag"></i> Recent Orders
                        </h4>
                    </div>
                    <div class="card-body">
                        @if($latestOrders->count())
                            <table class="table table-striped table-bordered margin-0">
                                <thead>
                                <tr>
                                    <th>Order #</th>
                                    <th>Topic</th>
                                    <th width="10%">Status</th>
                                    <th width="25%">Deadline</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latestOrders as $order)
                                    <tr>
                                        <td>
                                            <a href="{{ route('orders.show',['id'=> $order->id]) }}">
                                                {{ $order->id }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('orders.show',['id'=> $order->id]) }}">
                                                {{ $order->topic }}
                                            </a>
                                        </td>
                                        <td>@include('app.orders.status.full_status')</td>
                                        <td>@include('app.partials.order.deadline')</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                               <span class="alert-icon"><i class="bx bx-info-circle"></i></span>
                               <span class="alert-text">You don't have any orders.</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="card">
                    <div class="card-header">
                        <h4 class="m-0">
                            <i class="bx bx-bell-o"></i> Notifications
                        </h4>
                    </div>
                    <div class="card-body">
                        @if($notifications->count())
                            <div class="notifications">
                                @foreach($notifications as $notification)
                                    <div class="notification{{ $notification->read_at ? "":" unread" }}">
                                        <a href="{{ $notification->data['url'].'?notification='.$notification->id }}">
                                            <p>{{ $notification->data['message'] }}</p>
                                            <small class="time">{{ $notification->formattedDateTime('created_at') }}</small>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="alert alert-info">
                                <span class="alert-icon"><i class="bx bx-info-circle"></i></span>
                                <span class="alert-text">You don't have any notifications.</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection