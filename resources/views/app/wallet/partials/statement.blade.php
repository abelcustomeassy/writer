<div class="table-responsive">
    <table class="table table-striped" id="statementTable">
        <thead>
        <tr>
            <th>Description</th>
            <th>Date</th>
            <th>Amount</th>
        </tr>
        </thead>
    </table>
</div>
@push('scripts')
<script>
    $(document).ready(function () {
        if (!$.fn.dataTable.isDataTable('#statementTable')) {
            $('#statementTable').DataTable({
                dom: 'frtip',
                processing: true,
                serverSide: true,
                autoWidth: false,
                ajax: '{{ route('wallet.statement') }}',
                columns: [
                    {data: 'description', name: 'description',width: '60%'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'amount', name: 'amount'}
                ],
                order: [[1, 'desc']]
            });
        }
    });
</script>
@endpush