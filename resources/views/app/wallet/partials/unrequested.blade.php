<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#Order</th>
            <th>Discipline</th>
            <th>Pages</th>
            <th>Accepted Date</th>
            <th>Amount</th>
        </tr>
        </thead>
        @forelse($eligiblePayments as $eligiblePayment)
            <tr>
                <td>
                    @if($eligiblePayment->payment_type == 'full')
                        <a href="{{ route('orders.show', ['id' => $eligiblePayment->order_id]) }}">
                            {{ $eligiblePayment->order_id }}
                        </a>
                    @else
                        {{ $eligiblePayment->order_id }}
                    @endif
                </td>
                <td>{{ $eligiblePayment->order->discipline->name }}</td>
                <td>{{ $eligiblePayment->order->pages }}</td>
                <td>
                    @if($eligiblePayment->payment_type == 'full')
                        {{ $eligiblePayment->order->formattedDate('accepted_at') }}
                    @else
                        {{ $eligiblePayment->formattedDate('created_at')  }}
                    @endif
                </td>
                <td>{{ currency() }}{{ number_format($eligiblePayment->amount,2) }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="5" align="center">
                    You don't have any unrequested payments
                </td>
            </tr>
        @endforelse
        @if($eligibleTotal = $eligiblePayments->sum('amount'))
            <tr class="{{ $fines->count()?'text-muted':'text-default' }}">
                <th class="text-right font-size-14" colspan="4">
                    Total
                </th>
                <th class="font-size-14">
                    {{ currency() }}{{ number_format($eligiblePayments->sum('amount'), 2) }}
                </th>
            </tr>
        @endif
        @if($fineCount = $fines->count() && $eligibleTotal)
            <tr>
                <th></th>
                <th colspan="4">All Fines</th>
            </tr>
            <tr>
                <td></td>
                <th>Order#</th>
                <th colspan="2">Fine</th>
                <td></td>
            </tr>
            @foreach($fines as $fine)
                <tr>
                    <td></td>
                    <td>
                        <a href="{{ route('orders.show', ['id' => $fine->order_id]) }}">
                            {{ $fine->order_id }}
                        </a>
                    </td>
                    <td>{{ $fine->type }}</td>
                    <td>
                        @if($fine->status == $fine::APPLIED)
                            <button data-fine-id="{{ $fine->id }}"
                                    class="show_fine_removal_modal btn btn-success btn-sm btn-round btn-outline margin-0">
                                <i class="material-icons">close</i> Request Removal
                            </button>
                        @elseif($fine->status == $fine::REMOVAL_REQUESTED)
                            <span class="badge badge-warning">Removal Requested</span>
                        @elseif($fine->status == $fine::REMOVAL_DECLINED)
                            <span class="badge badge-danger">Removal Declined</span>
                        @endif
                    </td>
                    <td><span class="text-danger">-{{ currency() }}{{ number_format($fine->amount, 2) }}</span></td>
                </tr>
            @endforeach
            <tr>
                <th class="text-right text-muted font-size-14" colspan="4">
                    Total
                </th>
                <th class="font-size-14 text-muted">
                    -{{ currency() }}{{ number_format($fineTotal = $fines->sum('amount'), 2) }}
                </th>
            </tr>
            <tr>
                <th class="text-right font-size-16" colspan="4">
                    Available
                </th>
                <th class="font-size-18">
                    {{ currency() }}{{ number_format($eligibleTotal - $fineTotal, 2) }}
                </th>
            </tr>
        @endif
    </table>
</div>
@if($fineCount)
    @include('app.wallet.modals.fine_removal')
@endif