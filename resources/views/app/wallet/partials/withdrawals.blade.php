<div class="table-responsive">
    <table class="table table-striped" id="withdrawalsTable">
        <thead>
        <tr>
            <th></th>
            <th>Reference</th>
            <th>Payment Method</th>
            <th>Status</th>
            <th>Processed At</th>
            <th>Amount</th>
        </tr>
        </thead>
    </table>
</div>