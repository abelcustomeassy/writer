<div class="col-sm-1">
    <div class="payment-icon">
        <i class="bx bxl-paypal"></i>
    </div>
</div>
<div class="col-sm-4">
    <table>
        <tr>
            <td><strong>Name:</strong> {{ $paymentAccount->details->paypal_name }}</td>
        </tr>
        <tr>
            <td><strong>Email:</strong> {{ $paymentAccount->details->paypal_email }}</td>
        </tr>
    </table>
</div>