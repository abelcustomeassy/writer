<div class="col-sm-1">
    <div class="payment-icon">
        <i class="bx bx-mobile-phone"></i>
    </div>
</div>
<div class="col-sm-4">
    <table>
        <tr>
            <td><strong>Name:</strong> {{ $paymentAccount->details->mpesa_name }}</td>
        </tr>
        <tr>
            <td><strong>Phone Number:</strong> {{ $paymentAccount->details->mpesa_number }}</td>
        </tr>
    </table>
</div>