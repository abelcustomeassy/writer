<div class="col-sm-1">
    <div class="payment-icon">
        <i class="bx bx-bank"></i>
    </div>
</div>
<div class="col-sm-4">
    <table>
        <tr>
            <td><strong>Bank:</strong> {{ $paymentAccount->details->bank_name }}</td>
        </tr>
        <tr>
            <td><strong>A/C Name:</strong> {{ $paymentAccount->details->account_name }}</td>
        </tr>
        <tr>
            <td><strong>A/C Number:</strong> {{ $paymentAccount->details->account_number }}</td>
        </tr>
    </table>
</div>