<div class="text-right">
    <button class="btn btn-primary btn-round btn-outline" data-toggle="modal"
            data-target="#createPaymentAccountModal">
        <i class="bx bx-plus"></i>
        Create Account
    </button>
</div>
@forelse($paymentAccounts as $paymentAccount)
    <div class="bg-lighter py-3 mt-3">
        <div class="row">
            @if($paymentAccount->paymentMethod->code == 'paypal')
                @include('app.wallet.partials.accounts.paypal')
            @elseif($paymentAccount->paymentMethod->code == 'bank')
                @include('app.wallet.partials.accounts.bank')
            @elseif($paymentAccount->paymentMethod->code == 'mpesa')
                @include('app.wallet.partials.accounts.mpesa')
            @endif
            <div class="col-sm-3">
                <div class="verification">
                    @if($paymentAccount->verification_code)
                        <span class="badge badge-danger">
                            <i class="bx bx-close"></i> Unverified
                        </span>
                    @else
                        <span class="badge badge-success">
                            <i class="bx bx-check-circle"></i> Verified
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-sm-2">
                @if($paymentAccount->is_default)
                    <div class="status">
                        <span class="badge badge-success">
                           <i class="bx bx-check-circle"></i> Default
                        </span>
                    </div>
                @endif
            </div>
            <div class="col-sm-2">
                @if(!$paymentAccount->is_default)
                    <div class="actions">
                        <div class="btn-group btn-group-xs margin-0">
                            <button class="btn btn-success" data-toggle="tooltip" title="Make default account"
                                    onclick="document.getElementById('form-def-{{ $paymentAccount->id }}').submit()">
                                <i class="bx bx-check-circle"></i>
                            </button>
                            <button class="btn btn-danger" data-toggle="tooltip" title="Delete account"
                                    onclick="document.getElementById('form-del-{{ $paymentAccount->id }}').submit()">
                                <i class="bx bx-trash"></i>
                            </button>
                        </div>
                    </div>
                    <form id="form-def-{{ $paymentAccount->id }}" method="post"
                          action="{{ route('wallet.make-account-default',['id'=>$paymentAccount->id]) }}">
                        @csrf()
                    </form>
                    <form id="form-del-{{ $paymentAccount->id }}" method="post"
                          action="{{ route('wallet.delete-account',['id'=>$paymentAccount->id]) }}">
                        @csrf()
                        @method('delete')
                    </form>
                @endif
            </div>
        </div>
    </div>
@empty
    <div class="alert alert-info mt-3">
        <div class="alert-icon">
            <i class="bx bx-info-circle"></i>
        </div>
        <span class="alert-text">You don't have any accounts set up yet.</span>
    </div>
@endforelse
@include('app.wallet.modals.create_account')
