@if($status == $transactionStatus::PENDING)
    <span class="badge badge-warning">Pending</span>
@elseif($status == $transactionStatus::DECLINED)
    <span class="badge badge-danger">Declined</span>
@elseif($status == $transactionStatus::APPROVED)
    <span class="badge badge-success">Approved</span>
@endif
