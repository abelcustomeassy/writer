@if($status == $transactionStatus::PENDING)
    <span class="badge badge-info">Pending</span>
@endif
@if($status == $transactionStatus::CANCELLED)
    <span class="badge badge-danger">Cancelled</span>
@endif
@if($status == $transactionStatus::FAILED)
    <span class="badge badge-danger">Failed</span>
@endif
@if($status == $transactionStatus::COMPLETE)
    <span class="badge badge-success">Complete</span>
@endif
