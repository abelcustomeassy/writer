<div class="modal" id="withdraw_modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            @if(Auth::user()->hasDefaultAccount())
                <form id="withdraw_form" method="post" action="{{ route('wallet.withdraw') }}">
                    @csrf()
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">
                            <i class="material-icons">clear</i>
                        </button>
                        <h4 class="modal-title">Withdraw</h4>
                    </div>
                    <div class="modal-body">
                        Please note that the withdrawal will be made to:
                        <div class="well margin-top-10 margin-bottom-0">
                            <div class="row">
                                @if($paymentAccount->paymentMethod->code == 'paypal')
                                    @include('app.wallet.partials.accounts.paypal',['paymentAccount' => $paymentAccount])
                                @elseif($paymentAccount->paymentMethod->code == 'bank')
                                    @include('app.wallet.partials.accounts.bank',['paymentAccount' => $paymentAccount])
                                @elseif($paymentAccount->paymentMethod->code == 'mpesa')
                                    @include('app.wallet.partials.accounts.mpesa',['paymentAccount' => $paymentAccount])
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-simple"
                                data-dismiss="modal">Cancel
                        </button>
                        <button type="submit" class="btn btn-primary btn-round">
                            OK
                        </button>
                    </div>
                </form>
            @else
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">
                        <i class="material-icons">clear</i>
                    </button>
                    <h4 class="modal-title">Withdraw</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger alert-outline margin-0">
                        <div class="alert-icon">
                            <i class="material-icons">warning</i>
                        </div>
                        <b>Alert:</b> It looks like you have not set up a verified default payment account.
                        To get paid, go to the account section and update your account details.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-simple"
                            data-dismiss="modal">Close
                    </button>
                </div>
            @endif
        </div>
    </div>
</div>