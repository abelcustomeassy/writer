<div class="modal" id="fine_removal_modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form id="fine_removal_form" method="post" action="{{ route('wallet.remove-fine') }}" class="validate-form">
                @csrf()
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">
                        <i class="material-icons">clear</i>
                    </button>
                    <h4 class="modal-title">Remove Fine</h4>
                </div>
                <div class="modal-body padding-top-0">
                    <input type="hidden" name="fine_id">
                    <div class="form-group margin-top-10">
                        <label for="reason">Reason</label>
                        <textarea name="reason" id="reason" rows="4" data-rule-required="true" class="form-control"
                                  data-rule-maxlength="1000"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-simple"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-round">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>