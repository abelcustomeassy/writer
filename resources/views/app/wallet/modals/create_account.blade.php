<div class="modal" id="createPaymentAccountModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form id="createPaymentAccountForm" method="post" action="{{ route('wallet.create-payment-account') }}">
                @csrf()
                <div class="modal-header">
                    <h4 class="modal-title">Create Account</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group margin-top-0">
                        <label for="paymentMethod">Payment Method</label>
                        <select name="payment_method_code" id="paymentMethod" data-rule-required="true"
                                class="form-control">
                            <option value="">-- Select Option --</option>
                            @foreach($paymentMethods as $paymentMethod)
                                <option value="{{ $paymentMethod->code }}">{{ $paymentMethod->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="paypal" class="payment-method" style="display: none">
                        <div class="form-group">
                            <label for="paypal_name">PayPal Name</label>
                            <input type="text" name="paypal_name" id="paypal_name" data-rule-required="true"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="paypal_email">Paypal Email</label>
                            <input type="email" name="paypal_email" id="paypal_email" data-rule-required="true"
                                   class="form-control">
                        </div>
                    </div>
                    <div id="bank" class="payment-method">
                        <div class="form-group">
                            <label for="bank_name">Bank Name</label>
                            <input type="text" name="bank_name" id="bank_name" data-rule-required="true"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="account_name">A/C Name</label>
                            <input type="text" name="account_name" id="account_name" data-rule-required="true"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="account_number">A/C Number</label>
                            <input type="text" name="account_number" id="account_number" data-rule-required="true"
                                   class="form-control">
                        </div>
                    </div>
                    <div id="mpesa" class="payment-method">
                        <div class="form-group">
                            <label for="mpesa_name">Name</label>
                            <input type="text" name="mpesa_name" id="mpesa_name" data-rule-required="true"
                                   class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="mpesa_number">Phone Number</label>
                            <input type="text" name="mpesa_number" id="mpesa_number" data-rule-required="true"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-simple"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-round">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>