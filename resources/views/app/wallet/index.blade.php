@extends('layouts.app')

@section('title','My wallet')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="card">
        <div class="card-body">
            <div class="wallet">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="balance">
                            <span class="text-muted">Available Balance:</span>
                            <strong class="amount">
                                {{ currency() }}{{ number_format(Auth::user()->wallet->balance,2) }}
                            </strong>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        @if($configs->payments_enabled && Auth::user()->wallet->balance > 0 &&
                        !Auth::user()->hasPendingWithdrawals())
                            <div class="withdraw">
                                <button class="btn btn-round btn-withdraw" data-target="#withdraw_modal"
                                        data-toggle="modal">
                                    <i class="bx bx-money"></i> Withdraw
                                </button>
                            </div>
                            @include('app.wallet.modals.withdraw_account')
                        @endif
                    </div>
                </div>
                <hr class="mt-2 mb-4">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="active nav-link" href="#unrequested"
                                            data-toggle="tab">Unrequested</a></li>
                    <li class="nav-item"><a class="nav-link" href="#withdrawals" data-toggle="tab">Withdrawals</a></li>
                    <li class="nav-item"><a class="nav-link" href="#statement" data-toggle="tab">Statement</a></li>
                    <li class="nav-item"><a class="nav-link" href="#account" data-toggle="tab">Account</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="unrequested">
                        @include('app.wallet.partials.unrequested')
                    </div>
                    <div class="tab-pane pt-3" id="withdrawals">
                        @include('app.wallet.partials.withdrawals')
                    </div>
                    <div class="tab-pane pt-3" id="statement">
                        @include('app.wallet.partials.statement')
                    </div>
                    <div class="tab-pane pt-3" id="account">
                        @include('app.wallet.partials.account')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
