<p class="margin-0 font-size-13">{{ $order->formattedDateTime('writer_deadline') }}</p>
<strong class="text-muted">
    <small>({{ $order->formatInterval('writer_deadline') }})</small>
</strong>