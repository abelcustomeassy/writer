@extends('layouts.app')

@section('title','Notifications')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="card">
        <div class="card-body">
            <div class="notifications">
                @if($notifications->count() > 0)
                    @foreach($notifications as $notification)
                        <div class="notification{{ $notification->read_at ? "":" unread" }}">
                            <a href="{{ $notification->data['url'].'?notification='.$notification->id }}">
                                <p>{{ $notification->data['message'] }}</p>
                                <small class="time">{{ $notification->formattedDateTime('created_at') }}</small>
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-info">
                        <div class="alert-icon">
                            <i class="bx bx-info-circle"></i>
                        </div>
                        <span class="alert-text">You don't have any notifications.</span>
                    </div>
                @endif
            </div>
        </div>
        {{ $notifications->links() }}
    </div>
@endsection