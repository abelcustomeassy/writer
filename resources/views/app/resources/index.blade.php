@extends('layouts.app')

@section('title','Resources')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="card">
        <div class="card-body">
            @forelse($resources as $resource)
                <div class="my-2">
                    <h5 class="m-0">
                        {{ $resource->title }}
                    </h5>
                    <hr class="margin-top-5 margin-bottom-10">
                    <p class="bg-light">{{ $resource->description }}</p>
                    @if($resource->hasMedia())
                        <p>
                            <small class="text-muted">Download:</small>
                            <a href="{{ route('resources.download', ['mediaItem' => $resource->getFirstMedia('resources')->id]) }}">
                                <i class="bx bx-download"></i>
                                <strong>{{ $resource->getFirstMedia('resources')->file_name }}</strong>
                            </a>
                        </p>
                    @endif
                </div>
            @empty
                <div class="padding-5">
                    <div class="alert alert-info alert-outline margin-0">
                    <span class="alert-icon">
                        <i class="bx bx-info-circle"></i>
                    </span>
                        <span class="alert-text">There no resources.</span>
                    </div>
                </div>
            @endforelse
        </div>
        {{ $resources->links() }}
    </div>
@endsection