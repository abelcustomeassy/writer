<hr class="mb-3 mt-0">
<h6 class="navbar-heading p-0 text-muted">
    <span class="docs-normal">Orders</span>
</h6>
<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.available') }}">
            <i class="bx bx-time text-yellow"></i>
            Available
            @if($availableCount)
                <span class="badge badge-warning badge-counter">{{ $availableCount }}</span>
            @endif
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.bids') }}">
            <i class="bx bx-shopping-bag text-dark"></i>
            My Bids
            @if($bidsCount)
                <span class="badge badge-warning badge-counter">{{ $bidsCount }}</span>
            @endif
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.assigned') }}">
            <i class="bx bxs-hand-right text-info"></i>
            Assigned
            @if($assignedCount)
                <span class="badge badge-warning badge-counter">{{ $assignedCount }}</span>
            @endif
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.submitted') }}">
            <i class="bx bx-check-circle text-success"></i>
            Submitted
            @if($submittedCount)
                <span class="badge badge-warning badge-counter">{{ $submittedCount }}</span>
            @endif
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.reviewed') }}">
            <i class="bx bx-edit text-warning"></i>
            Revisions
            @if($reviewedCount)
                <span class="badge badge-warning badge-counter">{{ $reviewedCount }}</span>
            @endif
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.disputed') }}">
            <i class="bx bx-error-alt text-danger"></i>
            Disputed
            @if($disputedCount)
                <span class="badge badge-warning badge-counter">{{ $disputedCount }}</span>
            @endif
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.archived') }}">
            <i class="bx bx-archive text-success"></i>
            Completed
            
             @if($archievedCount)
                <span class="badge badge-warning badge-counter">{{ $archievedCount }}</span>
            @endif
        </a>
    </li>
</ul>
<hr class="my-3">
<h6 class="navbar-heading p-0 text-muted">
    <span class="docs-normal">Information</span>
</h6>
<ul class="navbar-nav mb-md-3">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('announcements.index') }}">
            <i class="bx bx-chat text-warning"></i>
            Announcements
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('resources.index') }}">
            <i class="bx bx-archive-in text-info"></i>
            Resources
        </a>
    </li>
</ul>