<nav class="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
    <div class="container-fluid">
        <ul class="navbar-nav align-items-center ml-md-auto">
          
            <li class="nav-item">
                <a class="nav-link" href="{{ route('wallet.index') }}">
                    <i class="bx bx-wallet"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('notifications') }}">
                    <i class="bx bx-bell"></i>
                    @if($notificationsCount)
                        <span id="notification-counter" class="notification">{{ $notificationsCount }}</span>
                    @endif
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('messages.index') }}">
                    <i class="bx bx-message"></i>
                    @if($messagesCount)
                        <span id="message-counter" class="notification">{{ $messagesCount }}</span>
                    @endif
                </a>
            </li>
            <li class="nav-item dropdown">
                <a href="javascript: void(0)" class="nav-link" data-toggle="dropdown">
                    <div class="writer_name">{{ Auth::user()->name }} <small class="text-warning">(ID: {{ Auth::user()->id }})</small></div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{ route('profile') }}" class="dropdown-item"><i class="bx bx-user"></i> Profile</a>
                    <a href="{{ route('settings') }}" class="dropdown-item"><i class="bx bxs-cog"></i> Settings</a>
                    <div class="dropdown-divider"></div>
                    <a href="javascript: void(0)"  class="dropdown-item"
                       onclick="event.preventDefault(); document.getElementById('logout').submit();">
                        <i class="bx bx-power-off"></i> Log out
                    </a>
                    <form id="logout" method="post" action="{{ route('logout') }}" style="display: none">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
              <li class="nav-item d-xl-none"  style="color: red">
                <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin"
                     data-target="#sidenav-main"  style="color: red">
                    <div class="sidenav-toggler-inner" style="color: red">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div class="header bg-default pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h2 class="text-white d-inline-block mb-0">@yield('title')</h2>
                </div>
            </div>
            @yield('stats')
        </div>
    </div>
</div>