<ul class="navbar-nav">
    <li class="nav-item{{ strpos($completed, 'profile') !== false ?' completed':'' }}{{ $active == 'profile'?' pending':'' }}">
        <a class="nav-link{{ $active == 'profile'?' active':'' }}"
           href="javascript: void(0)">
            <i class="bx bx-user text-primary"></i>
            <span class="nav-link-text">Profile</span>
        </a>
    </li>
    <li class="nav-item{{ strpos($completed,'education') !== false ?' completed':'' }}{{ $active == 'education'?' pending':'' }}">
        <a class="nav-link{{ $active == 'education'?' active':'' }}"
           href="javascript: void(0)">
            <i class="bx bx-book text-orange"></i>
            <span class="nav-link-text">Education</span>
        </a>
    </li>
    <li class="nav-item{{ strpos($completed,'skills') !== false ?' completed':'' }}{{ $active == 'skills'?' pending':'' }}">
        <a class="nav-link{{ $active == 'skills'?' active':'' }}"
           href="javascript: void(0)">
            <i class="bx bx-wrench text-primary"></i>
            <span class="nav-link-text">Skills</span>
        </a>
    </li>
    <li class="nav-item{{ strpos($completed,'language') !== false ?' completed':'' }}{{ $active == 'language'?' pending':'' }}">
        <a class="nav-link{{ $active == 'language'?' active':'' }}"
           href="javascript: void(0)">
            <i class="bx bx-time text-yellow"></i>
            <span class="nav-link-text">Language Test</span>
        </a>
    </li>
    <li class="nav-item{{ strpos($completed,'essay') !== false ?' completed':'' }}{{ $active == 'essay'?' pending':'' }}">
        <a class="nav-link{{ $active == 'essay'?' active':'' }}"
           href="javascript: void(0)">
            <i class="bx bx-edit text-success"></i>
            <span class="nav-link-text">Essay Test</span>
        </a>
    </li>
    <li class="nav-item{{ strpos($completed,'review') !== false ?' completed':'' }}">
        <a class="nav-link{{ $active == 'review'?' active':'' }}"
           href="javascript: void(0)">
            <i class="bx bx-check-shield text-info"></i>
            <span class="nav-link-text">Application Review</span>
        </a>
    </li>
</ul>