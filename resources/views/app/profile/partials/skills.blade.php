@if($user->skill)
    <h4>Citations</h4>
    <ul>
        @foreach($user->skill->selectedCitations() as $citation)
            <li>{{ $citation->name }}</li>
        @endforeach
    </ul>
    <h4>Disciplines</h4>
    <ul>
        @foreach($user->skill->selectedDisciplines() as $discipline)
            <li>{!! $discipline->name  !!}</li>
        @endforeach
    </ul>
@else
    <div class="alert alert-info alert-outline margin-top-10">
        <i class="material-icons">info_outline</i> No record found.
    </div>
@endif