<table class="table table-striped">
    <tr>
        <th>User ID</th>
        <td>{{  $user->id }}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{  $user->email }}</td>
    </tr>
    <tr>
        <th>Phone Number</th>
        <td>{{  $user->phone_number??"N/A" }}</td>
    </tr>
    <tr>
        <th>Country</th>
        <td>{{ $user->country->name?? "N/A" }}</td>
    </tr>
    <tr>
        <th>Timezone</th>
        <td>{{ $user->timezone??"N/A" }}</td>
    </tr>
    <tr>
        <th>English</th>
        <td>{{  $user->writerProfile->english??"N/A" }}</td>
    </tr>
    <tr>
        <th>Writer Type</th>
        <td>{{  $user->writerProfile->writer_type??"N/A" }}</td>
    </tr>
    <tr>
        <th>Writer Level</th>
        <td><span class="badge badge-info">{{  $user->writerProfile->writerLevel->name??"N/A" }}</span></td>
    </tr>
    <tr>
        <td colspan="2" width="90%">
            <h4 class="margin-top-0 margin-bottom-15">About Writer</h4>
            {!! nl2br(strip_tags($user->writerProfile->about,'<p><strong>')) !!}
        </td>
    </tr>
</table>
