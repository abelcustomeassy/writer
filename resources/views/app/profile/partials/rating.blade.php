<table class="table table-striped">
    <tr>
        <th>Client Rating</th>
        <td width="10%">{{  $clientRating }}</td>
    </tr>
    <tr>
        <th>Editor Rating</th>
        <td width="10%">{{  $editorRating }}</td>
    </tr>
    <tr>
        <th>Average Rating</th>
        <td width="10%">{{  $clientRating + $editorRating }}</td>
    </tr>
</table>
<hr>
<h3 class="mb-2">Order Stats</h3>


<table class="table table-striped">
    <tr>
        <th>Client Requests</th>
        <td width="10%">{{  $clientRequestsCount }} of {{ $totalOrders }}</td>
    </tr>
    <tr>
        <th>Revisions Count</th>
        <td width="10%">{{  $revisionCount }} of {{ $totalOrders }}</td>
    </tr>
    <tr>
        <th>Lateness Count</th>
        <td width="10%">{{  $latenessCount }} of {{ $totalOrders }}</td>
    </tr>
    <tr>
        <th>Dispute Count</th>
        <td width="10%">{{ $disputeCount }} of {{ $totalOrders }}</td>
    </tr>
</table>
