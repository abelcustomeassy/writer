@extends('layouts.app')

@section('title','Profile')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="card">
        <div class="card-body">
            <div class="profile">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="d-flex">
                            <div class="avatar">
                                @if($user->hasMedia('avatars'))
                                    <img class="img-circle img-fluid"
                                         src="{{ asset($user->getFirstMediaUrl('avatars')) }}">
                                @else
                                    <img class="img-circle img-fluid"
                                         src="{{ asset('images/default-avatar.png') }}">
                                @endif
                            </div>
                            <div class="profile-info">
                                <h3 class="margin-top-0 margin-bottom-0">{{ $user->name }}</h3>
                                <div class="user_type">
                                    <i class="bx bx-briefcase"></i>
                                    {{ $user->userType->name }}
                                </div>
                                <div class="status">
                                    @include('app.profile.status')
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="text-right">
                            <button class="btn btn-primary" data-toggle="modal"
                                    data-target="#editProfile">
                                <i class="bx bx-edit"></i> Edit Profile
                            </button>
                        </div>
                    </div>
                </div>
                <hr class="margin-bottom-0">
                <div class="full-profile">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" href="#general" data-toggle="tab">General</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="#skills" data-toggle="tab">Skills</a></li>
                        <li class="nav-item"><a class="nav-link" href="#ratingDetails" data-toggle="tab">Rating</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" class="tab-pane active">
                            @include('app.profile.partials.general')
                        </div>
                        <div id="skills" class="tab-pane">
                            @include('app.profile.partials.skills')
                        </div>
                        <div id="ratingDetails" class="tab-pane">
                            @include('app.profile.partials.rating')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="editProfile" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="editProfileForm" method="post" action="{{ route('profile.update') }}">
                    @csrf()
                    @method('PUT')
                    <div class="modal-header">
                        <h4>Edit Profile</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group margin-top-0">
                            <label for="phone">Phone Number</label>
                            <input id="phone" type="text"
                                   data-rule-required="true"
                                   value="{{ $user->phone_number }}"
                                   class="form-control">
                            <input id="phoneNumber" type="hidden" name="phoneNumber">
                        </div>
                        <div class="form-group">
                            <label for="countryCode">Country</label>
                            <select id="countryCode" type="text" name="countryCode"
                                    data-rule-required="true"
                                    class="form-control">
                                <option value="">Select Country</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="timezone">Timezone</label>
                            <select id="timezone" type="text" name="timezone"
                                    data-rule-required="true"
                                    class="form-control">
                                <option value="">Select Timezone</option>
                                @foreach($timezones as $timezone)
                                    <option value="{{ $timezone }}"
                                            {{ $timezone == $user->timezone ? "selected":"" }}>
                                        {{ $timezone }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-simple"
                                data-dismiss="modal">Close
                        </button>
                        <button type="submit" class="btn btn-primary btn-round">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection