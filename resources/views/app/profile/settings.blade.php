@extends('layouts.app')

@section('title','Profile Settings')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <form id="change-password" action="{{ route('update-password') }}" method="post">
        @csrf()
        @method('put')
        <div class="row">
            <div class="col-sm-6 col-sm-offset-2">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('current_password')?" has-error":"" }}">
                            <label for="current_password">Current Password</label>
                            <input type="password" name="current_password" class="form-control">
                            @if ($errors->has('current_password'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('current_password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('new_password')?" has-error":"" }}">
                            <label for="current_password">New Password</label>
                            <input type="password" id="new_password" name="new_password" class="form-control">
                            @if ($errors->has('new_password'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('new_password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('new_password_confirmation')?" has-error":"" }}">
                            <label for="current_password">Current Password</label>
                            <input type="password" name="new_password_confirmation" class="form-control">
                            @if ($errors->has('new_password_confirmation'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">
                            <i class="bx bx-save"></i> Change Password
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection