@if($user->status == $user::SUSPENDED)
    <i class="bx bx-circle text-danger"></i> Suspended
@elseif($user->status == $user::DEACTIVATED)
    <i class="bx bx-circle text-danger"></i> Deactivated
@else
    <i class="bx bx-circle text-success"></i> Active
@endif