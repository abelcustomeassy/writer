@if($order->status == $status::AVAILABLE)
    <span class="badge badge-warning">Pending</span>
@elseif($order->status == $status::CANCELLED)
    <span class="badge badge-danger">Cancelled</span>
@else
    <span class="badge badge-success">Assigned</span>
@endif