@if($order->status == $status::PENDING_CONFIRMATION)
    <span class="badge badge-danger">
        <i class="bx bx-close"></i>
    </span>
@else
    <span class="badge badge-success">
        <i class="bx bx-check-circle"></i>
    </span>
@endif