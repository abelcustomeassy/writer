@if($order->status == $status::UNPAID)
    <span class="badge badge-danger">Unpaid</span>
@elseif($order->status == $status::AVAILABLE)
    <span class="badge badge-warning">Available</span>
@elseif($order->status == $status::PENDING_CONFIRMATION)
    <span class="badge badge-info">Confirmation</span>
@elseif($order->status == $status::ASSIGNED)
    <span class="badge badge-warning">Assigned</span>
@elseif($order->status == $status::REASSIGNMENT_REQUESTED)
    <span class="badge badge-warning">Reassignment</span>
@elseif($order->status == $status::SUBMITTED)
    <span class="badge badge-warning">Submitted</span>
@elseif($order->status == $status::FORWARDED)
    <span class="badge badge-success">Forwarded</span>
@elseif($order->status == $status::ACCEPTED)
    <span class="badge badge-success">Accepted</span>
@elseif($order->status == $status::PENDING_CLIENT_REVIEW_CONFIRMATION)
    <span class="badge badge-success">Confirmation</span>
@elseif($order->status == $status::REVIEW_BY_CLIENT)
    <span class="badge badge-warning">Revision</span>
@elseif($order->status == $status::PENDING_ADMIN_REVIEW_CONFIRMATION)
    <span class="badge badge-warning">Confirmation</span>
@elseif($order->status == $status::REVIEW_BY_ADMIN)
    <span class="badge badge-warning">Revision</span>
@elseif($order->status == $status::CLIENT_REVIEW_SUBMISSION)
    <span class="badge badge-warning">Review</span>
@elseif($order->status == $status::ADMIN_REVIEW_SUBMISSION)
    <span class="badge badge-danger">Revision</span>
@elseif($order->status == $status::DISPUTED)
    <span class="badge badge-danger">Disputed</span>
@elseif($order->status == $status::PENDING_REFUND)
    <span class="badge badge-danger">Disputed</span>
@elseif($order->status == $status::REFUNDED)
    <span class="badge badge-danger">Refunded</span>
@elseif($order->status == $status::REFUND_REVIEW)
    <span class="badge badge-danger">Disputed</span>
@elseif($order->status == $status::CANCELLED)
    <span class="badge badge-danger">Cancelled</span>
@elseif($order->status == $status::DEADLINE_EXTENSION_REQUESTED)
    <span class="badge badge-danger">Deadline Extension</span>
@elseif($order->status == $status::REVIEW_REASSIGNMENT_REQUESTED)
    <span class="badge badge-danger">Reassignment</span>
@elseif($order->status == $status::ADMIN_REVIEW_REASSIGNMENT_REQUESTED)
    <span class="badge badge-danger">Reassignment</span>
@elseif($order->status == $status::REVISION_DEADLINE_EXTENSION_REQUESTED)
    <span class="badge badge-danger">Deadline Extension</span>
@elseif($order->status == $status::ADMIN_REVISION_DEADLINE_EXTENSION_REQUESTED)
    <span class="badge badge-danger">Deadline Extension</span>
@elseif($order->status == $status::PARTIAL_REFUND)
    <span class="badge badge-danger">Partial Refund</span>
@elseif($order->status == $status::AUTO_ACCEPTED)
    <span class="badge badge-danger">Accepted</span>
@elseif($order->status == $status::MANUALLY_ACCEPTED)
    <span class="badge badge-danger">Accepted</span>
@endif