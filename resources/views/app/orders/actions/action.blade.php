<div class="row">
    @if($order->status == $status::AVAILABLE)
        @if(!$order->myBids->count())
            <div class="col-sm-6">
                <button class="btn btn-success btn-sm"
                        onclick="document.getElementById('request-order').submit()">
                    Apply Order
                </button>
                <form id="request-order" style="display: none"
                      action="{{ route('orders.request',['id' => $order->id]) }}"
                      method="post">
                    @csrf()
                </form>
            </div>
        @else
            <div class="col-sm-6" >
                <button class="btn btn-success btn-sm"
                        onclick="document.getElementById('cancel-bid').submit()">
                    Cancel Request
                </button>
                <form id="cancel-bid" style="display: none"
                      action="{{ route('orders.cancel-request',['id' => $order->id]) }}"
                      method="post">
                    @csrf()
                </form>
            </div>
        @endif
        @if(Auth::user()->hasTakes() && $order->canBeTaken())
            <div class="col-sm-6">
                <button class="btn btn-info btn-sm" onclick="document.getElementById('take-order').submit()">
                    Take Order
                </button>
                <form id="take-order" style="display: none"
                      action="{{ route('orders.take',['id' => $order->id]) }}"
                      method="post">
                    @csrf()
                </form>
            </div>
        @endif
    @elseif($order->status == $status::PENDING_CONFIRMATION)
        <div class="col-sm-6">
            <button class="btn btn-success btn-sm"
                    onclick="document.getElementById('confirm-order').submit()">
                Confirm Order
            </button>
            <form action="{{ route('orders.confirm', ['id' => $order->id]) }}" method="post" id="confirm-order">
                @csrf()
            </form>
        </div>
        <div class="col-sm-6">
            <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#reject_order">
                Reject Order
            </button>
            @include('app.orders.modals.reject_order')
        </div>
    @elseif($order->status == $status::PENDING_CLIENT_REVIEW_CONFIRMATION)
        <div class="col-sm-6">
            <button class="btn btn-success btn-sm"
                    onclick="document.getElementById('confirm-client-review').submit()">
                Confirm Revision
            </button>
            <form action="{{ route('orders.confirm-client-review', ['id' => $order->id]) }}" method="post"
                  id="confirm-client-review">
                @csrf()
            </form>
        </div>
    @elseif($order->status == $status::PENDING_ADMIN_REVIEW_CONFIRMATION)
        <div class="col-sm-6">
            <button class="btn btn-success btn-sm"
                    onclick="document.getElementById('confirm-admin-review').submit()">
                Confirm Revision
            </button>
            <form action="{{ route('orders.confirm-admin-review', ['id' => $order->id]) }}" method="post"
                  id="confirm-admin-review">
                @csrf()
            </form>
        </div>
    @elseif($order->status == $status::ASSIGNED || $order->status == $status::REVIEW_BY_CLIENT ||
        $order->status == $status::REVIEW_BY_ADMIN || $order->status == $status::REASSIGNMENT_REQUESTED ||
        $order->status == $status::DEADLINE_EXTENSION_REQUESTED || $order->status == $status::REVISION_DEADLINE_EXTENSION_REQUESTED ||
        $order->status == $status::ADMIN_REVISION_DEADLINE_EXTENSION_REQUESTED)
        <div class="col-sm-6">
            <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#submit-order">
                Submit Order
            </button>
            @include('app.orders.modals.submit_order')
        </div>
        <div class="col-sm-6">
            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#reassign_order">
                Reassign Order
            </button>
            @include('app.orders.modals.reassign_order')
        </div>
        <div class="col-sm-6">
            <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#deadline_extension">
                Deadline Extension
            </button>
            @include('app.orders.modals.deadline_extension')
        </div>
    @else
        <div class="col-sm-12">
            <small class="text-muted">No action required.</small>
        </div>
    @endif
</div>
