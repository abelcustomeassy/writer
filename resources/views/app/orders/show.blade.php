@extends('layouts.app')
@section('title', '#'.$order->id.'-'.$order->topic)

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="row order-details">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <h3 class="margin-top-0">
                       #{{ $order->id }}
                        <div class="status">
                            @include('app.orders.status.full_status')
                        </div>
                    </h3>
                    <hr class="margin-0">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" href="#details" data-toggle="tab">Details</a></li>
                        <li class="nav-item">
                            <a class="nav-link" href="#order-files" data-toggle="tab">
                                @if($order->fileCount())
                                    <span class="badge badge-default">{{ $order->fileCount() }}</span>
                                @endif
                                Files
                            </a>
                        </li>
                        <li class="nav-item"><a  class="nav-link" href="#messages" data-toggle="tab">Messages</a></li>
                        @if($order->revisions->count())
                            <li class="nav-item"><a class="nav-link" href="#revisions" data-toggle="tab">Revisions</a></li>
                        @endif
                        @if($order->disputes->count())
                            <li class="nav-item"><a class="nav-link" href="#dispute" data-toggle="tab">Dispute</a></li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div id="details" class="tab-pane active">
                            @include('app.orders.partials.details')
                        </div>
                        <div id="order-files" class="tab-pane">
                            @include('app.orders.partials.files')
                        </div>
                        <div id="messages" class="tab-pane">
                            @include('app.orders.partials.messages')
                        </div>
                        @if($order->revisions->count())
                            <div id="revisions" class="tab-pane">
                                @include('app.orders.partials.revisions')
                            </div>
                        @endif
                        @if($order->disputes->count())
                            <div id="dispute" class="tab-pane">
                                @include('app.orders.partials.disputes')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    @if($order->status != $status::AVAILABLE)
                        <h4 class="text-center">
                            Price:{{ currency() }}{{ $order->writer_amount }}
                        </h4>
                    @else
                        <h4 class="text-center">Price:{{ currency() }}{{ $writerAmount }}</h4>
                    @endif
                </div>
            </div>
            @if(Auth::user()->status != \App\Models\User::SUSPENDED || $order->status != $status::AVAILABLE )
                <div class="card margin-top-10">
                    <div class="card-body">
                        <div class="card-title">
                            <h4 class="margin-0 margin-bottom-10">Order Actions</h4>
                            <hr class="margin-0 margin-bottom-10">
                        </div>
                        <div class="actions">
                            @include('app.orders.actions.action')
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
