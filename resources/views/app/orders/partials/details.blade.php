<table class="table table-bordered table-striped margin-top-10">
    <tbody>
    <tr>
        <td width="25%"><strong>Topic</strong></td>
        <td>{{ $order->topic }}</td>
    </tr>
    <tr>
        <td><strong>Academic Level:</strong></td>
        <td> {{ $order->academiclevel->name }}</td>
    </tr>
    <tr>
        <td><strong>Discipline:</strong></td>
        <td>{{ $order->discipline->name }}</td>
    </tr>
    <tr>
        <td><strong>Paper Type:</strong></td>
        <td>{{ $order->paperType->name }}</td>
    </tr>
    <tr>
        <td><strong>Deadline:</strong></td>
        <td>@include('app.partials.order.deadline')</td>
    </tr>
    <tr>
        <td><strong># of Pages</strong>:</td>
        <td>{{ $order->pages }}</td>
    </tr>
    <tr>
        <td><strong>Spacing</strong>:</td>
        <td> {{ $order->spacing }}</td>
    </tr>
    <tr>
        <td><strong># of Words</strong>:</td>
        <td> {{ $order->spacing == "double"?300 * $order->pages:300 * $order->pages * 2 }}</td>
    </tr>
    <tr>
        <td><strong># of Sources</strong>:</td>
        <td> {{ $order->sources?:"Not Specified" }}</td>
    </tr>
    @if(isset($order->citation->name))
        <tr>
            <td><strong>Citation</strong>:</td>
            <td> {{ $order->citation->name }}</td>
        </tr>
    @else
        <tr>
            <td><strong>Citation</strong>:</td>
            <td> {{ $order->citation_id }}</td>
        </tr>
    @endif
    @if($order->ppt_slides)
        <tr>
            <td><strong>PPT Slides</strong>:</td>
            <td><span class="badge badge-default font-size-12">{{ $order->ppt_slides }}</span></td>
        </tr>
        <tr>
            <td><strong>Speaker Notes</strong>:</td>
            <td><span class="badge badge-default font-size-12">{{ $order->has_speaker_notes ? 'Yes':'No' }}</span></td>
        </tr>
    @endif
    @if($order->charts)
        <tr>
            <td><strong>Charts</strong>:</td>
            <td><span class="badge badge-default font-size-12">{{ $order->charts }}</span></td>
        </tr>
    @endif
    @if($order->requires_digital_references)
        <tr>
            <td><strong>Digital References</strong>:</td>
            <td><span class="badge badge-default font-size-12">Yes</span></td>
        </tr>
    @endif
    @if($order->plagiarism_report)
        <tr>
            <td><strong>Plagiarism Report</strong>:</td>
            <td><span class="badge badge-default font-size-12">Yes</span></td>
        </tr>
    @endif
    @if($order->grammar_report)
        <tr>
            <td><strong>Grammar Report</strong>:</td>
            <td><span class="badge badge-default font-size-12">Yes</span></td>
        </tr>
    @endif
    @if($order->requires_enl_writer)
        <tr>
            <td><strong>ENL Writer</strong>:</td>
            <td><span class="badge badge-default font-size-12">Yes</span></td>
        </tr>
    @endif
    @if($order->related_orders)
        <tr>
            <td><strong>Related Orders</strong>:</td>
            <td>
                @foreach($order->related_orders as $relatedOrder)
                    <a href="{{ route('orders.show',['id'=>$relatedOrder]) }}">
                        <span class="badge badge-default font-size-12">#{{ $relatedOrder }}</span>
                    </a>
                @endforeach
            </td>
        </tr>
    @endif
    <tr>
        <td colspan="2">
            <h4 class="margin-top-0">Instructions</h4>
            {!! nl2br(strip_tags($order->instructions,'<strong><p>')) !!}
        </td>
    </tr>
    </tbody>
</table>
