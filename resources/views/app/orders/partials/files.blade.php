<table class="table files">
    <tr>
        <th>File</th>
        <th width="25%">Who Uploaded</th>
    </tr>
    @forelse($order->getMedia('orders') as $media)
        <tr>
            <td>
                <a href="{{ route('orders.download',['mediaItem' => $media->id]) }}">
                    <p class="margin-0">#{{ $order->id }}_{{ \Illuminate\Support\Str::limit($media->file_name,30) }}</p>
                    <small class="text-muted">
                        @if($media->hasCustomProperty('order_files'))
                            {{ $media->getCustomProperty('order_files')  }}
                        @endif
                    </small>
                </a>
            </td>
            <td>
                <p class="margin-0">
                    {{ $media->getCustomProperty('uploader') }}
                </p>
                <small class="text-muted">
                    {{ $media->updated_at }}
                </small>
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="2" align="center">There are no files for this order.</td>
        </tr>
    @endforelse
</table>