@if(Auth::user()->status != \App\Models\User::SUSPENDED || $order->status != $status::AVAILABLE )
    <div class="text-right mt-3">
        <a href="#" class="btn btn-round btn-sm btn-primary btn-outline" data-toggle="modal" data-target="#new_message">
            <i class="bx bx-edit"></i> New Message
        </a>
    </div>
    <hr class="mt-2">
    @include('app.messages.new_message', [
        'orderId' => $order->id,
        'sendClient'=> $order->writer_id == Auth::user()->id,
    ])
    @include('app.messages.reply_message', ['orderId' => $order->id])
@endif
@include('app.messages.messages',[
   'messages' => $order->userMessages,
   'user' => Auth::user(),
   'messageFlag' => new \App\Common\MessageFlag()
])

