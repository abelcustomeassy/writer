<div class="modal" id="reassign_order" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="reassign_order_form" action="{{ route('orders.reassign',['id' => $order->id]) }}"
                  class="validate-form" method="post">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Re-assign Order</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group margin-top-0">
                        <label for="reason" class="control-label">Reason</label>
                        <textarea data-rule-required="true" name="reason" id="reason" rows="4"
                                  class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-simple"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-round">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
