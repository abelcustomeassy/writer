<div class="modal" id="reject_order" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('orders.reject', ['id' => $order->id]) }}" method="post" id="reject_order_form"
                  class="validate-form" method="post">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Reject Assignment</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group margin-top-0">
                        <label for="reason" class="control-label">Reason</label>
                        <textarea data-rule-required="true" name="reason" id="reason" rows="4"
                                  class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-simple"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-round">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
