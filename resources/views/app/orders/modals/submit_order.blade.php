<div class="modal" id="submit-order" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="submit-order-form" action="{{ route('orders.submit',['id' => $order->id]) }}" method="post"
                  enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Upload Files</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="submissions" name="submissions[]" multiple>
                            <label class="custom-file-label" for="submissions">Choose file</label>
                        </div>
                    </div>
                    <div class="submissions-list"></div>
                    @if($order->requires_digital_references)
                        <div class="form-group">
                            <label for="srcs">Digital Sources</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="sources" name="sources[]" multiple>
                                <label class="custom-file-label" for="sources">Choose file</label>
                            </div>
                        </div>
                        <div class="sources-list"></div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-simple"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-round">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
