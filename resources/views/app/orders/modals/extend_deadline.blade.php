<div class="modal" id="extend_deadline" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="deadline_extension_form" action="{{ route('orders.extend-deadline',['id' => $order->id]) }}"
                  class="validate-form" method="post">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Extend Deadline</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group margin-top-0">
                        <div class="row">
                            <div class="col-sm-8">
                                <label for="time_value">Time</label>
                                <input type="text" name="time_value" id="time_value" class="form-control"
                                       data-rule-required="true"
                                       data-rule-digits="true">
                            </div>
                            <div class="col-sm-4">
                                <div class="margin-top-25">
                                    <select name="time_type" id="time_type" class="form-control">
                                        <option value="minutes">Minutes</option>
                                        <option value="hours">Hours</option>
                                        <option value="days">Days</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message" class="control-label">Message</label>
                        <textarea data-rule-required="true" name="message" id="message" rows="4"
                                  class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-simple"
                            data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary btn-round">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
