@extends('layouts.app')
@section('title', $title)

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table(['class' => 'table table-striped']) !!}
            </div>
        </div>
    </div>
@stop
@include('app.partials.data_tables.js_files')