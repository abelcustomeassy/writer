@if($order->status == $status::UNPAID)
    <span class="badge badge-danger">Unpaid</span>
@elseif($order->status == $status::AVAILABLE)
    <span class="badge badge-warning">Pending</span>
@elseif($order->status == $status::ASSIGNED)
    <span class="badge badge-warning">Assigned</span>
@elseif($order->status == $status::SUBMITTED)
    <span class="badge badge-warning">Assigned</span>
@elseif($order->status == $status::FORWARDED)
    <span class="badge badge-warning">Submitted</span>
@elseif($order->status == $status::ACCEPTED)
    <span class="badge badge-success">Accepted</span>
@elseif($order->status == $status::AUTO_ACCEPTED)
    <span class="badge badge-success">Accepted</span>
@elseif($order->status == $status::MANUALLY_ACCEPTED)
    <span class="badge badge-success">Accepted</span>
@elseif($order->status == $status::REVIEW_BY_CLIENT)
    <span class="badge badge-warning">Review</span>
@elseif($order->status == $status::REVIEW_BY_ADMIN)
    <span class="badge badge-warning">Assigned</span>
@elseif($order->status == $status::REVIEW_SUBMISSION)
    <span class="badge badge-warning">Assigned</span>
@elseif($order->status == $status::REVISE_REVIEW_BY_ADMIN)
    <span class="badge badge-warning">Assigned</span>
@elseif($order->status == $status::DISPUTED)
    <span class="badge badge-danger">Disputed</span>
@elseif($order->status == $status::REFUNDED)
    <span class="badge badge-info">Refunded</span>
@endif