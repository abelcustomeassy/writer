@extends('layouts.app')

@section('title','Announcements')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="row">
        <div class="col-md-8 col-md-offset-2 margin-top-15">
            @if($isRead = $announcement->isUnread())
            <div class="alert alert-warning alert-outline margin-0 margin-bottom-5">
                <div class="alert-icon">
                    <i class="material-icons">warning</i>
                </div>
                <p class="bold">You must read and confirm the announcement below for you to navigate to other pages.</p>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <h3 class="margin-0">{{ $announcement->title }}</h3>
                    </div>
                    <div class="content">
                        {!! $announcement->message !!}
                    </div>
                    <hr>
                    <p>Regards,</p>
                    <strong>{{ $announcement->department->name }}</strong><br>
                    <span class="text-muted"><i
                                class="bx bx-calendar"></i>{{ $announcement->formattedDateTime('created_at') }}</span>
                </div>
                @if($isRead)
                    <div class="card-footer">
                        <button class="btn btn-primary btn-round" onclick="document.getElementById('confirm').submit()">
                            <i class="bx bx-check-circle"></i> Confirm
                        </button>
                        <form id="confirm" style="display: none" method="post"
                              action="{{ route('announcements.confirm',['id' => $announcement->id ]) }}">
                            @csrf()
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection