@extends('layouts.app')

@section('title','Announcements')

@section('sidebar')
    @include('app.nav.sidebar')
@endsection

@section('app')
    <div class="card">
        <div class="card-body">
            <div class="notifications">
                @forelse($announcements as $announcement)
                    <hr class="margin-0">
                    <div class="padding-5 margin-left-10">
                        <h5 class="margin-0">
                            <a href="{{ route('announcements.show', ['slug' => $announcement->slug]) }}">
                                <p class="margin-0">{{ $announcement->title }}</p>
                                <small class="time">{{ $announcement->formattedDateTime('created_at') }}</small>
                            </a>
                        </h5>
                    </div>
                @empty
                    <div class="alert alert-info alert-outline margin-0">
                        <span class="alert-icon">
                            <i class="bx bx-info-circle"></i>
                        </span>
                        <span class="alert-text">There no announcements.</span>
                    </div>
                @endforelse
            </div>
        </div>
        {{ $announcements->links() }}
    </div>
@endsection