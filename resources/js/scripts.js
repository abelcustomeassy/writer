/**
 * Get the form validations
 */
require('./forms');

/**
 * Get messages javascript
 */
require('./messages/messages');

/**
 * Activate some defaults
 */
$(document).ready(function () {
    //  Activate the Tooltips
    $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip();

    // Activate Popovers
    $('[data-toggle="popover"]').popover();

    // Activate select 2
    $('select.form-control').select2();

    // Validate form on select
    $('select').on('select2:select', function (e) {
        $(this).valid();
    });

    // Add validation to form
    $("form.validate-form").validate();


    $('.timezone').val(moment.tz.guess());

    $("input[type=file]").change(function () {
        let fileName = $(this).val();
        fileName = fileName.replace("C:\\fakepath\\", "");
        $(this).next('.custom-file-label').html(fileName);
    });

    // Make order rows click able.
    $("body").on("click", ".dataTable .dt-order-item", function () {
        window.location = $(this).data("href");
    });

    // Handle setting the active class on menu
    let url = window.location;
    $('ul.nav a').filter(function () {
        return this.href == url;
    }).parent().addClass('active');

    $(".input-group.date").datepicker({autoclose: true, format: 'yyyy-mm-dd'});

    $(document).ajaxError(function (event, jqxhr, settings, exception) {
        if (exception === 'Unauthorized') {
            Swal({
                title: "Session Time Out",
                text: "Your session has timed out, Please log in again.",
                type: "warning",
            }).then(() => {
                window.location.replace('/login');
            });
        } else {
            console.log(exception);
        }
    });

    $.fn.dataTable.ext.errMode = 'none';
});

 if($('.collapse').length){
        var hash = window.location.hash;

        if(hash.length>0){
            $(hash+".collapse").addClass('show')
    }
   
        }
        