<?php

namespace App\Models\Orders;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class OrderAssignment extends Model
{
    /**
     * Status showing the user is the current assigned user of the order.
     */
    const ASSIGNED = 1;

    /**
     * Status showing the user was de-assigned the order.
     */
    const DEASSIGNED = 0;

    /**
     * The order associated with the assignment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * The user associated with the assignment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
