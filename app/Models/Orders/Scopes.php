<?php

namespace App\Models\Orders;

use App\Common\OrderStatus;
use Illuminate\Database\Eloquent\Builder;

trait Scopes
{
    public function scopeCompleted(Builder $builder)
    {
        return $builder->where(function (Builder $query) {
            $query->where('status', OrderStatus::ACCEPTED)
                ->orWhere('status', OrderStatus::AUTO_ACCEPTED)
                ->orWhere('status', OrderStatus::MANUALLY_ACCEPTED)
                ->orWhere('status', OrderStatus::PARTIAL_REFUND)
                ->orWhere('status', OrderStatus::REFUNDED);
        });
    }
}
