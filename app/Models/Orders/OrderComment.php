<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    /**
     * Attributes that can't be mass assigned.
     *
     * @var array
     */
    protected $guarded = [];
}
