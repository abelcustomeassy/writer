<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderWriterPricing extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'beginner' => 'object',
        'intermediate' => 'object',
        'elite' => 'object',
        'advanced' => 'object',
    ];

    /**
     * The order the pricing is related to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function order()
    {
        return $this->belongsTo(Order::class);
    }
}
