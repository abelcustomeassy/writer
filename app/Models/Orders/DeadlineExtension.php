<?php

namespace App\Models\Orders;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class DeadlineExtension extends Model
{
    /**
     * Status for a deadline requests pending approval.
     */
    const PENDING = 0;

    /**
     * Status for a deadline requests that has been approved.
     */
    const APPROVED = 1;

    /**
     * Status for a deadline requests that have been declined.
     */
    const DECLINED = 2;

    /**
     * Attributes that can't be mass assigned.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['processed_at'];

    /**
     * The order associated with the deadline extension.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class)->withDefault();
    }

    /**
     * The writer associated with the deadline extension.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function writer()
    {
        return $this->belongsTo(User::class)->withDefault();
    }
}
