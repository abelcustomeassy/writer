<?php

namespace App\Models\Orders;

use Illuminate\Database\Eloquent\Model;

class OrderBid extends Model
{
    /**
     * Attributes that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'comments',
    ];
}
