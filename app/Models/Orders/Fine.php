<?php

namespace App\Models\Orders;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Fine extends Model
{
    /**
     * The fine has been applied to the user.
     */
    const APPLIED = 1;

    /**
     * The fine has been deducted from payments.
     */
    const COMPLETE = 2;

    /**
     * The fine has been removed.
     */
    const REMOVED = 3;

    /**
     * The writer has requested removal of fine.
     */
    const REMOVAL_REQUESTED = 4;

    /**
     * The writer request for removal was declined.
     */
    const REMOVAL_DECLINED = 5;

    /**
     * Attributed that can't be mass assigned.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if (Auth::check()) {
            self::addGlobalScope('user', function (Builder $builder) {
                $builder->where('user_id', Auth::user()->id);
            });
        }
    }

    /**
     * Get all the applied fines.
     *
     * @param $query
     * @return mixed
     */
    public function scopeApplicable($query)
    {
        return $query->where(function ($query) {
            $query->where('status', self::APPLIED)->orWhere('status', self::REMOVAL_REQUESTED)
                ->orWhere('status', self::REMOVAL_DECLINED);
        })->get();
    }

    /**
     * Fines that have not been completed by a withdrawal.
     *
     * @param $query
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->where(function ($query) {
            $query->where('status', self::APPLIED)
                ->orWhere('status', self::REMOVAL_DECLINED);
        })->get();
    }

    /**
     * The user associated with the fine.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The order associated with the fine.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
