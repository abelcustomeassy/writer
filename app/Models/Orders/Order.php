<?php

namespace App\Models\Orders;

use App\Models\User;
use App\Traits\FormatsDates;
use App\Common\MessageStates;
use App\Traits\Orders\Counters;
use App\Models\Messages\Message;
use App\Common\ApplicationStatus;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use App\Models\Configurations\Citation;
use App\Models\Configurations\PaperType;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Configurations\Discipline;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use App\Models\Configurations\AcademicLevel;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Order extends Model implements HasMedia
{
    use Counters, HasMediaTrait, FormatsDates, Scopes;

    /**
     * Attributes that should be converted to native types.
     *
     * @var array
     */
    protected $casts = ['related_orders' => 'array'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['accepted_at', 'writer_deadline', 'taken_at'];

    /**
     * Boot the model with some defaults.
     */
    public static function boot()
    {
        parent::boot();

        self::addGlobalScope('validOrder', function (Builder $builder) {

            if (Auth::check()) {
                self::addWriterConstraint($builder);
            }

            $builder->where('paid', true);
        });
    }

    /**
     * Add writer constraint.
     *
     * @param Builder $builder
     */
    protected static function addWriterConstraint(Builder $builder)
    {
        if (Auth::user()->application->status == ApplicationStatus::APPROVED) {
            self::addWriterTypeConstraints($builder);
        }
    }

    /**
     * Add constraints to an approved writer.
     *
     * @param $builder
     */
    public static function addWriterTypeConstraints(Builder $builder)
    {
        if (Auth::user()->writerProfile->writer_type == 'GENERAL') {
            $builder->whereHas('discipline', function ($query) {
                $query->where('is_technical', 0);
            });
        } else {
            $builder->whereHas('discipline', function ($query) {
                $query->where('is_technical', 1);
            });
        }
    }

    /**
     * The order academic level.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function academicLevel()
    {
        return $this->belongsTo(AcademicLevel::class)->withDefault();
    }

    /**
     * The citation for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function citation()
    {
        return $this->belongsTo(Citation::class)->withDefault();
    }

    /**
     * The paper type the order belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paperType()
    {
        return $this->belongsTo(PaperType::class)->withDefault();
    }

    /**
     * The discipline for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discipline()
    {
        return $this->belongsTo(Discipline::class)->withDefault();
    }

    /**
     * The client who created the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(User::class, 'client_id')->withDefault();
    }

    /**
     * The writer assigned to the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function writer()
    {
        return $this->belongsTo(User::class, 'writer_id')->withDefault();
    }

    /**
     * The employee assigned to the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id')->withDefault();
    }

    /**
     * All the bids for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bids()
    {
        return $this->hasMany(OrderBid::class);
    }

    /**
     * Get the bids for the authenticated user.
     *
     * @return mixed
     */
    public function myBids()
    {
        return $this->bids()->whereUserId(Auth::user()->id);
    }

    /**
     * Get all the messages for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * Cache the media on the object.
     *
     * @param string $collectionName
     *
     * @return mixed
     */
    public function loadMedia(string $collectionName)
    {
        $collection = $this->exists ? $this->media : collect($this->unAttachedMediaLibraryItems)->pluck('media');

        return $collection->filter(function (Media $mediaItem) use ($collectionName) {
            if ($collectionName == '') {
                return true;
            }

            return $mediaItem->collection_name === $collectionName;
        })->sortBy('order_column', null, true)->values();
    }

    /**
     * Get user specific messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userMessages()
    {
        return $this->messages()->whereHas('states', function ($query) {
            $query->whereUserId(Auth::user()->id)->where(function ($query) {
                $query->where('state', MessageStates::UNREAD)->orWhere('state', MessageStates::READ)->orWhere('state', MessageStates::OWN);
            });
        })->orderBy('created_at', 'DESC');
    }

    /**
     * Order revision or dispute comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(OrderComment::class);
    }

    /**
     * Get the comments concerning disputes.
     *
     * @return mixed
     */
    public function disputes()
    {
        return $this->comments()->whereType('DISPUTE');
    }

    /**
     * Get the comments concerning revisions.
     *
     * @return mixed
     */
    public function revisions()
    {
        return $this->comments()->whereType('REVISION');
    }

    /**
     * Get the refund details for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function refund()
    {
        return $this->hasOne(Refund::class);
    }

    /**
     * Get the fine details for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fines()
    {
        return $this->hasMany(Fine::class);
    }

    /**
     * Activity logs related to the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activityLogs()
    {
        return $this->hasMany(OrderActivityLog::class);
    }

    /**
     * The writer pricing for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function writerPricing()
    {
        return $this->hasOne(OrderWriterPricing::class)->withDefault();
    }

    /**
     * Get the deadline extensions for the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function deadlineExtension()
    {
        return $this->hasOne(DeadlineExtension::class)
            ->where('status', DeadlineExtension::PENDING)
            ->where('user_id', Auth::user()->id);
    }

    /**
     * Get the file count for the order relevant to the user.
     *
     * @return bool|int
     */
    public function fileCount()
    {
        if (!$this->hasMedia('orders')) {
            return false;
        }

        return $this->getMedia('orders')->count();
    }

    /**
     * Determine whether to allow a take for an order.
     *
     * @return bool
     */
    public function canBeTaken()
    {
        if (date('Y-m-d H:i:s') > $this->writer_deadline) {
            return false;
        }

        return true;
    }
}
