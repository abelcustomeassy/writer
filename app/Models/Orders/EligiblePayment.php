<?php

namespace App\Models\Orders;

use App\Models\User;
use App\Traits\FormatsDates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class EligiblePayment extends Model
{
    use FormatsDates;

    /**
     * Eligible payments that have not been requested.
     */
    const PENDING = 0;

    /**
     * Eligible payments that have been requested.
     */
    const REQUESTED = 1;

    /**
     * Eligible payments that have been processed.
     */
    const PROCESSED = 2;

    /**
     * Eligible payments that are now invalid.
     */
    const INVALIDATED = 3;

    /**
     * Attributes that can't be mass assigned.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::addGlobalScope('user', function (Builder $builder) {
            $builder->where('user_id', Auth::user()->id);
        });
    }

    /**
     * Get the pending eligible orders.
     *
     * @param $query
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->where('status', self::PENDING)->get();
    }

    /**
     * Get the pending eligible orders.
     *
     * @param $query
     * @return mixed
     */
    public function scopeRequested($query)
    {
        return $query->where('status', self::REQUESTED)->get();
    }

    /**
     * The user associated with the eligible order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The order details associated with the record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
