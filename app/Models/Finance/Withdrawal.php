<?php

namespace App\Models\Finance;

use App\Models\User;
use App\Traits\FormatsDates;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Configurations\PaymentMethod;

class Withdrawal extends Model
{
    use FormatsDates;

    /**
     * Withdrawal is pending approval.
     */
    const PENDING = 0;

    /**
     * Withdrawal has been approved and paid.
     */
    const APPROVED = 1;

    /**
     * Withdrawal has been declined.
     */
    const DECLINED = 2;

    /**
     * Attributes that can't be mass assigned.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['processed_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'payment_method_details' => 'object',
        'payment_items' => 'object',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::addGlobalScope('writer', function (Builder $builder) {
            $builder->where('user_id', Auth::user()->id);
        });
    }

    /**
     * The user associated with the payment request.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Payment method associated with the withdrawal.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }
}
