<?php

namespace App\Models\Finance;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Configurations\PaymentMethod;

class PaymentAccount extends Model
{
    /**
     * Attributes that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'payment_method_id',
        'details',
        'is_default',
        'verification_code',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'object',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::addGlobalScope('user', function (Builder $builder) {
            $builder->where('user_id', Auth::user()->id);
        });
    }

    /**
     * The user for the payment account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Payment method for the account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    /**
     * Verify the new payment account.
     *
     * @return bool
     */
    public function verify()
    {
        $this->verification_code = null;

        return $this->save();
    }

    /**
     * Make the account default for receiving payments.
     *
     * @return bool
     */
    public function makeDefault()
    {
        $this->is_default = true;

        return $this->save();
    }

    /**
     * Make the account default for receiving payments.
     *
     * @return bool
     */
    public function removeDefault()
    {
        $this->is_default = false;

        return $this->save();
    }
}
