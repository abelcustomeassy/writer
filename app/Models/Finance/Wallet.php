<?php

namespace App\Models\Finance;

use App\Models\Finance\WalletTransaction AS Transaction;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    /**
     * Add funds to the wallet
     *
     * @param $amount
     * @return bool
     */
    public function credit($amount)
    {
        $this->balance += $amount;
        return $this->save();
    }

    /**
     * Remove funds from the wallets
     *
     * @param $amount
     * @return bool
     */
    public function debit($amount)
    {
        $this->balance -= $amount;
        return $this->save();
    }

    /**
     * Wallet deposit transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
