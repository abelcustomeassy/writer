<?php

namespace App\Models\Finance;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * Status for a pending transaction
     */
    const PENDING = 0;

    /**
     * Status for a complete transaction
     */
    const COMPLETE = 1;

    /**
     * Status for a failed transaction
     */
    const FAILED = 2;


    /**
     * Status for a cancelled transaction
     */
    const CANCELLED = 3;

    /**
     * Attributes that can be mass assigned
     *
     * @var array
     */
    protected $fillable = [
        'reference',
        'type',
        'description',
        'amount',
        'local_reference',
        'user_id',
        'website_id',
    ];

    /**
     * Boot the model with some defaults
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($transaction) {
            $transaction->maker_id = Auth::check() ? Auth::user()->id : "SYSTEM";
        });
    }

    /**
     * Attach different models to a transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function attachable()
    {
        return $this->morphTo();
    }

    /**
     * Mark a transaction as pending
     *
     * @return bool
     */
    public function pending()
    {
        $this->status = self::PENDING;
        return $this->save();
    }

    /**
     * Complete a transaction
     *
     * @return bool
     */
    public function complete()
    {
        $this->status = self::COMPLETE;
        return $this->save();
    }


    /**
     * Mark a transaction as failed
     *
     * @return bool
     */
    public function failed()
    {
        $this->status = self::FAILED;
        return $this->save();
    }

    /**
     * Cancel a transaction
     *
     * @return bool
     */
    public function cancelled()
    {
        $this->status = self::CANCELLED;
        return $this->save();
    }
}
