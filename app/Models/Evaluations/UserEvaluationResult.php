<?php

namespace App\Models\Evaluations;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class UserEvaluationResult extends Model implements HasMedia
{
    use HasMediaTrait;

    /**
     * Attributes that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'started_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['started_at', 'submitted_at'];

    /**
     * Boot the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($result) {
            $result->user_id = Auth::user()->id;
        });

        static::addGlobalScope('test', function (Builder $builder) {
            $builder->where('user_id', Auth::user()->id);
        });
    }
}
