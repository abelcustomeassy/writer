<?php

namespace App\Models\Evaluations;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The question's options.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany(QuestionOption::class);
    }
}
