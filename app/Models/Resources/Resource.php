<?php

namespace App\Models\Resources;

use App\Models\Configurations\Website;
use App\Traits\FormatsDates;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Resource extends Model implements HasMedia
{
    use FormatsDates, HasMediaTrait;

    /**
     * Websites that should display the resource.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function websites()
    {
        return $this->belongsToMany(Website::class, 'resource_websites');
    }

    /**
     * Get all resources for the website.
     *
     * @return mixed
     */
    public static function resources()
    {
        return self::whereHas('websites', function ($query) {
            $query->where('websites.id',websiteId());
        })->orderBy('created_at', 'DESC')->paginate(5);
    }
}
