<?php

namespace App\Models;

use App\Common\OrderStatus;
use App\Traits\User\HasRole;
use App\Models\Orders\Order;
use App\Models\Finance\Wallet;
use App\Models\Applications\Skill;
use App\Models\Finance\Withdrawal;
use App\Traits\Messages\HasMessages;
use App\Models\Finance\PaymentAccount;
use App\Models\Configurations\Website;
use App\Models\Configurations\Country;
use App\Models\Applications\Application;
use Illuminate\Notifications\Notifiable;
use App\Models\Notifications\Notification;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, HasMediaTrait, HasMessages, HasRole;

    /**
     * Status for a deactivated account.
     */
    const DEACTIVATED = 0;

    /**
     * Status for a activated account.
     */
    const ACTIVATED = 1;

    /**
     * Status for a suspended account.
     */
    const SUSPENDED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_number',
        'country_code',
        'timezone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Boot the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->validation_code = Str::random(30);
            $user->website_id = websiteId();
            $user->user_type_id = userTypeId();
        });
    }

    /**
     * The website the user belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    /**
     * Activate the user account.
     *
     * @return bool
     */
    public function acceptEmail()
    {
        $this->validation_code = null;

        return $this->save();
    }

    /**
     * Get the users's orders.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'writer_id');
    }

    /**
     * The application made by the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function application()
    {
        return $this->hasOne(Application::class)->withDefault();
    }

    /**
     * The profile for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function writerProfile()
    {
        return $this->hasOne(WriterProfile::class);
    }

    /**
     * The profile for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employeeProfile()
    {
        return $this->hasOne(EmployeeProfile::class);
    }

    /**
     * The User skills.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function skill()
    {
        return $this->hasOne(Skill::class);
    }

    /**
     * Check if the user has any takes left.
     *
     * @return bool
     */
    public function hasTakes()
    {
        if (! $takes = $this->writerProfile->writerLevel->takes) {
            return false;
        }

        $activeOrders = $this->orders()->where(function ($query) {
            $query->where('status', '<>', OrderStatus::SUBMITTED)
                  ->where('status', '<>', OrderStatus::FORWARDED)
                  ->where('status', '<>', OrderStatus::CANCELLED)
                  ->where('status', '<>', OrderStatus::ACCEPTED)
                  ->where('status', '<>', OrderStatus::AUTO_ACCEPTED)
                  ->where('status', '<>', OrderStatus::MANUALLY_ACCEPTED)
                  ->where('status', '<>', OrderStatus::DISPUTED)
                  ->where('status', '<>', OrderStatus::REFUNDED)
                  ->where('status', '<>', OrderStatus::PENDING_REFUND)
                  ->where('status', '<>', OrderStatus::PARTIAL_REFUND)
                  ->where('status', '<>', OrderStatus::REFUND_REVIEW)
                  ->where('status', '<>', OrderStatus::ADMIN_REVIEW_SUBMISSION)
                  ->where('status', '<>', OrderStatus::CLIENT_REVIEW_SUBMISSION);
        })->count();

        return $activeOrders < $takes;
    }

    /**
     * Get all the system administrators.
     *
     * @return mixed
     */
    public static function administrators()
    {
        return self::whereUserTypeId(adminUserTypeId())->whereNull('validation_code')->whereStatus(self::ACTIVATED);
    }

    /**
     * All the payment accounts for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentAccounts()
    {
        return $this->hasMany(PaymentAccount::class)->orderBy('is_default', 'DESC');
    }

    /**
     * Get the users wallet.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    /**
     * Check if the user has a default payment account.
     *
     * @return bool
     */
    public function hasDefaultAccount()
    {
        return $this->paymentAccounts()->where('is_default', 1)->whereNull('verification_code')->exists();
    }

    /**
     * Get the default payment account for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function paymentAccount()
    {
        return $this->hasOne(PaymentAccount::class)->where('is_default', 1)->whereNull('verification_code');
    }

    /**
     * All payment requests made by the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class);
    }

    /**
     * Check if the user has a pending payment request.
     *
     * @return bool
     */
    public function hasPendingWithdrawals()
    {
        return $this->withdrawals()->where('status', Withdrawal::PENDING)->exists();
    }

    /**
     * Country associated with the profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_code', 'code')->withDefault();
    }

    /**
     * Get the entity's notifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable')->orderBy('created_at', 'desc');
    }
}
