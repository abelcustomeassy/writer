<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Models\Configurations\WriterLevel;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class WriterProfile extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $guarded = [];

    /**
     * Boot the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($profile) {
            $profile->user_id = Auth::user()->id;
        });

    }

    /**
     * Writer level associated with the profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function writerLevel()
    {
        return $this->belongsTo(WriterLevel::class)->withDefault();
    }
}
