<?php

namespace App\Models\Applications;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    /**
     * The user who made the application.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
