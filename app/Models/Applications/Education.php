<?php

namespace App\Models\Applications;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Education extends Model implements HasMedia
{
    use HasMediaTrait;

    /**
     * Attributes that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'academic_level_id',
        'school',
        'course',
        'date_completed',
    ];

    /**
     * Boot the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($education) {
            $education->user_id = Auth::user()->id;
        });
    }
}
