<?php

namespace App\Models\Announcements;

use App\Models\User;
use App\Traits\FormatsDates;
use Illuminate\Support\Facades\Auth;
use App\Models\Configurations\Website;
use Illuminate\Database\Eloquent\Model;
use App\Models\Configurations\Department;
use Illuminate\Database\Eloquent\Builder;

class Announcement extends Model
{
    use FormatsDates;

    /**
     * Check if the user has read the announcement.
     *
     * @return Announcement
     */
    public static function unread()
    {
        return self::whereDoesntHave('users', function ($query) {
            $query->where('users.id', Auth::user()->id);
        })->where('created_at', '>', Auth::user()->created_at)->first();
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::addGlobalScope('website', function (Builder $builder) {
            $builder->whereHas('websites', function ($query) {
                $query->where('websites.id', websiteId());
            });
        });
    }

    /**
     * Websites associated with the announcement.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function websites()
    {
        return $this->belongsToMany(Website::class, 'website_announcements');
    }

    /**
     * Check if the announcement has been read by the current user.
     *
     * @return bool
     */
    public function isUnread()
    {
        return ! $this->users()->where('users.id', Auth::user()->id)->exists();
    }

    /**
     * Users that have read the announcement.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_announcements')->withTimestamps();
    }

    /**
     * The department the announcement is from.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
