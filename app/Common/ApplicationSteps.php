<?php

namespace App\Common;


class ApplicationSteps
{
    /**
     * The total number of steps
     */
    const TOTAL = 6;

    /**
     * Profile page
     */
    const PROFILE = 1;


    /**
     * Education page
     */
    const EDUCATION = 2;


    /**
     * Education page
     */
    const SKILLS = 3;

    /**
     * LANGUAGE evaluation
     */
    const LANGUAGE_EVALUATION = 4;

    /**
     * ESSAY evaluation
     */
    const ESSAY_EVALUATION = 5;

    /**
     * The application is complete
     */
    const COMPLETE = 6;

}
