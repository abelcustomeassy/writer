<?php

namespace App\Rating;

use App\Common\OrderStatus;
use App\Models\Orders\Order;
use App\Models\Ratings\Rating;
use App\Models\User;

class Rater
{
    public static function getTotalOrders(User $user)
    {
        return Order::query()
            ->withoutGlobalScopes()
            ->where('writer_id', $user->id)
            ->where(function ($query) {
                $query->where('status', OrderStatus::ACCEPTED)
                    ->orWhere('status', OrderStatus::AUTO_ACCEPTED)
                    ->orWhere('status', OrderStatus::MANUALLY_ACCEPTED)
                    ->orWhere('status', OrderStatus::PARTIAL_REFUND)
                    ->orWhere('status', OrderStatus::REFUNDED);
            })
            ->count();
    }

    public static function getClientRating(User $user)
    {
        $totalRating = Rating::query()
            ->where(function ($query) {
                $query->where('type', 'client_mark')
                    ->orWhere('type', 'auto_client_mark');
            })
            ->whereHas('order', function ($query) {
                $query->where('status', OrderStatus::ACCEPTED)
                    ->orWhere('status', OrderStatus::AUTO_ACCEPTED)
                    ->orWhere('status', OrderStatus::MANUALLY_ACCEPTED)
                    ->orWhere('status', OrderStatus::PARTIAL_REFUND)
                    ->orWhere('status', OrderStatus::REFUNDED);
            })
            ->where('user_id', $user->id)
            ->sum('value');

        if ($totalOrders = self::getTotalOrders($user)) {
            return round($totalRating / $totalOrders, 2);
        }

        return 0.00;
    }

    public static function getEditorRating(User $user)
    {
        $totalRating = Rating::query()
            ->where(function ($query) {
                $query->where('type', '<>', 'client_mark')
                    ->where('type', '<>', 'auto_client_mark');
            })
            ->whereHas('order', function ($query) {
                $query->where('status', OrderStatus::ACCEPTED)
                    ->orWhere('status', OrderStatus::AUTO_ACCEPTED)
                    ->orWhere('status', OrderStatus::MANUALLY_ACCEPTED)
                    ->orWhere('status', OrderStatus::PARTIAL_REFUND)
                    ->orWhere('status', OrderStatus::REFUNDED);
            })
            ->where('user_id', $user->id)
            ->sum('value');

        if ($totalOrders = self::getTotalOrders($user)) {
            return round($totalRating / $totalOrders, 2);
        }

        return 0.00;
    }
}
