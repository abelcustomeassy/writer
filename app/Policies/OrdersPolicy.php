<?php

namespace App\Policies;

use App\Common\OrderStatus;
use App\Models\Orders\Order;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrdersPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Orders\Order $order
     * @return mixed
     */
    public function view(User $user, Order $order)
    {
        return $user->id == $order->writer_id || $order->status == OrderStatus::AVAILABLE;
    }

    /**
     * Determine whether the user can update an order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Orders\Order $order
     * @return mixed
     */
    public function update(User $user, Order $order)
    {
        return $user->id == $order->writer_id;
    }
}
