<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateUploadLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate symbolic links for the site file uploads.';

    /**
     * The base path for the application.
     *
     * @var string
     */
    protected $basePath;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->basePath = base_path();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createUploadsDirectory();

        $this->createAvatarsDirectory();

        $this->createAvatarsSymLink();

        $this->createPublicAvatarsSymLink();

        $this->createOrdersDirectory();

        $this->createOrdersSymLink();

        $this->createCertsDirectory();

        $this->createCertsSymLink();

        $this->createPublicCertsSymLink();

        $this->createFeaturedDirectory();

        $this->createFeaturedSymLink();

        $this->createPublicFeaturedSymLink();

        $this->createEssaysDirectory();

        $this->createEssaysSymLink();

        $this->createDocumentsDirectory();

        $this->createDocumentsSymLink();

        $this->createMessagesDirectory();

        $this->createMessagesSymLink();

        $this->createResourcesDirectory();

        $this->createResourcesSymLink();
    }

    /**
     * Create the uploads directory outside the application.
     */
    protected function createUploadsDirectory()
    {
        if (file_exists($this->basePath.'/../uploads')) {
            return $this->error('Uploads directory already exists.');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads');

        return $this->info('Uploads directory created successfully.');
    }

    /**
     * Create the avatars directory outside the application.
     */
    protected function createAvatarsDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/avatars')) {
            return $this->error('Avatars directory already exists.');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/avatars');

        return $this->info('Avatars directory created successfully.');
    }

    /**
     * Create the avatars symbolic link.
     */
    protected function createAvatarsSymLink()
    {
        if (file_exists(storage_path('app/avatars'))) {
            return $this->error('The sym link to the avatars directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/avatars', storage_path('app/avatars')
        );

        return $this->info('Sym link to avatars directory created successfully.');
    }

    /**
     * Create public avatars directory symlink.
     */
    protected function createPublicAvatarsSymLink()
    {
        if (file_exists(public_path('avatars'))) {
            return $this->error('The "public/avatars" directory already exists.');
        }

        $this->laravel->make('files')->link(
            storage_path('app/avatars'), public_path('avatars')
        );

        $this->info('The [public/avatars] directory has been linked.');
    }

    /**
     * Create the certs directory.
     */
    protected function createCertsDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/certs')) {
            return $this->error('Main Certs directory already exists.');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/certs');

        return $this->info('Main Certs directory created successfully.');
    }

    /**
     * Create the certs symbolic link.
     */
    protected function createCertsSymLink()
    {
        if (file_exists(storage_path('app/certs'))) {
            return $this->error('The sym link to the certs directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/certs', storage_path('app/certs')
        );

        return $this->info('Sym link to certs directory created successfully.');
    }

    /**
     * Create the certs symbolic link.
     */
    protected function createPublicCertsSymLink()
    {
        if (file_exists(public_path('certs'))) {
            return $this->error('The "public/certs" directory already exists.');
        }

        $this->laravel->make('files')->link(
            storage_path('app/certs'), public_path('certs')
        );

        $this->info('The [public/certs] directory has been linked.');
    }

    /**
     * Create the orders directory outside the application.
     */
    protected function createOrdersDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/orders')) {
            return $this->error('The main Orders directory already exists');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/orders');

        return $this->info('Orders directory created successfully.');
    }

    /**
     * Create the orders symbolic link.
     */
    protected function createOrdersSymLink()
    {
        if (file_exists(storage_path('app/orders'))) {
            return $this->error('The sym link to the orders directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/orders', storage_path('app/orders')
        );

        return $this->info('Sym link to orders directory created successfully.');
    }

    /**
     * Create the Featured directory outside the application.
     */
    protected function createFeaturedDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/featured')) {
            return $this->error('Featured directory already exists.');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/featured');

        return $this->info('Featured directory created successfully.');
    }

    /**
     * Create the orders symbolic link.
     */
    protected function createFeaturedSymLink()
    {
        if (file_exists(storage_path('app/featured'))) {
            return $this->error('The sym link to the Featured directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/featured', storage_path('app/featured')
        );

        return $this->info('Sym link to Featured directory created successfully.');
    }

    /**
     * Create public avatars directory symlink.
     */
    protected function createPublicFeaturedSymLink()
    {
        if (file_exists(public_path('featured'))) {
            return $this->error('The "public/featured" directory already exists.');
        }

        $this->laravel->make('files')->link(
            storage_path('app/featured'), public_path('featured')
        );

        $this->info('The [public/featured] directory has been linked.');
    }

    /**
     * Create the essay directory outside the application.
     */
    protected function createEssaysDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/essays')) {
            return $this->error('Essays directory already exists.');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/essays');

        return $this->info('Essays directory created successfully.');
    }

    /**
     * Create the essays symbolic link.
     */
    protected function createEssaysSymLink()
    {
        if (file_exists(storage_path('app/essays'))) {
            return $this->error('The sym link to the essays directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/essays', storage_path('app/essays')
        );

        return $this->info('Sym link to essays directory created successfully.');
    }

    /**
     * Create the documents directory outside the application.
     */
    protected function createDocumentsDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/documents')) {
            return $this->error('Documents directory already exists.');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/documents');

        return $this->info('Documents directory created successfully.');
    }

    /**
     * Create the documents symbolic link.
     */
    protected function createDocumentsSymLink()
    {
        if (file_exists(storage_path('app/documents'))) {
            return $this->error('The sym link to the documents directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/documents', storage_path('app/documents')
        );

        return $this->info('Sym link to documents directory created successfully.');
    }

    /**
     * Create the orders directory outside the application.
     */
    protected function createMessagesDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/messages')) {
            return $this->error('The main Messages directory already exists');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/messages');

        return $this->info('Messages directory created successfully.');
    }

    /**
     * Create the orders symbolic link.
     */
    protected function createMessagesSymLink()
    {
        if (file_exists(storage_path('app/messages'))) {
            return $this->error('The sym link to the messages directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/messages', storage_path('app/messages')
        );

        return $this->info('Sym link to messages directory created successfully.');
    }

    /**
     * Create the resources directory outside the application.
     */
    protected function createResourcesDirectory()
    {
        if (file_exists($this->basePath.'/../uploads/resources')) {
            return $this->error('The main Resources directory already exists');
        }

        $this->laravel->make('files')->makeDirectory($this->basePath.'/../uploads/resources');

        return $this->info('Resources directory created successfully.');
    }

    /**
     * Create the resources symbolic link.
     */
    protected function createResourcesSymLink()
    {
        if (file_exists(storage_path('app/resources'))) {
            return $this->error('The sym link to the resources directory already exists.');
        }

        $this->laravel->make('files')->link(
            $this->basePath.'/../uploads/resources', storage_path('app/resources')
        );

        return $this->info('Sym link to resources directory created successfully.');
    }
}
