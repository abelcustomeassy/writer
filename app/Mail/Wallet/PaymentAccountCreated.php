<?php

namespace App\Mail\Wallet;

use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use App\Models\Finance\PaymentAccount;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentAccountCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var PaymentAccount
     */
    public $paymentAccount;

    /**
     * Create a new message instance.
     *
     * @param PaymentAccount $paymentAccount
     */
    public function __construct(PaymentAccount $paymentAccount)
    {
        $this->paymentAccount = $paymentAccount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.wallet.payment_account_created');
    }
}
