<?php

namespace App\Traits\Notifications;

use App\Models\User;
use App\Models\Orders\Fine;
use Illuminate\Http\Request;
use App\Models\Messages\Message;
use Illuminate\Http\JsonResponse;
use App\Notifications\Fines\FineRemoved;
use App\Models\Applications\Application;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Messages\NewMessageReceived;
use App\Notifications\Application\ApplicationDeclined;
use App\Notifications\Application\ApplicationApproved;
use App\Traits\Notifications\Orders\HandlesOrderNotifications;
use App\Traits\Notifications\Users\HandlesAccountNotifications;

trait ReceivesRemoteNotifications
{
    use HandlesOrderNotifications, HandlesAccountNotifications;

    /**
     * Notification the writer of a approved notification.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function applicationApproved(Request $request)
    {
        $application = Application::findOrFail($request->application_id);

        Notification::send($application->user, new ApplicationApproved($application));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a declined application.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function applicationDeclined(Request $request)
    {
        $application = Application::findOrFail($request->application_id);

        Notification::send($application->user, new ApplicationDeclined($application));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify a user of a new message via email.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newMessage(Request $request)
    {
        $message = Message::findOrFail($request->message_id);
        $user = $message->recipient;

        if ($message->recipient_id != $request->recipient_id) {
            $user = User::findOrFail($request->recipient_id);
        }

        Notification::send($user, new NewMessageReceived($message));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer a fine has been removed.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fineRemoved(Request $request)
    {
        $fine = Fine::findOrFail($request->fine_id);

        Notification::send($fine->user, new FineRemoved($fine));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }
}
