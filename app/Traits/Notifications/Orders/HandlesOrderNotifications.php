<?php

namespace App\Traits\Notifications\Orders;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Orders\Order;
use Illuminate\Http\JsonResponse;
use App\Notifications\Orders\OrderRevised;
use App\Notifications\Orders\OrderDisputed;
use App\Notifications\Orders\NewFilesAdded;
use App\Notifications\Orders\OrderAssigned;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Orders\OrderReassigned;
use App\Notifications\Orders\OrderBidApproved;
use App\Notifications\Orders\NewOrderAvailable;
use App\Notifications\Fines\WrongFileFineApplied;
use App\Notifications\Fines\PlagiarismFineApplied;
use App\Notifications\Fines\ReassignmentFineApplied;
use App\Notifications\Fines\IgnoredMessagesFineApplied;
use App\Notifications\Fines\DeadlineExtensionFineApplied;

trait HandlesOrderNotifications
{
    /**
     * Notify the writer of a direct order assignment.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function orderAssigned(Request $request)
    {
        $order = Order::find($request->order_id);

        Notification::send($order->writer, new OrderAssigned($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a revision request.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function orderRevised(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new OrderRevised($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer the bid made for the order has been approved.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function orderBidApproved(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new OrderBidApproved($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of the addition of new files.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function newFilesAdded(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new NewFilesAdded($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a disputed order.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function orderDisputed(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new OrderDisputed($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of order reassignment.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function orderReassigned(Request $request)
    {
        $user = User::findOrFail($request->user_id);

        Notification::send($user, new OrderReassigned($request->order_id));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a plagiarism fine.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function plagiarismFineApplied(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new PlagiarismFineApplied($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a wrong file fine.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function wrongFileFineApplied(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new WrongFileFineApplied($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a ignored messages fine.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function ignoredMessagesFineApplied(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new IgnoredMessagesFineApplied($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a ignored messages fine.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function reassignmentFineApplied(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new ReassignmentFineApplied($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer of a deadline extension fine has been applied.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function deadlineExtensionFineApplied(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send($order->writer, new DeadlineExtensionFineApplied($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }

    /**
     * Notify the writer a new order has been placed.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newOrderAvailable(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        Notification::send(User::writers(), new newOrderAvailable($order));

        return new JsonResponse(['success' => true, 'message' => 'Notification sent successfully.']);
    }
}
