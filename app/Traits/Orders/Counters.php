<?php

namespace App\Traits\Orders;

use App\Common\ApplicationStatus;
use App\Common\OrderStatus;
use App\Models\Orders\Fine;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

trait Counters
{
    /**
     * Number of available orders for bidding.
     *
     * @return mixed
     */
    public static function availableCount()
    {
        $application = Auth::user()->application;

        return $application->status == ApplicationStatus::PENDING || $application->status == ApplicationStatus::PENDING_APPROVAL
            ? 0 : self::whereStatus(OrderStatus::AVAILABLE)
                ->whereNull('deleted_at')
                ->whereDoesntHave('bids', function (Builder $query) {
                    $query->where('user_id', Auth::user()->id);
                })->count();
    }

    /**
     * Number of available orders for bidding.
     *
     * @return mixed
     */
    public static function bidsCount()
    {
        return self::whereStatus(OrderStatus::AVAILABLE)->whereHas('bids', function ($query) {
            $query->where('user_id', Auth::user()->id);
        })->count();
    }

    /**
     * Number of assigned orders.
     *
     * @return mixed
     */
    public static function assignedCount()
    {
        return self::where(function ($query) {
            $query->whereStatus(OrderStatus::ASSIGNED)->orWhere('status', OrderStatus::REASSIGNMENT_REQUESTED)
                ->orWhere('status', OrderStatus::DEADLINE_EXTENSION_REQUESTED)
                ->orWhere('status', OrderStatus::PENDING_CONFIRMATION);
        })->whereWriterId(Auth::user()->id)->count();
    }

    /**
     * Submitted orders count.
     *
     * @return mixed
     */
    public static function submittedCount()
    {
        return self::where(function ($query) {
            $query->where('status', OrderStatus::FORWARDED)->orWhere('status', OrderStatus::SUBMITTED)
                ->orWhere('status', OrderStatus::ADMIN_REVIEW_SUBMISSION)
                ->orWhere('status', OrderStatus::CLIENT_REVIEW_SUBMISSION);
        })->whereWriterId(Auth::user()->id)->count();
    }

    /**
     * Get number of orders under revision.
     *
     * @return mixed
     */
    public static function revisionsCount()
    {
        return self::where(function ($query) {
            $query->where('status', OrderStatus::PENDING_CLIENT_REVIEW_CONFIRMATION)
                ->orWhere('status', OrderStatus::PENDING_ADMIN_REVIEW_CONFIRMATION)
                ->orWhere('status', OrderStatus::ADMIN_REVIEW_REASSIGNMENT_REQUESTED)
                ->orWhere('status', OrderStatus::REVISION_DEADLINE_EXTENSION_REQUESTED)
                ->orWhere('status', OrderStatus::REVIEW_REASSIGNMENT_REQUESTED)
                ->orWhere('status', OrderStatus::ADMIN_REVISION_DEADLINE_EXTENSION_REQUESTED)
                ->orWhere('status', OrderStatus::REVIEW_BY_ADMIN)->orWhere('status', OrderStatus::REVIEW_BY_CLIENT);
        })->whereWriterId(Auth::user()->id)->count();
    }

/**
     * Get number of orders under revision.
     *
     * @return mixed
     */
    public static function archievedCount()
    {
        return self::where(function ($query) {
            $query->whereStatus(OrderStatus::CANCELLED)
                    ->orWhere('status', OrderStatus::ACCEPTED)
                    ->orWhere('status', OrderStatus::AUTO_ACCEPTED)
                    ->orWhere('status', OrderStatus::PARTIAL_REFUND)
                    ->orWhere('status', OrderStatus::MANUALLY_ACCEPTED)
                    ->orWhere('status', OrderStatus::REFUNDED);
        })->whereWriterId(Auth::user()->id)->count();
    }








    /**
     * Number of orders under dispute
     *
     * @return mixed
     */
    public static function disputesCount()
    {
        return self::where(function ($query) {
            $query->whereStatus(OrderStatus::DISPUTED)->orWhere('status', OrderStatus::PENDING_REFUND)
                ->orWhere('status', OrderStatus::REFUND_REVIEW);
        })->whereWriterId(Auth::user()->id)->count();
    }

    public static function hasRevisionCount(User $user)
    {
        return self::completed()->whereHas('revisions')
            ->where('writer_id', $user->id)
            ->count();
    }

    public static function hasDisputeCount(User $user)
    {
        return self::completed()->whereHas('disputes')
            ->where('writer_id', $user->id)
            ->count();
    }

    public static function wasLateCount(User $user)
    {
        return self::completed()->whereHas('fines', function (Builder $query) {
            $query->where('type', 'late submission fine')
                ->where('status', Fine::COMPLETE);
        })->where('writer_id', $user->id)
            ->count();
    }

    public static function clientRequestsCount(User $user)
    {
        return self::completed()->where('is_direct_assignment', true)
            ->where('writer_id', $user->id)
            ->count();
    }

    public static function completedOrdersCount(User $user)
    {
        return self::completed()
            ->where('writer_id', $user->id)
            ->count();
    }
}
