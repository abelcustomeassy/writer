<?php

namespace App\Traits\Orders;

use App\DataTables\Orders\ArchivedOrdersDataTable as ArchivedOrders;
use App\DataTables\Orders\AssignedOrdersDataTable as AssignedOrders;
use App\DataTables\Orders\AvailableOrdersDataTable as AvailableOrders;
use App\DataTables\Orders\BidOrdersDataTable as BidOrders;
use App\DataTables\Orders\DisputedOrdersDataTable as DisputedOrders;
use App\DataTables\Orders\OrdersUnderReviewDataTable as OrdersUnderReview;
use App\DataTables\Orders\SubmittedOrdersDataTable as SubmittedOrders;

trait OrdersDataTables
{
    /**
     * Show orders that have not been paid for or yet to be assigned
     * to a writer
     *
     * @param AvailableOrders $dataTable
     * @return mixed
     */
    public function available(AvailableOrders $dataTable)
    {
        return $dataTable->render('app.orders.index', ['title' => 'Available Orders']);
    }

    /**
     * Show all the orders that the writer has bid for.
     *
     * @param BidOrders $dataTable
     * @return mixed
     */
    public function bids(BidOrders $dataTable)
    {
        return $dataTable->render('app.orders.index', ['title' => 'All Bids']);
    }

    /**
     * Show orders that have been assigned to writers.
     *
     * @param AssignedOrders $dataTable
     * @return mixed
     */
    public function assigned(AssignedOrders $dataTable)
    {
        return $dataTable->render('app.orders.index', ['title' => 'Assigned Orders']);
    }

    /**
     * Orders that have been submitted by writers.
     *
     * @param SubmittedOrders $dataTable
     * @return mixed
     */
    public function submitted(SubmittedOrders $dataTable)
    {
        return $dataTable->render('app.orders.index', ['title' => 'Submitted Orders']);
    }

    /**
     * Orders that are under review.
     *
     * @param OrdersUnderReview $dataTable
     * @return mixed
     */
    public function reviewed(OrdersUnderReview $dataTable)
    {
        return $dataTable->render('app.orders.index', ['title' => 'Under Revision']);
    }

    /**
     * Orders that are under dispute.
     *
     * @param DisputedOrders $dataTable
     * @return mixed
     */
    public function disputed(DisputedOrders $dataTable)
    {
        return $dataTable->render('app.orders.index', ['title' => 'Under Dispute']);
    }

    /**
     * Orders that have been accepted, refunded or cancelled.
     *
     * @param ArchivedOrders $dataTable
     * @return mixed
     */
    public function archived(ArchivedOrders $dataTable)
    {
        return $dataTable->render('app.orders.index', ['title' => 'Archived Orders']);
    }
}