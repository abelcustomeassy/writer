<?php

namespace App\Traits\Messages;

use App\Common\MessageStates;
use App\Models\Messages\Message;
use App\Models\Messages\MessageState;

trait HasMessages
{
    /**
     * Get the user messages.
     *
     * @return mixed
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'sender_id')
            ->orWhere('recipient_id', '=', $this->id)
            ->orderBy('created_at', 'DESC');
    }

    /**
     * Get the user message states.
     *
     * @return mixed
     */
    public function messageStates()
    {
        return $this->hasMany(MessageState::class);
    }

    /**
     * Get the user unread messages.
     *
     * @return mixed
     */
    public function unreadMessages()
    {
        return $this->messageStates()->where('state', MessageStates::UNREAD);
    }
}
