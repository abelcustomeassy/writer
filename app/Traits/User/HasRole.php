<?php

namespace App\Traits\User;

use App\Common\ApplicationStatus;
use App\Models\Configurations\Department;
use App\Models\Configurations\UserType;
use App\Models\Configurations\Website;
use App\Models\Roles\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

trait HasRole
{
    /**
     * The website the user belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function website()
    {
        return $this->belongsTo(Website::class)->withDefault();
    }

    /**
     * The user type for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userType()
    {
        return $this->belongsTo(UserType::class)->withDefault();
    }

    /**
     * The department the user is in.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class)->withDefault();
    }

    /**
     * A user has one role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class)->withDefault();
    }

    /**
     * Determine if the user has the given role.
     *
     * @param  string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        return $this->role->name === $role;
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param  string $permission
     * @return boolean
     */
    public function hasPermission($permission)
    {
        return $this->role->permissions->contains('name', $permission);
    }

    /**
     * Check if user is administrator
     *
     * @return boolean
     */
    public function isAdministrator()
    {
        return $this->userType->name == "Administrator";
    }

    /**
     * Get all the system administrators.
     *
     * @return mixed
     */
    public static function administrators()
    {
        return self::whereUserTypeId(adminUserTypeId())
            ->whereStatus(self::ACTIVATED);
    }

    /**
     * Get all the system active writers.
     *
     * @param Builder $query
     * @return mixed
     */
    public function scopeWriters(Builder $query)
    {
        return $query->whereHas('application', function ($query) {
            $query->where('status', ApplicationStatus::APPROVED);
        })->where('status', User::ACTIVATED)->where('website_id', websiteId())->get();
    }

    /**
     * Get employees with a certain permission.
     *
     * @param string $permission
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function withPermission($permission, $department = false)
    {
        return self::whereHas('employeeProfile', function (Builder $query) use ($permission, $department) {
            if ($department) {
                $query->where('department_id', $department);
            }

            $query->whereHas('role', function (Builder $query) use ($permission) {
                $query->whereHas('permissions', function (Builder $query) use ($permission) {
                    $query->where('name', $permission);
                });
            });
        });
    }
}
