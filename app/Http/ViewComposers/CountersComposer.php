<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Orders\Order;
use Illuminate\Support\Facades\Auth;

class CountersComposer
{
    /**
     * CountersComposer constructor.
     */
    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'availableCount' => Order::availableCount(),
            'bidsCount' => Order::bidsCount(),
            'assignedCount' => Order::assignedCount(),
            'submittedCount' => Order::submittedCount(),
            'reviewedCount' => Order::revisionsCount(),
            'disputedCount' => Order::disputesCount(),
            'archievedCount'=> Order::archievedCount(),
            'notificationsCount' => Auth::user()->unreadNotifications->count(),
            'messagesCount' => Auth::user()->unreadMessages->count(),
        ]);
    }
}