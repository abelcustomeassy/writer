<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Common\ApplicationSteps;
use App\Common\ApplicationStatus;
use Illuminate\Support\Facades\Auth;

class SidebarComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $application = Auth::user()->application;
        $progress = floor($application->step / ApplicationSteps::TOTAL * 100);

        $view->with([
            'enabled' => $progress == 100 && $application->status == ApplicationStatus::APPROVED ? true : false,
            'rating' => optional(Auth::user()->writerProfile)->rating,
            'completeness' => $progress,
            'user' => Auth::user()
        ]);
    }

}