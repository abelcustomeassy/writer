<?php

namespace App\Http\Middleware;

use Closure;
use  App\Models\Announcements\Announcement;
use Illuminate\Support\Facades\Auth;

class CheckAnnouncement
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return $next($request);
        }

        if ($announcement = Announcement::unread()) {
            return redirect(route('announcements.show', ['slug' => $announcement->slug]));
        }

        return $next($request);
    }
}
