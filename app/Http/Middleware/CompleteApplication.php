<?php

namespace App\Http\Middleware;

use App\Common\ApplicationSteps;
use Closure;
use App\Common\ApplicationStatus;
use Illuminate\Support\Facades\Auth;

class CompleteApplication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return $next($request);
        }

        $application = Auth::user()->application;

        if ($application->step != ApplicationSteps::COMPLETE) {
            return redirect(route('application'));
        }

        if ($application->status == ApplicationStatus::DECLINED || $application->status == ApplicationStatus::PENDING
            || $application->status == ApplicationStatus::PENDING_APPROVAL) {
            return redirect(route('application'));
        }

        return $next($request);
    }
}
