<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $orderRules = [
            'paper_type_id' => 'required',
            'field_id' => 'required',
            'topic' => 'required',
            'citation_id' => 'required',
            'academic_level_id' => 'required',
            'deadline_id' => 'required',
            'pages' => 'required',
            'spacing' => 'required',
            'payment_method' => 'required'
        ];

        $userRules = [
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:8'
        ];

        if (!Auth::check()) {
            return array_merge($orderRules, $userRules);
        }

        return $orderRules;
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'paper_type_id.required' => 'Paper type is required',
            'field_id.required' => 'Discipline is required',
            'citation_id.required' => 'Citation is required',
            'academic_level_id.required' => 'Academic level is required',
            'deadline_id.required' => 'Deadline is required'
        ];
    }
}
