<?php

namespace App\Http\Requests;

use App\Models\User;
use GuzzleHttp\Client;

class RemoteRequest
{
    /**
     * Client for making the remote post request.
     *
     * @var Client
     */
    protected $client;

    /**
     * Notification constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->client = new Client([
            'base_uri' => 'https://'.$user->website->url.'/notifications/',
            'verify' => config('app.env') === 'local' ? false : true,
        ]);
    }

    /**
     * Make the remote post request to the website.
     *
     * @param $to
     * @param array $data
     * @param string $method
     * @return mixed
     */
    public function send($to, $data = [], $method = 'POST')
    {
        $response = $this->client->request($method, $to, ['json' => $data]);

        return json_decode($response->getBody()->getContents());
    }
}
