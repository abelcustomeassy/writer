<?php

namespace App\Http\Controllers;

 use App\Notifications\Orders\OrderTaken;
 use RealRashid\SweetAlert\Facades\Alert;
use App\Rating\Rater;
use App\Accountant\Wallet;
use App\Common\OrderStatus;
use App\Models\Orders\Fine;
use App\Messenger\Messenger;
use Illuminate\Http\Request;
use App\Models\Orders\Order;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Models\Media;
use App\Models\Orders\OrderActivityLog;
use App\Traits\Orders\OrdersDataTables;
use App\Models\Orders\DeadlineExtension;
use App\Notifications\Remote\NotifyEmployee;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Fines\LatenessFineApplied;
use App\Models\Configurations\WriterConfiguration;

class OrdersController extends Controller
{
    use OrdersDataTables;

    /**
     * OrdersController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show order details
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        if ($order->writer_id && $order->writer_id != Auth::user()->id) {
            Alert::error('Failed!', 'Only owner writer can view this order.')->persistent();

            return redirect()->back();
        }

        return view('app.orders.show', [
            'order' => $order,
            'status' => new OrderStatus(),
            'department' => $order->employee->employeeProfile->department,
            'writerAmount' => $order->status == OrderStatus::AVAILABLE ? $this->getWriterPrice($order) : $order->writer_amount,
        ]);
    }

    /**
     * Get the set writer price for the order.
     *
     * @param Order $order
     * @return float|int
     */
    protected function getWriterPrice($order)
    {
        if ($order->writerPricing()->exists()) {
            return $this->getOrderWriterPricing($order);
        }

        $writer = Auth::user();

        if ($writer->writerProfile->writerLevel->payment == 'PERCENTAGE') {
            return round($order->amount * ($writer->writerProfile->writerLevel->value->percentage / 100), 2);
        }

        $amount = $order->ppt_slides * $writer->writerProfile->writerLevel->value->ppt;

        if ($order->spacing == 'single') {
            return round($amount + $order->pages * 2 * $writer->writerProfile->writerLevel->value->pages, 2);
        }

        return round($amount + $order->pages * $writer->writerProfile->writerLevel->value->pages, 2);
    }

    /**
     * Get writer pricing from order.
     *
     * @param $order
     * @return float|int
     */
    protected function getOrderWriterPricing($order)
    {
        $levelName = strtolower(Auth::user()->writerProfile->writerLevel->name);

        if (isset($order->writerPricing->$levelName->percentage)) {
            return ($order->writerPricing->$levelName->percentage / 100) * $order->amount;
        }

        if ($order->spacing == 'single') {
            return ($order->ppt_slides * $order->writerPricing->$levelName->ppt) +
                ($order->pages * 2 * $order->writerPricing->$levelName->pages);
        }

        return ($order->ppt_slides * $order->writerPricing->$levelName->ppt) +
            ($order->pages * $order->writerPricing->$levelName->pages);
    }

    /**
     * Download the media file.
     *
     * @param Media $mediaItem
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Media $mediaItem)
    {
        return response()->download($mediaItem->getPath(), "#".$mediaItem->model->id."_".$mediaItem->file_name);
    }

    /**
     * Request for an order.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function requestOrder($id)
    {
        $order = Order::findOrFail($id);

        if ($order->myBids->count()) {
            Alert::error('Failed!', 'You have already applied for this order.')->persistent();

            return redirect()->back();
        }

        $bid = $order->bids()->create([
            'user_id' => Auth::user()->id,
            'comments' => '',
        ]);

        (new NotifyEmployee($order->employee))->bidPlaced($bid);

        Alert::success('Success', 'Order Requested successfully.')->persistent();

        return redirect()->back();
    }

    /**
     * Cancel Order request.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelRequest($id)
    {
        $order = Order::findOrFail($id);
        if ($order->myBids->count()) {
            $order->myBids()->delete();
            Alert::success('Success', 'Order Request cancelled successfully.')->persistent();

            return redirect()->back();
        }

        Alert::success('Success', 'You do not have any requests for the order.')->persistent();

        return redirect()->back();
    }

    /**
     * Take the order.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function takeOrder($id)
    {
        $order = Order::findOrFail($id);
        if ($order->writer_id) {
            Alert::error('Failed!', 'Order already assigned.')->persistent();

            return redirect(route('orders.assigned'));
        }

        $order->status = OrderStatus::ASSIGNED;
        $order->writer_id = Auth::user()->id;
        $order->is_take = true;
        $order->taken_at = now();
        $order->save();

        $order->writer_amount = $this->getWriterPrice($order);
        $order->save();

        Notification::send(Auth::user(), new OrderTaken($order));

        OrderActivityLog::record($order, 'Used a take to get the order');

        Alert::success('Success', 'Order taken successfully.')->persistent();

        return redirect()->back();
    }

    /**
     * Confirm agreement to work on the order.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirmOrder($id)
    {
        $order = Order::findOrFail($id);
        if ($order->writer_id != Auth::user()->id) {
            Alert::error('Failed!', 'You cannot confirm this order')->persistent();

            return redirect()->back();
        }

        $order->status = OrderStatus::ASSIGNED;
        $order->save();

        OrderActivityLog::record($order, 'Confirmed to start working on the order');

        (new NotifyEmployee($order->employee))->orderConfirmed($order);

        Alert::success('Success', 'Order confirmed successfully.')->persistent();

        return redirect()->back();
    }

    /**
     * Confirm the order revision from the client.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirmClientReview($id)
    {
        $order = Order::findOrFail($id);
        if ($order->writer_id != Auth::user()->id) {
            Alert::error('Failed!', 'You cannot confirm this order')->persistent();

            return redirect()->back();
        }

        $order->status = OrderStatus::REVIEW_BY_CLIENT;
        $order->save();

        OrderActivityLog::record($order, 'Confirmed to start working on the revision');

        (new NotifyEmployee($order->employee))->orderReviewConfirmed($order);

        Alert::success('Success', 'Order confirmed successfully.')->persistent();

        return redirect()->back();
    }

    /**
     * Confirm the order revision from admin.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirmAdminReview($id)
    {
        $order = Order::findOrFail($id);
        if ($order->writer_id != Auth::user()->id) {
            Alert::error('Failed!', 'You cannot confirm this order')->persistent();

            return redirect()->back();
        }

        $order->status = OrderStatus::REVIEW_BY_ADMIN;
        $order->save();

        OrderActivityLog::record($order, 'Confirmed to start working on the admin revision');

        (new NotifyEmployee($order->employee))->orderReviewConfirmed($order);

        Alert::success('Success', 'Order confirmed successfully.')->persistent();

        return redirect()->back();
    }

    /**
     * Reject the order.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rejectOrder(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        if ($order->writer_id != Auth::user()->id) {
            Alert::error('Failed!', 'You cannot reject this order')->persistent();

            return redirect(route('orders.assigned'));
        }

        $order->myBids()->delete();

        $order->writer_id = null;
        $order->status = OrderStatus::AVAILABLE;
        $order->is_direct_assignment = false;
        $order->is_take = false;
        $order->save();

        OrderActivityLog::record($order, 'Declined to work on the order');

        Messenger::send($order->employee_id, 'Rejected', $request->reason, $order->id);

        Alert::success('Success', 'Order rejected successfully.')->persistent();

        return redirect(route('orders.assigned'));
    }

    /**
     * Request order reassignment.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function reassign(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $this->authorize('update', $order);

        $this->validate($request, ['reason' => 'required|max:1000']);

        $config = WriterConfiguration::first();

        if ($order->is_take && now()->diffInMinutes($order->taken_at) <= $config->valid_avail_minutes) {
            return $this->availOrder($order, $request);
        }

        $order->status = OrderStatus::REASSIGNMENT_REQUESTED;
        $order->save();

        OrderActivityLog::record($order, 'Requested order reassignment');

        Messenger::send($order->employee_id, 'Order Re-assignment', $request->reason, $order->id);

        Alert::success('Success', 'Order reassignment request placed successfully.')->persistent();

        return redirect(route('orders.assigned'));
    }

    /**
     * Make the order available.
     *
     * @param Order $order
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    protected function availOrder(Order $order, Request $request)
    {
        $order->status = OrderStatus::AVAILABLE;
        $order->writer_id = null;
        $order->is_take = false;
        $order->is_direct_assignment = false;
        $order->save();

        Messenger::send($order->employee_id, 'Order Re-assignment reason', $request->reason, $order->id);

        OrderActivityLog::record($order, 'Made the order available');

        Alert::success('Success', 'Order de-assigned successfully.')->persistent();

        return redirect(route('orders.assigned'));
    }

    /**
     * Request deadline extension.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function deadlineExtensionRequest(Request $request, int $id)
    {
        $order = Order::findOrFail($id);

        $this->authorize('update', $order);

        $this->validate($request, $this->deadlineExtensionRules());

        if ($order->deadlineExtension) {
            Alert::error('Failed!', 'You already have a pending request')->persistent();

            return redirect()->back();
        }

        DeadlineExtension::create([
            'user_id' => Auth::user()->id,
            'order_id' => $order->id,
            'type' => $request->time_type,
            'value' => $request->time_value,
        ]);

        if ($order->status == OrderStatus::ASSIGNED) {
            $order->status = OrderStatus::DEADLINE_EXTENSION_REQUESTED;
            $order->save();
        }

        if ($order->status == OrderStatus::REVIEW_BY_CLIENT) {
            $order->status = OrderStatus::REVISION_DEADLINE_EXTENSION_REQUESTED;
            $order->save();
        }

        if ($order->status == OrderStatus::REVIEW_BY_ADMIN) {
            $order->status = OrderStatus::ADMIN_REVISION_DEADLINE_EXTENSION_REQUESTED;
            $order->save();
        }

        OrderActivityLog::record($order, 'Requested deadline extension');

        Messenger::send($order->employee_id, 'Deadline Extension', $request->message, $order->id);

        Alert::success( 'Success','Request placed successfully')->persistent();

        return redirect()->back();
    }

    /**
     * Deadline extension rules.
     *
     * @return array
     */
    protected function deadlineExtensionRules()
    {
        return [
            'time_value' => 'required|digits_between:1,2',
            'time_type' => 'required|max:255',
            'message' => 'required',
        ];
    }

    /**
     * Order submission by the writer.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submitOrder(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $this->validate($request, $this->orderSubmissionValidationRules($order));

        if ($order->writer_id != Auth::user()->id) {
            Alert::error('Failed!', 'You cannot submit for this order')->persistent();

            return redirect()->back();
        }

        if ($request->hasFile('submissions')) {
            $this->uploadFiles($order, $request, 'submissions');
        }

        if ($request->hasFile('sources')) {
            $this->uploadFiles($order, $request, 'sources');
        }

        $status = OrderStatus::SUBMITTED;

        if ($order->status == OrderStatus::REVIEW_BY_CLIENT) {
            $status = OrderStatus::CLIENT_REVIEW_SUBMISSION;
        }

        $order->status = $status;
        $order->save();

        if ($order->writer_deadline->lessThan(now())) {
            $this->applyLatenessFine($order);
        }

        OrderActivityLog::record($order, 'Submitted order');

        (new NotifyEmployee($order->employee))->orderSubmitted($order);

        Alert::success('Success', 'Order submission completed successfully')->persistent();

        return redirect()->back();
    }

    /**
     * Validation rules for order submissions.
     *
     * @param Order $order
     * @return array
     */
    protected function orderSubmissionValidationRules(Order $order)
    {
        $rules = ['submissions' => 'required|min:1'];

        if ($order->requires_digital_references) {
            return array_merge($rules, ['sources' => 'required|min:1']);
        }

        return $rules;
    }

    /**
     * Upload any files that maybe on the request.
     *
     * @param Order $order
     * @param Request $request
     * @param string $requestKey
     * @return void
     */
    protected function uploadFiles(Order $order, Request $request, $requestKey)
    {
        $order->addMultipleMediaFromRequest([$requestKey])->each(function ($fileAdder) use ($request, $requestKey) {
            $mediaItem = $fileAdder->toMediaCollection('orders', 'orders');
            if ($requestKey == "submissions") {
                $lastDot = strrpos($mediaItem->file_name, '.') + 1;
                $extension = substr($mediaItem->file_name, $lastDot);
                parse_str($mediaItem->name.' '.$extension, $sanitized);

                $mediaItem->setCustomProperty('order_files', $request->filled(key($sanitized)) ? $request->get(key($sanitized)) : "Completed Paper");
            }

            if ($requestKey == "sources") {
                $mediaItem->setCustomProperty('order_files', "Digital Source");
            }

            $mediaItem->setCustomProperty('uploader', 'Writer');
            $mediaItem->setCustomProperty('target', 'Employee');
            $mediaItem->save();
        });
    }

    /**
     * Apply fine for a late order submission.
     *
     * @param \App\Models\Orders\Order $order
     */
    protected function applyLatenessFine(Order $order)
    {
        $config = WriterConfiguration::first();

        $amount = $this->getWriterPrice($order) * $config->late_submission_fine / 100;
        $description = '<strong>#'.$order->id.'</strong>&nbsp;&nbsp;late submission fine';

        Wallet::debit($amount, $description);

        Fine::create([
            'user_id' => $order->writer_id,
            'type' => 'late submission fine',
            'order_id' => $order->id,
            'amount' => $amount,
            'status' => Fine::APPLIED,
        ]);

        OrderActivityLog::record($order, 'Lateness fine was applied');

        Notification::send($order->writer, new LatenessFineApplied($order));
    }
}
