<?php

namespace App\Http\Controllers;

 use RealRashid\SweetAlert\Facades\Alert;
use App\Mail\ContactFormEmail;
use App\Rules\ValidRecaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class PagesController extends Controller
{

    /**
     * Load the website home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('pages.home');
    }

    /**
     * Get the page being requested
     *
     * @param $page
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function page($page)
    {
        $view = 'pages.' . str_replace('-', '_', $page);

        if (View::exists($view)) {
            return view($view);
        }

        abort('404');
    }
}
