<?php

namespace App\Http\Controllers;

use App\Models\Announcements\Announcement;
use Illuminate\Support\Facades\Auth;

class AnnouncementsController extends Controller
{
    /**
     * AnnouncementsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all the announcements for the site.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.announcements.index', [
            'announcements' => Announcement::paginate(10)
        ]);
    }

    /**
     * Show the announcement.
     *
     * @param $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view('app.announcements.show', [
            'announcement' => Announcement::where('slug', $slug)->firstOrFail()
        ]);
    }

    /**
     * Confirm the announcement has been seen.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function confirm($id)
    {
        $announcement = Announcement::findOrFail($id);
        $announcement->users()->attach(Auth::user()->id);

        return redirect(route('announcements.index'));
    }
}
