<?php

namespace App\Http\Controllers;

 use RealRashid\SweetAlert\Facades\Alert;
use App\Mail\Wallet\PaymentAccountCreated;
use App\Models\Configurations\PaymentMethod;
use App\Models\Configurations\WriterConfiguration;
use App\Models\Finance\PaymentAccount;
use App\Models\Finance\WalletTransaction;
use App\Models\Finance\Withdrawal;
use App\Models\Orders\EligiblePayment;
use App\Models\Orders\Fine;
use App\Models\User;
use App\Notifications\Payments\PaymentRequestReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class WalletController extends Controller
{
    /**
     * WalletController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * View the wallet details.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.wallet.index', [
            'paymentAccounts' => Auth::user()->paymentAccounts,
            'paymentAccount' => Auth::user()->paymentAccount,
            'paymentMethods' => PaymentMethod::exceptWallet(),
            'configs' => WriterConfiguration::first(),
            'eligiblePayments' => EligiblePayment::with('order')->pending(),
            'requestedPayments' => EligiblePayment::with('order')->requested(),
            'fines' => Fine::applicable(),
        ]);
    }

    /**
     * Handle fine removal requests.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeFine(Request $request)
    {
        $this->validate($request, [
            'fine_id' => 'required',
            'reason' => 'required|max:1000',
        ]);

        $fine = Fine::findOrFail($request->fine_id);
        $fine->reason = $request->reason;
        $fine->status = Fine::REMOVAL_REQUESTED;
        $fine->save();

        Alert::success('Success', 'Fine removal request successful')->persistent();

        return redirect(route('wallet.index'));
    }

    /**
     * Create a new payment account.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createPaymentAccount(Request $request)
    {
        $this->validate($request, ['payment_method_code' => 'required:max:255']);

        $user = Auth::user();

        $paymentAccount = $user->paymentAccounts()->create($this->paymentAccountDetails($request, $user));

        Mail::to($user)->send(new PaymentAccountCreated($paymentAccount));

        Alert::success('Account created', 'Please check your email to confirm changes')->persistent();

        return redirect(route('wallet.index').'#account');
    }

    /**
     * Prepare the account details.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return array
     */
    protected function paymentAccountDetails(Request $request, User $user)
    {
        $account = [
            'payment_method_id' => PaymentMethod::whereCode($request->payment_method_code)->first()->id,
            'verification_code' => Str::random(16),
            'is_default' => $user->paymentAccounts->count() == 0,
        ];

        switch ($request->payment_method_code) {
            case 'paypal':
                $account['details'] = [
                    'paypal_name' => $request->paypal_name,
                    'paypal_email' => $request->paypal_email,
                ];
                break;
            case 'bank':
                $account['details'] = [
                    'bank_name' => $request->bank_name,
                    'account_name' => $request->account_name,
                    'account_number' => $request->account_number,
                ];
                break;
            case 'mpesa':
                $account['details'] = [
                    'mpesa_name' => $request->mpesa_name,
                    'mpesa_number' => $request->mpesa_number,
                ];
                break;
        }

        return $account;
    }

    /**
     * Verify the new payment account.
     *
     * @param $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verifyPaymentAccount($code)
    {
        $paymentAccount = PaymentAccount::whereUserId(Auth::user()->id)->whereVerificationCode($code)->first();
        if (is_null($paymentAccount)) {
            Alert::error('Error', 'We could not find that verification')->persistent();

            return redirect(route('wallet.index').'#account');
        }

        $paymentAccount->verify();

        Alert::success('Success', 'New account has been verified successfully')->persistent();

        return redirect(route('wallet.index').'#account');
    }

    /**
     * Make an account the default account to receive payments.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function makeAccountDefault($id)
    {
        $account = PaymentAccount::findOrFail($id);

        if ($account->verification_code) {
            Alert::error('Failed', 'Please verify this account first')->persistent();

            return redirect(route('wallet.index').'#account');
        }

        $account->makeDefault();

        $defaultAccounts = PaymentAccount::where('id', '<>', $id)->get();

        foreach ($defaultAccounts as $defaultAccount) {
            $defaultAccount->removeDefault();
        }

        Alert::success('Success', 'Default account changed successfully')->persistent();

        return redirect(route('wallet.index').'#account');
    }

    /**
     * Delete payment account from the system.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAccount($id)
    {
        $account = PaymentAccount::findOrFail($id);
        $account->delete();

        Alert::success('Success', 'Payment account deleted successfully')->persistent();

        return redirect(route('wallet.index').'#account');
    }

    /**
     * Withdraw balance from wallet.
     *
     * @return \Illuminate\Http\Response
     */
    public function withdraw()
    {
        if (Auth::user()->hasPendingWithdrawals()) {
            Alert::error('Failed!', 'You already have a pending withdrawal.')->persistent();

            return redirect(route('wallet.index'));
        }

        $paymentAccount = Auth::user()->paymentAccount;

        if (is_null($paymentAccount)) {
            Alert::error('Failed!', 'You don\'t have a default account.')->persistent();

            return redirect(route('wallet.index'));
        }

        $eligiblePayments = EligiblePayment::pending();
        $fines = Fine::pending();

        $withdrawal = Withdrawal::create([
            'reference' => $reference = 'REQ'.now()->timestamp,
            'user_id' => Auth::user()->id,
            'amount' => $amount = Auth::user()->wallet->balance,
            'payment_method_id' => $paymentAccount->payment_method_id,
            'payment_method_details' => (array) $paymentAccount->details,
            'payment_items' => [
                'orders' => $eligiblePayments->map(function ($eligiblePayment) {
                    return [
                        'order_id' => $eligiblePayment->order_id,
                        'discipline' => $eligiblePayment->order->discipline->name,
                        'pages' => $eligiblePayment->order->pages,
                        'accepted_at' => $eligiblePayment->order->accepted_at->format('dS M, Y'),
                        'amount' => $eligiblePayment->amount,
                    ];
                })->toArray(),
                'fines' => $fines->map(function ($fine) {
                    return collect($fine->toArray())->only(['type', 'amount', 'order_id'])->all();
                })->toArray(),
            ],
        ]);

        Auth::user()->wallet->debit($amount);

        WalletTransaction::create([
            'wallet_id' => Auth::user()->wallet->id,
            'reference' => $reference,
            'description' => '<strong>'.$reference.'</strong> Withdrawal',
            'amount' => $amount,
            'type' => 'd',
            'status' => WalletTransaction::PENDING,
        ]);

        foreach ($eligiblePayments as $eligiblePayment) {
            $eligiblePayment->status = EligiblePayment::REQUESTED;
            $eligiblePayment->withdrawal_id = $withdrawal->id;
            $eligiblePayment->save();
        }

        foreach ($fines as $fine) {
            $fine->status = Fine::COMPLETE;
            $fine->withdrawal_id = $withdrawal->id;
            $fine->save();
        }

        Notification::send($withdrawal->user, new PaymentRequestReceived($withdrawal));

        Alert::success('success', 'Withdrawal request made successfully')->persistent();

        return redirect(route('wallet.index'));
    }

    /**
     * Withdrawals data table.
     *
     * @return mixed
     */
    public function withdrawals()
    {
        $withdrawals = Withdrawal::with('paymentMethod');

        return Datatables::of($withdrawals)->editColumn('status', function ($withdrawal) {
            return view('app.wallet.status.withdrawal', [
                'status' => $withdrawal->status,
                'transactionStatus' => new Withdrawal(),
            ]);
        })->editColumn('amount', function ($withdrawal) {
            return currency().number_format($withdrawal->amount, 2);
        })->editColumn('processed_at', function ($withdrawal) {
            if (empty($withdrawal->processed_at)) {
                return "-";
            }

            return $withdrawal->formattedDateTime('processed_at');
        })->rawColumns(['status', 'reference'])->make(true);
    }

    /**
     * Statements data table.
     *
     * @return mixed
     */
    public function statement()
    {
        $transactions = WalletTransaction::query();

        return Datatables::of($transactions)
                         ->editColumn('created_at', function ($transaction) {
                             return $transaction->formattedDateTime('created_at');
                         })
                         ->editColumn('amount', function ($transaction) {
                             if ($transaction->type == 'd') {
                                 return '<span class="text-danger">-'.currency().number_format($transaction->amount, 2).'</span>';
                             }

                             return currency().number_format($transaction->amount, 2);
                         })->rawColumns(['amount', 'description'])->make(true);
    }

    /**
     * Validation rules for adding a payment account.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'payment_method_code' => 'required:max:255',
        ];
    }
}
