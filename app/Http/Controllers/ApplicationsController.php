<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use RealRashid\SweetAlert\Facades\Alert;
use App\Notifications\Application\ApplicationCompleted;
use Carbon\Carbon;
use App\Models\WriterProfile;
use Illuminate\Http\Request;
use App\Common\ApplicationSteps;
use Illuminate\Http\JsonResponse;
use App\Common\ApplicationStatus;
use App\Models\Applications\Skill;
use App\Models\Evaluations\Question;
use Illuminate\Support\Facades\Auth;
use App\Models\Evaluations\Evaluation;
use App\Models\Configurations\Country;
use App\Models\Applications\Education;
use App\Models\Configurations\Citation;
use App\Models\Configurations\Discipline;
use App\Models\Evaluations\QuestionOption;
use App\Models\Evaluations\UserEvaluation;
use App\Models\Configurations\AcademicLevel;
use App\Models\Evaluations\UserEvaluationResult;
use Illuminate\Support\Facades\Notification;

class ApplicationsController extends Controller
{
    /**
     * The current user's application.
     *
     * @var
     */
    protected $application;

    /**
     * ApplicationsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Determine which page to load.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->application = Auth::user()->application;
        $step = $this->application->step;

        switch ($step) {
            case $step == ApplicationSteps::PROFILE:
                return view('app.applications.profile', [
                    'countries' => Country::all(),
                ]);
            case $step == ApplicationSteps::EDUCATION:
                return view('app.applications.education', [
                    'academic_levels' => AcademicLevel::all(),
                ]);
            case $step == ApplicationSteps::SKILLS:
                $technical = Auth::user()->writerProfile->writer_type == 'TECHNICAL' ? true : false;

                return view('app.applications.skills', [
                    'citations' => Citation::all(),
                    'disciplines' => Discipline::whereIsTechnical($technical)->get(),
                ]);
            case $step == ApplicationSteps::LANGUAGE_EVALUATION:
                return $this->languageEvaluation();
            case $step == ApplicationSteps::ESSAY_EVALUATION:
                return $this->essayEvaluation();
            case $step == ApplicationSteps::COMPLETE:
                return $this->complete();
        }
    }

    /**
     * Load the language evaluation.
     *
     * @return \Illuminate\Http\Response
     */
    public function languageEvaluation()
    {
        $userEvaluation = UserEvaluation::with('evaluation')->pending();

        if (is_null($userEvaluation->started_at)) {
            return view('app.applications.evaluations.language', [
                'data' => json_encode([
                    'type' => 'evaluation',
                    'evaluation' => $userEvaluation,
                ]),
            ]);
        }

        $question = $this->getQuestion($userEvaluation);

        if ($question instanceof Question) {
            return view('app.applications.evaluations.language', [
                'data' => json_encode([
                    'type' => 'question',
                    'evaluation' => $userEvaluation,
                    'question' => $question,
                ]),
            ]);
        }

        $this->completeEvaluation($userEvaluation);

        return redirect(route('application'));
    }

    /**
     * Get the next question for an evaluation.
     *
     * @param UserEvaluation $userEvaluation
     * @return Question|array
     */
    public function getQuestion(UserEvaluation $userEvaluation)
    {
        $userEvaluation->load('results');

        if ($userEvaluation->results->contains('submitted_at', null)) {
            return $this->pendingQuestion($userEvaluation);
        }

        return $this->newQuestion($userEvaluation);
    }

    /**
     * Return a pending question.
     *
     * @param UserEvaluation $userEvaluation
     * @return \App\Models\Evaluations\Question|\Illuminate\Database\Eloquent\Builder|\Illuminate\Http\Response
     */
    protected function pendingQuestion(UserEvaluation $userEvaluation)
    {
        $result = $userEvaluation->results->where('submitted_at', null)->first();
        $question = Question::with(['options' => function($query){
            if (App::environment('production')) {
                return $query->select(['id', 'question_id', 'description']);
            }

            return $query->select(['id', 'question_id', 'description', 'is_answer']);
        }])->find($result->question_id);

        if ($this->checkIfExpired($result, $question)) {
            if ($question->type == 'ESSAY') {
                return $this->failedEssayEvaluation($userEvaluation);
            }

            return $this->newQuestion($userEvaluation);
        }

        $remainingTime = $this->getRemainingTime($result);

        if ($remainingTime <= 0) {
            return $this->newQuestion($userEvaluation);
        }

        $question->time = $remainingTime;
        $question->number = sizeof($userEvaluation->results->pluck('question_id')->toArray());

        return $question;
    }

    /**
     * Check if the question period has expired.
     *
     * @param UserEvaluationResult $userEvaluationResult
     * @param Question $question
     * @return bool
     */
    public function checkIfExpired(UserEvaluationResult $userEvaluationResult, Question $question)
    {
        if ($question->time_type == 's') {
            return $userEvaluationResult->started_at->diffInSeconds(Carbon::now()) >= $question->time;
        }
        if ($question->time_type == 'm') {
            return $userEvaluationResult->started_at->diffInMinutes(Carbon::now()) >= $question->time;
        }
        if ($question->time_type == 'h') {
            return $userEvaluationResult->started_at->diffInHours(Carbon::now()) >= $question->time;
        }

        return $userEvaluationResult->started_at->diffInDays(Carbon::now()) >= $question->time;
    }

    /**
     * Fail an application due to late submission.
     *
     * @param UserEvaluation $userEvaluation
     * @return \Illuminate\Http\Response
     */
    protected function failedEssayEvaluation(UserEvaluation $userEvaluation)
    {
        $application = Auth::user()->application;
        $application->step = ApplicationSteps::COMPLETE;
        $application->status = ApplicationStatus::DECLINED;
        $application->save();

        $userEvaluation->status = false;
        $userEvaluation->completed_at = Carbon::now();
        $userEvaluation->save();

        Alert::error('Declined!', 'Application declined due to late submission')->persistent();

        return redirect(route('application'));
    }

    /**
     * Return a new question.
     *
     * @param $userEvaluation
     * @return Question|array
     */
    protected function newQuestion($userEvaluation)
    {
        $pendingQuestions = array_diff($userEvaluation->questions,
            $userEvaluation->results->pluck('question_id')->toArray());

        if (sizeof($pendingQuestions) == 0) {
            return ['evaluation' => 'complete'];
        }

        $question = Question::with([
            'options' => function ($query) {
                if (App::environment('production')) {
                    return $query->select(['id', 'question_id', 'description']);
                }

                return $query->select(['id', 'question_id', 'description', 'is_answer']);
            },
        ])->find(Arr::first($pendingQuestions));

        $userEvaluation->results()->save(new UserEvaluationResult([
            'question_id' => $question->id,
            'started_at' => Carbon::now(),
        ]));

        $question->time = $this->convertToSeconds($question);
        $question->number = sizeof($userEvaluation->results->pluck('question_id')->toArray()) + 1;

        return $question;
    }

    /**
     * Convert the question time to seconds.
     *
     * @param Question $question
     * @return mixed
     */
    protected function convertToSeconds(Question $question)
    {
        if ($question->time_type == 'm') {
            return $question->time * 60;
        }

        if ($question->time_type == 'h') {
            return $question->time * 60 * 60;
        }

        if ($question->time_type == 'd') {
            return $question->time * 60 * 60 * 24;
        }

        return $question->time;
    }

    /**
     * Get the remaining time in seconds for a question.
     *
     * @param UserEvaluationResult $result
     * @return int
     */
    protected function getRemainingTime(UserEvaluationResult $result)
    {
        $question = Question::find($result->question_id);
        $passedTime = $result->started_at->diffInSeconds(Carbon::now());

        if ($question->time_type == 's') {
            return $question->time - $passedTime;
        }

        if ($question->time_type == 'm') {
            return $question->time * 60 - $passedTime;
        }

        if ($question->time_type == 'h') {
            return $question->time * 60 * 60 - $passedTime;
        }

        return $question->time * 60 * 60 * 24 - $passedTime;
    }

    /**
     * Complete the evaluation.
     *
     * @param $userEvaluation
     * @return \Illuminate\Http\Response|array
     */
    protected function completeEvaluation($userEvaluation)
    {
        if ($userEvaluation->evaluation->type == "LANGUAGE") {
            return $this->completeLanguageEvaluation($userEvaluation);
        }

        return $this->completeEssayEvaluation($userEvaluation);
    }

    /**
     * Complete a language evaluation.
     *
     * @param UserEvaluation $userEvaluation
     * @return array
     */
    protected function completeLanguageEvaluation(UserEvaluation $userEvaluation)
    {
        $application = Auth::user()->application;

        $correctScore = $userEvaluation->results->where('correct', 1)->count();
        $requiredScore = $userEvaluation->evaluation->min_score;

        if ($correctScore < $requiredScore) {
            $application->step = ApplicationSteps::COMPLETE;
            $application->status = ApplicationStatus::DECLINED;
            $application->save();

            $userEvaluation->status = false;
            $userEvaluation->result = $correctScore;
            $userEvaluation->passed = false;
            $userEvaluation->completed_at = Carbon::now();
            $userEvaluation->save();

            return ['evaluation' => 'complete'];
        }

        $userEvaluation->status = true;
        $userEvaluation->result = $correctScore;
        $userEvaluation->passed = true;
        $userEvaluation->completed_at = Carbon::now();
        $userEvaluation->save();

        $this->setUpEssayEvaluation();

        return ['evaluation' => 'complete'];
    }

    /**
     * Set up the essay evaluation.
     *
     * @return array|boolean
     */
    public function setUpEssayEvaluation()
    {
        $evaluations = Evaluation::whereType('ESSAY')->inRandomOrder()->get();
        if (! $evaluations->count()) {
            $application = Auth::user()->application;
            $application->step = ApplicationSteps::COMPLETE;

            Notification::send(Auth::user(), new ApplicationCompleted);

            return $application->save();
        }

        $application = Auth::user()->application;
        $application->step = ApplicationSteps::ESSAY_EVALUATION;
        $application->save();

        $evaluation = $evaluations->first();

        $userEvaluation = UserEvaluation::firstOrCreate([
            'evaluation_id' => $evaluation->id,
        ]);

        if ($userEvaluation->wasRecentlyCreated) {
            $questions = Question::select('id')->whereType('ESSAY')
                                 ->inRandomOrder()
                                 ->take($evaluation->total_questions)->get();

            $userEvaluation->questions = $questions->pluck('id');
            $userEvaluation->save();
        }

        return $userEvaluation;
    }

    /**
     * Complete the essay evaluation and the application as a whole.
     *
     * @param UserEvaluation $userEvaluation
     * @return \Illuminate\Http\Response
     */
    protected function completeEssayEvaluation(UserEvaluation $userEvaluation)
    {
        $application = Auth::user()->application;
        $application->step = ApplicationSteps::COMPLETE;
        $application->save();

        $userEvaluation->status = true;
        $userEvaluation->completed_at = Carbon::now();
        $userEvaluation->save();

        Notification::send(Auth::user(), new ApplicationCompleted);

        return redirect(route('application'));
    }

    /**
     * Load the essay evaluation.
     *
     * @return \Illuminate\Http\Response
     */
    public function essayEvaluation()
    {
        $userEvaluation = UserEvaluation::with('evaluation:id,instructions')->pending();

        if (is_null($userEvaluation->started_at)) {
            return view('app.applications.evaluations.essay', [
                'data' => [
                    'type' => 'evaluation',
                    'userEvaluation' => $userEvaluation,
                ],
            ]);
        }

        $question = $this->getQuestion($userEvaluation);

        if ($question instanceof Question) {
            return view('app.applications.evaluations.essay', [
                'data' => [
                    'type' => 'essay',
                    'evaluation' => $userEvaluation,
                    'question' => $question,
                ],
            ]);
        }

        return $this->completeEvaluation($userEvaluation);
    }

    /**
     * Show the application complete status.
     *
     * @return \Illuminate\Http\Response
     */
    public function complete()
    {
        return view('app.applications.complete', [
            "application" => Auth::user()->application,
            "status" => new ApplicationStatus(),
            "step" => new ApplicationSteps(),
        ]);
    }

    /**
     * Load the User Profile.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $this->validate($request, [
            'english' => 'required|max:255',
            'phone_number' => 'required|max:255',
            'writer_type' => 'required|max:255',
            'country_code' => 'required|max:255',
            'about' => 'required|min:250|max:1000',
            'document' => 'required|image|max:5120',
        ]);

        $profile = WriterProfile::create([
            'english' => $request->english,
            'writer_type' => $request->writer_type,
            'about' => $request->about,
        ]);

        Auth::user()->update([
            'phone_number' => $request->phone_mumber,
            'country_code' => $request->country_code,
        ]);

        $profile->addMediaFromRequest('document')->toMediaCollection('documents', 'documents');
        Auth::user()->addMediaFromRequest('avatar')->toMediaCollection('avatars', 'avatars');

        $application = Auth::user()->application;
        $application->step = ApplicationSteps::EDUCATION;
        $application->save();

        Alert::success('Success', 'Profile saved successfully.')->persistent();

        return redirect(route('application'));
    }

    /**
     * Update the writer's education.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function education(Request $request)
    {
        $this->validate($request, [
            'academic_level_id' => 'required',
            'course' => 'required|max:255',
            'school' => 'required|max:255',
            'date_completed' => 'required|date',
            'certificate' => 'image|max:5120',
        ]);

        $education = Education::create($request->all(['academic_level_id', 'school', 'course', 'date_completed']));
        $education->addMediaFromRequest('certificate')->toMediaCollection('certs', 'certs');

        $application = Auth::user()->application;
        $application->step = ApplicationSteps::SKILLS;
        $application->save();

        Alert::success('Success', 'Education saved successfully')->persistent();

        return redirect(route('application'));
    }

    /**
     * Update the writer's skills.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function skills(Request $request)
    {
        $this->validate($request, [
            'past_writer' => 'required',
            'citations' => 'required',
            'disciplines' => 'required|min:5|max:15',
        ]);

        Skill::create($request->all(['past_writer', 'citations', 'disciplines']));

        $this->setUpLanguageEvaluation();

        Alert::success('Success', 'Skills saved successfully')->persistent();

        return redirect(route('application'));
    }

    /**
     * Set up the language evaluation.
     *
     * @return UserEvaluation|bool
     */
    public function setUpLanguageEvaluation()
    {
        $evaluations = Evaluation::whereType('LANGUAGE')->inRandomOrder()->get();
        $application = Auth::user()->application;

        if (! $evaluations->count()) {
            return $this->setUpEssayEvaluation();
        }

        $application->step = ApplicationSteps::LANGUAGE_EVALUATION;
        $application->save();

        $evaluation = $evaluations->first();

        $userEvaluation = UserEvaluation::firstOrCreate([
            'evaluation_id' => $evaluation->id,
        ]);

        if ($userEvaluation->wasRecentlyCreated) {
            $questions = Question::select('id')->whereType('LANGUAGE')
                                 ->inRandomOrder()
                                 ->take($evaluation->total_questions)->get();

            $userEvaluation->questions = $questions->pluck('id');
            $userEvaluation->save();
        }

        return $userEvaluation;
    }

    /**
     * Start the language evaluation.
     *
     * @return JsonResponse
     */
    public function startLanguageEvaluation()
    {
        $userEvaluation = UserEvaluation::pending();
        $userEvaluation->started_at = $started_at = Carbon::now();
        $userEvaluation->save();

        $question = $this->newQuestion($userEvaluation);

        return new JsonResponse($question);
    }

    /**
     * Submit a language evaluation question.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function submitLanguageQuestion(Request $request)
    {
        $this->validate($request, [
            'question_id' => 'required',
        ]);

        $userEvaluation = UserEvaluation::pending();
        $userEvaluationResult = UserEvaluationResult::whereUserEvaluationId($userEvaluation->id)
                                                    ->whereQuestionId($request->question_id)
                                                    ->firstOrFail();

        $userEvaluationResult->option_id = $request->option_id;
        $userEvaluationResult->submitted_at = Carbon::now();
        $userEvaluationResult->save();

        $this->markQuestion($userEvaluationResult);

        $question = $this->newQuestion($userEvaluation);

        if ($question instanceof Question) {
            return new JsonResponse([
                "evaluation" => "pending",
                "question" => $question,
            ]);
        }

        return new JsonResponse($this->completeEvaluation($userEvaluation));
    }

    /**
     * Mark the question.
     *
     * @param $result
     * @return mixed
     */
    protected function markQuestion($result)
    {
        $questionOption = QuestionOption::whereQuestionId($result->question_id)->whereIsAnswer(true)->first();
        if ($questionOption->id == $result->option_id) {
            $result->correct = true;
        } else {
            $result->correct = false;
        }

        return $result->save();
    }

    /**
     * Start the Essay Evaluation.
     *
     * @return \Illuminate\Http\Response
     */
    public function startEssayEvaluation()
    {
        $started_at = Carbon::now();

        $userEvaluation = UserEvaluation::pending();
        $question = Question::find(Arr::first($userEvaluation->questions));

        $userEvaluation->started_at = $started_at;
        $userEvaluation->save();

        $userEvaluation->results()->save(new UserEvaluationResult([
            'question_id' => $question->id,
            'started_at' => $started_at,
        ]));

        return redirect(route('application'));
    }

    /**
     * Submit the essay evaluation.
     *
     * @param integer $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function submitEssayQuestion(Request $request, $id)
    {
        $this->validate($request, ['essay' => 'mimes:doc,docx'], ["essay.mimes" => "Upload a valid word document"]);

        $userEvaluation = UserEvaluation::pending();
        $userEvaluationResult = UserEvaluationResult::whereQuestionId($id)->whereUserEvaluationId($userEvaluation->id)
                                                    ->first();
        $userEvaluationResult->submitted_at = $now = Carbon::now();
        $userEvaluationResult->save();

        $userEvaluationResult->addMediaFromRequest('essay')->toMediaCollection('essays', 'essays');

        $application = Auth::user()->application;
        $question = Question::find($id);

        // Allocated time has exceeded
        if ($this->checkIfExpired($userEvaluationResult, $question)) {
            $application->status = ApplicationStatus::DECLINED;
            $application->step = ApplicationSteps::COMPLETE;
            $application->save();

            Alert::warning('Declined!', 'Late essay submission')->persistent();

            return redirect(route('application'));
        }

        // Check for another question
        $question = $this->newQuestion($userEvaluation);

        if ($question instanceof Question) {
            return redirect(route('application'));
        }

        return $this->completeEssayEvaluation($userEvaluation);
    }
}
