<?php

namespace App\Http\Controllers;

use App\Models\Blog\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    /**
     * Display the blog posts
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::posts();
        return view('blog.posts', [
            'posts' => $posts
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view('blog.show', [
            'post' => Post::whereSlug($slug)->firstOrFail()
        ]);
    }



    public function search(Request $request)
    {
        $posts = Post::search($request)->paginate(5);
        return view('blog.posts', [
            'posts' => $posts
        ]);
    }

}
