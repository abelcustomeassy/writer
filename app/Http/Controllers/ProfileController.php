<?php

namespace App\Http\Controllers;

 use App\Models\Orders\Order;
 use RealRashid\SweetAlert\Facades\Alert;
use DateTimeZone;
use App\Models\User;
use App\Rating\Rater;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Configurations\Website;
use App\Models\Configurations\Country;

class ProfileController extends Controller
{
    /**
     * ProfileController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'checkEmail']);
    }

    public function index()
    {
        $sixtyDaysAgo = now()->subDays(60);

        return view('app.profile.index', [
            'user' => Auth::user(),
            'clientRating' => Rater::getClientRating(Auth::user()),
            'editorRating' => Rater::getEditorRating(Auth::user()),

            'disputeCount' => Order::hasDisputeCount(Auth::user()),
            'latenessCount' => Order::wasLateCount(Auth::user()),
            'revisionCount' => Order::hasRevisionCount(Auth::user()),
            'clientRequestsCount' => Order::clientRequestsCount(Auth::user()),

            'totalOrders' => Order::completedOrdersCount(Auth::user()),

            'countries' => Country::all(),
            'timezones' => DateTimeZone::listIdentifiers(DateTimeZone::ALL),
        ]);
    }

    /**
     * Update user details.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, $this->rules(), $this->messages());

        $user = Auth::user();

        $user->update([
            'phone_number' => $request->phoneNumber,
            'country_code' => $request->countryCode,
            'timezone' => $request->timezone,
        ]);

        Alert::success('Success', 'User profile updated')->persistent();

        return redirect()->back();
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'timezone' => 'required|max:255',
            'phoneNumber' => 'required|max:255',
            'countryCode' => 'required|max:255',
        ];
    }

    /**
     * Messages for validations.
     *
     * @return array
     */
    protected function messages()
    {
        return [
            'avatar.image' => 'Please upload images only',
            'avatar.max' => 'Image should not exceed 2MB',
        ];
    }

    /**
     * Show update password form.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        return view('app.profile.settings');
    }

    /**
     * Update the user password.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'new_password' => 'required:confirmed',
        ]);

        $user = Auth::user();

        if (! Hash::check($request->current_password, $user->password)) {
            return redirect()->back()->withErrors(['current_password' => 'Current password did not match.']);
        }

        $user->password = bcrypt($request->new_password);
        $user->save();

        Alert::success('Success', 'Password updated successfully')->persistent();

        return redirect()->back();
    }

    /**
     * Check if email exists.
     *
     * @param Request $request
     * @return string
     */
    public function checkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required']);
        $website = Website::whereName(config('app.name'))->first();
        $user = User::whereEmail($request->email)->whereWebsiteId($website->id)->first();
        if (is_null($user)) {
            return 'true';
        }

        return 'false';
    }

    /**
     * Upload the avatar.
     *
     * @param User $user
     * @return void
     */
    protected function uploadAvatar(User $user)
    {
        if ($user->hasMedia('avatars')) {
            $user->clearMediaCollection('avatars');
        }
        $user->addMediaFromRequest('avatar')
             ->toMediaCollection('avatars', 'avatars');
    }
}
