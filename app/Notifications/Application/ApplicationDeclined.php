<?php

namespace App\Notifications\Application;

use Illuminate\Bus\Queueable;
use App\Models\Applications\Application;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ApplicationDeclined extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The writer application concerned.
     *
     * @var Application
     */
    protected $application;

    /**
     * Create a new notification instance.
     *
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown('emails.application.application_declined', [
            'application' => $this->application,
        ]);
    }
}
