<?php

namespace App\Notifications\Orders;

use App\Models\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderBidApproved extends Notification implements ShouldQueue
{
    use Queueable;
    /**
     * @var \App\Notifications\Orders\Order
     */
    protected $order;

    /**
     * Create a new notification instance.
     *
     * @param \App\Models\Orders\Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown('email.orders.order_bid_approved', [
            'order' => $this->order,
            'url' => route('orders.show', ['id' => $this->order->id])
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'Your bid for order #' . $this->order->id . ' has been approved.',
            'url' => route('orders.show', ['id' => $this->order->id])
        ];
    }
}
