<?php

namespace App\Notifications\Orders;

use App\Models\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderTaken extends Notification implements ShouldQueue
{
    use Queueable;

    public $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown('emails.orders.order_taken', [
            'order' => $this->order,
            'url' => route('orders.show', ['id' => $this->order->id]),
        ])->subject('You have taken order #'.$this->order->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'You have taken order #'.$this->order->id,
            'url' => route('orders.show', ['id' => $this->order->id]),
        ];
    }
}
