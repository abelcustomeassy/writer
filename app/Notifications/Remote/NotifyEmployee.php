<?php

namespace App\Notifications\Remote;

use App\Models\User;
use App\Models\Orders\Order;
use App\Http\Requests\RemoteRequest;

class NotifyEmployee extends RemoteRequest
{
    /**
     * Notification constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    /**
     * Notify the employee they have been assigned a new order.
     *
     * @param Order $order
     * @return mixed
     */
    public function orderSubmitted(Order $order)
    {
        $url = 'order-submitted';
        $data = [
            'order_id' => $order->id,
        ];

        return $this->send($url, $data);
    }

    public function orderConfirmed(Order $order)
    {
        $url = 'order-confirmed';
        $data = [
            'order_id' => $order->id,
        ];

        return $this->send($url, $data);
    }

    public function orderReviewConfirmed($order)
    {
        $url = 'order-review-confirmed';
        $data = [
            'order_id' => $order->id,
        ];

        return $this->send($url, $data);
    }

    public function bidPlaced($bid)
    {
        $url = 'bid-placed';
        $data = [
            'bid_id' => $bid->id,
        ];

        return $this->send($url, $data);
    }
}
