<?php

namespace App\Notifications\Payments;

use Illuminate\Bus\Queueable;
use App\Models\Finance\Withdrawal;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PaymentRequestReceived extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var \App\Models\Finance\Withdrawal
     */
    private $withdrawal;

    /**
     * Create a new notification instance.
     *
     * @param \App\Models\Finance\Withdrawal $withdrawal
     */
    public function __construct(Withdrawal $withdrawal)
    {
        $this->withdrawal = $withdrawal;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown('emails.payments.payment_request_received', [
            'withdrawal' => $this->withdrawal
        ])->subject('Payment request for reference '.$this->withdrawal->reference);
    }
}
