<?php

namespace App\Notifications\Fines;

use App\Models\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DeadlineExtensionFineApplied extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var \App\Models\Orders\Order $order
     */
    private $order;

    /**
     * Create a new notification instance.
     *
     * @param \App\Models\Orders\Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown('emails.fines.deadline_extension_fine_applied', [
            'order' => $this->order,
            'url' => route('wallet.index')
        ])->subject('Deadline extension fine applied for order #' . $this->order->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'Deadline extension fine applied for order #' . $this->order->id,
            'url' => route('wallet.index')
        ];
    }
}
