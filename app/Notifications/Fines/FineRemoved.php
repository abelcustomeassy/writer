<?php

namespace App\Notifications\Fines;

use App\Models\Orders\Fine;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FineRemoved extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var \App\Models\Orders\Fine
     */
    private $fine;

    /**
     * Create a new notification instance.
     *
     * @param \App\Models\Orders\Fine $fine
     */
    public function __construct(Fine $fine)
    {
        $this->fine = $fine;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->markdown('emails.fines.fine_removed', [
            'orderId' => $this->fine->order_id,
            'writer' => $notifiable,
            'url' => route('wallet.index')
        ])->subject('Fine applied for order #' . $this->fine->order_id . ' removal');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'Fine applied for order #' . $this->fine->order_id . 'removal',
            'url' => route('wallet.index')
        ];
    }
}
