<?php

namespace App\DataTables\Orders;

use App\Models\Orders\Order;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;

class OrdersDataTable extends DataTable
{
    /**
     * Initialize default table parameters for the order dataTables.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function defaultDataTable($query)
    {
        return datatables($query)
            ->setRowAttr([
                'data-href' => function ($order) {
                    return route('orders.show', ['id' => $order->id]);
                },
            ])
            ->setRowClass('dt-order-item')
            ->editColumn('id', function ($order) {
                return view('app.partials.link', [
                    'link' => route("orders.show", ["id" => $order->id]),
                    'text' => '#' . $order->id,
                ]);
            })
            ->editColumn('writer_deadline', function ($order){
                return view('app.partials.order.deadline', ['order' => $order]);
            })
            ->editColumn('writer_amount', '{!! currency().$writer_amount !!}');
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function sqlQuery(Order $model)
    {
        return $model::with('academicLevel', 'citation', 'discipline')
            ->select([
                'orders.id',
                'topic',
                'amount',
                'pages',
                'writer_deadline',
                'status',
                'academic_level_id',
                'citation_id',
                'discipline_id',
                'created_at'
            ]);
    }

    /**
     * Get the writer should be paid.
     *
     * @return string
     */
    protected function getWriterPriceQuery()
    {
        if (Auth::user()->writerProfile->writerLevel->payment == 'PERCENTAGE') {
            return "(orders.amount * (" . Auth::user()->writerProfile->writerLevel->value->percentage . " / 100)) as writer_amount";
        }

        $pages = Auth::user()->writerProfile->writerLevel->value->pages?? 0;
        $ppt = Auth::user()->writerProfile->writerLevel->value->ppt?? 0;

        return "(CASE WHEN
                    orders.spacing = 'single'
                    THEN (ppt_slides * $ppt) + (orders.pages * 2 * $pages)
                    ELSE (ppt_slides * $ppt) + (orders.pages * $pages)
                END) as writer_amount";
    }
}
