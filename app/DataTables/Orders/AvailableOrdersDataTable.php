<?php

namespace App\DataTables\Orders;

use App\Common\OrderStatus;
use App\Models\Orders\Order;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AvailableOrdersDataTable extends OrdersDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return $this->defaultDataTable($query)
                    ->editColumn('writer_amount', function ($order) {
                        if ($order->writerPricing()->exists()) {
                            return currency().$this->getWriterPricing($order);
                        }

                        return currency().$order->writer_amount;
                    })
                    ->rawColumns(['id', 'writer_deadline']);
    }

    /**
     * Get writer pricing from order.
     *
     * @param $order
     * @return float|int
     */
    protected function getWriterPricing($order)
    {
        $levelName = strtolower(Auth::user()->writerProfile->writerLevel->name);

        if (isset($order->writerPricing->$levelName->percentage)) {
            return ($order->writerPricing->$levelName->percentage / 100) * $order->amount;
        }

        if ($order->spacing == 'single') {
            return ($order->ppt_slides * $order->writerPricing->$levelName->ppt) +
                ($order->pages * 2 * $order->writerPricing->$levelName->pages);
        }

        return ($order->ppt_slides * $order->writerPricing->$levelName->ppt) +
            ($order->pages * $order->writerPricing->$levelName->pages);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $this->sqlQuery($model)
                    ->addSelect(DB::Raw($this->getWriterPriceQuery()))
                    ->where('status', OrderStatus::AVAILABLE)
                    ->whereNull('deleted_at')
                    ->whereDoesntHave('bids', function (Builder $query) {
                        $query->where('user_id', Auth::user()->id);
                    });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => '# Order'],
            ['data' => 'topic', 'name' => 'topic', 'title' => 'Topic','width'=>'30%'],
            ['data' => 'academic_level.name', 'name' => 'academicLevel.name', 'title' => 'Academic Level'],
            ['data' => 'discipline.name', 'name' => 'discipline.name', 'title' => 'Discipline'],
            ['data' => 'pages', 'name' => 'pages', 'title' => 'Pages'],
            ['data' => 'writer_deadline', 'name' => 'writer_deadline', 'title' => 'Deadline'],
            ['data' => 'writer_amount', 'name' => 'writer_amount', 'title' => 'Price'],
        ];
    }

    /**
     * Set some build parameters
     *
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order' => [[5, 'asc']],
            'autoWidth' => false,
            'responsive' => true,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'available_orders_'.date('YmdHis');
    }
}
