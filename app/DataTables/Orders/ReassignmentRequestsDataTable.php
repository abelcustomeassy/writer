<?php

namespace App\DataTables\Orders;

use App\Common\OrderStatus;
use App\Models\Orders\Order;
use Illuminate\Support\Facades\Auth;

class ReassignmentRequestsDataTable extends OrdersDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return $this->defaultDataTable($query)
                    ->rawColumns(['id', 'writer_deadline']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $this->sqlQuery($model)->whereWriterId(Auth::user()->id)
            ->where('status', OrderStatus::REASSIGNMENT_REQUESTED);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Set some build parameters
     *
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order' => [[5, 'asc']],
            'autoWidth' => false
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => '# Order'],
            ['data' => 'topic', 'name' => 'topic', 'title' => 'Topic','width'=>'30%'],
            ['data' => 'academic_level.name', 'name' => 'academicLevel.name', 'title' => 'Academic Level'],
            ['data' => 'discipline.name', 'name' => 'discipline.name', 'title' => 'Discipline'],
            ['data' => 'pages', 'name' => 'pages', 'title' => 'Pages'],
            ['data' => 'writer_deadline', 'name' => 'writer_deadline', 'title' => 'Deadline'],
            ['data' => 'amount', 'name' => 'amount', 'title' => 'Price'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reassignment_requests_'.date('YmdHis');
    }
}
