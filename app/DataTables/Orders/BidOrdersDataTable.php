<?php

namespace App\DataTables\Orders;

use App\Common\OrderStatus;
use App\Models\Orders\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BidOrdersDataTable extends OrdersDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->setRowAttr([
                'data-href' => function ($order) {
                    return route('orders.show', ['id' => $order->id]);
                },
            ])
            ->setRowClass('dt-order-item')
            ->editColumn('id', function ($order) {
                return view('app.partials.link', [
                    'link' => route("orders.show", ["id" => $order->id]),
                    'text' => '#'.$order->id,
                ]);
            })
            ->editColumn('status', function ($order) {
                return view('app.orders.status.mini_status', [
                    'order' => $order,
                    'status' => new OrderStatus(),
                ]);
            })
            ->editColumn('writer_deadline', function ($order){
                return view('app.partials.order.deadline', ['order' => $order]);
            })
            ->editColumn('writer_amount', function ($order) {
                if ($order->writerPricing()->exists()) {
                    return currency().$this->getWriterPricing($order);
                }

                return currency().$order->writer_amount;
            })
            ->rawColumns(['id', 'status', 'writer_deadline']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $this->sqlQuery($model)
            ->addSelect('writer_amount')
            ->addSelect(DB::Raw($this->getWriterPriceQuery()))
            ->whereHas('bids', function ($query) {
                $query->whereUserId(Auth::user()->id);
            })->whereStatus(OrderStatus::AVAILABLE);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Set some build parameters
     *
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order' => [[5, 'asc']],
            'autoWidth' => false
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => '# Order'],
            ['data' => 'topic', 'name' => 'topic', 'title' => 'Topic','width'=>'30%'],
            ['data' => 'academic_level.name', 'name' => 'academicLevel.name', 'title' => 'Academic Level'],
            ['data' => 'discipline.name', 'name' => 'discipline.name', 'title' => 'Discipline'],
            ['data' => 'pages', 'name' => 'pages', 'title' => 'Pages'],
            ['data' => 'writer_deadline', 'name' => 'writer_deadline', 'title' => 'Deadline'],
            ['data' => 'status', 'name' => 'status', 'title' => 'Status'],
            ['data' => 'writer_amount', 'name' => 'writer_amount', 'title' => 'Price'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'bid_orders_'.date('YmdHis');
    }
}
