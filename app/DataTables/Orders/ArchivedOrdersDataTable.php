<?php

namespace App\DataTables\Orders;

use App\Common\OrderStatus;
use App\Models\Orders\Order;
use Illuminate\Support\Facades\Auth;

class ArchivedOrdersDataTable extends OrdersDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return $this->defaultDataTable($query)
            ->editColumn('status', function ($order) {
                return view('app.orders.status.full_status', [
                    'order' => $order,
                    'status' => new OrderStatus(),
                ]);
            })
            ->rawColumns(['id', 'status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $this->sqlQuery($model)
            ->addSelect('writer_amount')
            ->where(function ($query) {
            $query->where('status', OrderStatus::ACCEPTED)
                ->orWhere('status', OrderStatus::AUTO_ACCEPTED)
                ->orWhere('status', OrderStatus::MANUALLY_ACCEPTED)
                ->orWhere('status', OrderStatus::CANCELLED)
                ->orWhere('status', OrderStatus::PARTIAL_REFUND)
                ->orWhere('status', OrderStatus::REFUNDED);
        })->whereWriterId(Auth::user()->id);

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Set some build parameters
     *
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order' => [[5, 'desc']],
            'autoWidth' => false
        ];
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => '# Order'],
            ['data' => 'topic', 'name' => 'topic', 'title' => 'Topic','width'=>'30%'],
            [
                'data' => 'academic_level.name',
                'name' => 'academicLevel.name',
                'title' => 'Academic Level',
                'searchable' => false,
            ],
            ['data' => 'discipline.name', 'name' => 'discipline.name', 'title' => 'Discipline'],
            ['data' => 'writer_amount', 'name' => 'writer_amount', 'title' => 'Price'],
            'status',
            ['data' => 'created_at', 'name' => 'created_at', 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'archived_orders_'.date('YmdHis');
    }
}
