<?php

namespace App\DataTables\Orders;

use App\Common\OrderStatus;
use App\Models\Orders\Order;
use Illuminate\Support\Facades\Auth;

class AssignedOrdersDataTable extends OrdersDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return $this->defaultDataTable($query)->addColumn('confirmed', function ($order) {
            return view('app.orders.status.confirm', [
                'order' => $order,
                'status' => new OrderStatus(),
            ]);
        })->rawColumns(['id', 'confirmed', 'writer_deadline']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $this->sqlQuery($model)
                    ->addSelect('writer_amount')
                    ->whereWriterId(Auth::user()->id)->where(function ($query) {
                $query->orWhere('status', OrderStatus::ASSIGNED)
                      ->orWhere('status', OrderStatus::REASSIGNMENT_REQUESTED)
                      ->orWhere('status', OrderStatus::DEADLINE_EXTENSION_REQUESTED)
                      ->orWhere('status', OrderStatus::PENDING_CONFIRMATION);
            });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()->columns($this->getColumns())->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => '# Order'],
            ['data' => 'topic', 'name' => 'topic', 'title' => 'Topic', 'width' => '30%'],
            ['data' => 'academic_level.name', 'name' => 'academicLevel.name', 'title' => 'Academic Level'],
            ['data' => 'discipline.name', 'name' => 'discipline.name', 'title' => 'Discipline'],
            ['data' => 'pages', 'name' => 'pages', 'title' => 'Pages'],
            ['data' => 'writer_deadline', 'name' => 'writer_deadline', 'title' => 'Deadline'],
            ['data' => 'writer_amount', 'name' => 'writer_amount', 'title' => 'Price'],
            'confirmed',
        ];
    }

    /**
     * Set some build parameters
     *
     * @return array
     */
    public function getBuilderParameters()
    {
        return [
            'order' => [[5, 'asc']],
            'autoWidth' => false,
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'assigned_orders_'.date('YmdHis');
    }
}
