<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\SidebarComposer;
use App\Http\ViewComposers\CountersComposer;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $views = [
            'app.dashboard',
            'app.nav.top-nav',
            'app.nav.sidebar',
        ];

        View::composer('app.nav.sidebar', SidebarComposer::class);
        View::composer(['app.nav.top', 'app.dashboard', 'app.nav.sidebar'], CountersComposer::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}