<?php
// Load the home page
Route::get('/', 'PagesController@index')->name('home');

// Register authentication routes
Auth::routes();

// Show the page indicating registration is complete
Route::get('/complete', 'Auth\RegisterController@complete')->name('complete');

// Confirm registration
Route::get('/confirm/{code}', 'Auth\RegisterController@confirm')->name('confirm');

// Check Email
Route::post('check-email', 'ProfileController@checkEmail');

// Post content from contact form
Route::post('contact', 'ContactController@store')->name('contacts.store');

// These routes can not be accessed without completing the application
Route::group(['middleware' => ['application', 'announcement']], function () {
    // Load the dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    // Orders
    Route::get('orders/available', 'OrdersController@available')->name('orders.available');
    Route::get('orders/bids', 'OrdersController@bids')->name('orders.bids');
    Route::get('orders/assigned', 'OrdersController@assigned')->name('orders.assigned');
    Route::get('orders/submitted', 'OrdersController@submitted')->name('orders.submitted');
    Route::get('orders/reviewed', 'OrdersController@reviewed')->name('orders.reviewed');
    Route::get('orders/disputed', 'OrdersController@disputed')->name('orders.disputed');
    Route::get('orders/archived', 'OrdersController@archived')->name('orders.archived');

    Route::get('orders/download/{mediaItem}', 'OrdersController@download')->name('orders.download');
    Route::post('orders/add-new-files/{id}', 'OrdersController@addNewFiles')->name('orders.add-new-files');
    Route::post('orders/confirm/{id}', 'OrdersController@confirmOrder')->name('orders.confirm');
    Route::post('orders/confirm-admin-review/{id}', 'OrdersController@confirmAdminReview')->name('orders.confirm-admin-review');
    Route::post('orders/confirm-client-review/{id}', 'OrdersController@confirmClientReview')->name('orders.confirm-client-review');
    Route::post('orders/reassign/{id}', 'OrdersController@reassign')->name('orders.reassign');
    Route::post('orders/reject/{id}', 'OrdersController@rejectOrder')->name('orders.reject');
    Route::post('orders/request/{id}', 'OrdersController@requestOrder')->name('orders.request');
    Route::post('orders/submit/{id}', 'OrdersController@submitOrder')->name('orders.submit');
    Route::post('orders/take/{id}', 'OrdersController@takeOrder')->name('orders.take');
    Route::post('orders/deadline-extension/{id}', 'OrdersController@deadlineExtensionRequest')->name('orders.deadline-extension');
    Route::post('orders/cancel-request/{id}', 'OrdersController@cancelRequest')->name('orders.cancel-request');
    Route::get('orders/{id}', 'OrdersController@show')->name('orders.show');

    // Messages
    Route::get('messages', 'MessagesController@index')->name('messages.index');
    Route::get('messages/download/{mediaItem}', 'MessagesController@download')->name('messages.download');
    Route::post('messages', 'MessagesController@store')->name('create-message');
    Route::post('messages/read', 'MessagesController@doRead')->name('read-message');
    Route::post('messages/reply', 'MessagesController@replyMessage')->name('reply-message');

    // Notifications
    Route::get('notifications', 'NotificationsController@index')->name('notifications');

    // Resources
    Route::get('resources', 'ResourceController@index')->name('resources.index');
    Route::get('resources/download/{mediaItem}', 'ResourceController@download')->name('resources.download');

    // Profile
    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::put('update-profile', 'ProfileController@update')->name('profile.update');
    Route::get('settings', 'ProfileController@settings')->name('settings');
    Route::put('update-password', 'ProfileController@updatePassword')->name('update-password');

    // Wallet
    Route::get('wallet', 'WalletController@index')->name('wallet.index');
    Route::get('wallet/withdrawals', 'WalletController@withdrawals')->name('wallet.withdrawals');
    Route::get('wallet/statement', 'WalletController@statement')->name('wallet.statement');
    Route::post('wallet/withdraw', 'WalletController@withdraw')->name('wallet.withdraw');
    Route::post('wallet/remove-fine', 'WalletController@removeFine')->name('wallet.remove-fine');
    Route::post('wallet/create-payment-account', 'WalletController@createPaymentAccount')->name('wallet.create-payment-account');
    Route::post('wallet/make-account-default/{id}', 'WalletController@makeAccountDefault')->name('wallet.make-account-default');
    Route::delete('wallet/delete-account/{id}', 'WalletController@deleteAccount')->name('wallet.delete-account');
    Route::get('wallet/verify-payment-account/{code}', 'WalletController@verifyPaymentAccount')->name('wallet.verify-payment-account');
});

// Announcements
Route::get('announcements', 'AnnouncementsController@index')->name('announcements.index');
Route::post('announcements/confirm/{id}', 'AnnouncementsController@confirm')->name('announcements.confirm');
Route::get('announcements/{slug}', 'AnnouncementsController@show')->name('announcements.show');

// Writer Application
Route::get('application', 'ApplicationsController@index')->name('application');
Route::post('application-profile', 'ApplicationsController@profile')->name('application-profile');
Route::post('application-education', 'ApplicationsController@education')->name('application-education');
Route::post('application-skills', 'ApplicationsController@skills')->name('application-skills');

Route::post('start-language-evaluation', 'ApplicationsController@startLanguageEvaluation')->name('start-language-evaluation');
Route::post('submit-language-question', 'ApplicationsController@submitLanguageQuestion')->name('submit-language-question');

Route::post('start-essay-evaluation', 'ApplicationsController@startEssayEvaluation')->name('start-essay-evaluation');
Route::post('submit-essay-question/{id}', 'ApplicationsController@submitEssayQuestion')->name('submit-essay-question');

// Blog
Route::group(['prefix' => 'blog'], function () {
    Route::get('/', 'PostsController@index')->name('posts.index');
    Route::get('/{slug}', 'PostsController@publicShow')->name('posts.show');
    Route::post('/search', 'PostsController@search')->name('posts.search');
});

// Handle Notification
Route::group(['prefix' => 'notifications'], function () {
    Route::post('new-files-added', 'NotificationsController@newFilesAdded');
    Route::post('application-approved', 'NotificationsController@applicationApproved');
    Route::post('application-declined', 'NotificationsController@applicationDeclined');
    Route::post('order-assigned', 'NotificationsController@orderAssigned');
    Route::post('order-revised', 'NotificationsController@orderRevised');
    Route::post('order-disputed', 'NotificationsController@orderDisputed');
    Route::post('order-bid-approved', 'NotificationsController@orderBidApproved');
    Route::post('order-reassigned', 'NotificationsController@orderReassigned');
    Route::post('plagiarism-fine-applied', 'NotificationsController@plagiarismFineApplied');
    Route::post('wrong-file-fine-applied', 'NotificationsController@wrongFileFineApplied');
    Route::post('ignored-messages-fine-applied', 'NotificationsController@ignoredMessagesFineApplied');
    Route::post('reassignment-fine-applied', 'NotificationsController@reassignmentFineApplied');
    Route::post('deadline-extension-fine-applied', 'NotificationsController@deadlineExtensionFineApplied');
    Route::post('fine-removed', 'NotificationsController@fineRemoved');

    Route::post('account-suspended', 'NotificationsController@accountSuspended');
    Route::post('account-un-suspended', 'NotificationsController@accountUnSuspended');
    Route::post('account-activated', 'NotificationsController@accountActivated');
    Route::post('account-deactivated', 'NotificationsController@accountDeactivated');

    Route::post('writer-level-upgraded', 'NotificationsController@writerLevelUpgraded');
    Route::post('writer-level-downgraded', 'NotificationsController@writerLevelDowngraded');

    Route::post('new-message', 'NotificationsController@newMessage');
    Route::post('new-order-available', 'NotificationsController@newOrderAvailable');
});

// This should be the last item
Route::get('{page}', 'PagesController@page');
